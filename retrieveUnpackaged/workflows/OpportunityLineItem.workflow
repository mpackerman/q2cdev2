<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AV_CA_BUN</fullName>
        <description>CA Special Offer Bundle Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>candrews@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>georgewarren@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlinden@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kclayton@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/CA_Special_Offer_Bundle_Notification</template>
    </alerts>
    <alerts>
        <fullName>ELL_New_Product_Notification</fullName>
        <description>ELL New Product Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lacook@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/ELL_Notifiction</template>
    </alerts>
    <alerts>
        <fullName>HEAL_Closed_Won_notification</fullName>
        <description>HEAL Closed Won notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>kmelfi@kcdistancelearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HEAL_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>K12_K_5_Closed_Won_Notification</fullName>
        <description>K12 K-5 Closed Won Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>kmelfi@kcdistancelearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/K12_Kto5_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>LearnBop_Notification</fullName>
        <description>LearnBop Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>bharani1@learnbop.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/LearnBop_Notifiction</template>
    </alerts>
    <alerts>
        <fullName>MIL_Legacy_Product_Notification</fullName>
        <description>MIL Legacy Product Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>kmelfi@kcdistancelearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_MIL_MMLA/MIL_Legacy_Product_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_MIL_on_closed_won_with_training</fullName>
        <ccEmails>tbruns@middleburyinteractive.com</ccEmails>
        <description>Notify MIL on closed won with training</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_MIL_Closed_WonTRN</template>
    </alerts>
    <fieldUpdates>
        <fullName>AEC_Products_Check</fullName>
        <field>AEC_Products__c</field>
        <literalValue>1</literalValue>
        <name>AEC Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AEC_Products_UnCheck</fullName>
        <field>AEC_Products__c</field>
        <literalValue>0</literalValue>
        <name>AEC Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>B2B_2012_Set_Notified_for_Legacy_to_Ye</fullName>
        <field>Notify_for_Legacy_Products__c</field>
        <literalValue>Yes</literalValue>
        <name>B2B 2012 - Set Notified for Legacy to Ye</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>B2B_Product_Name_Update</fullName>
        <field>Product_Name__c</field>
        <formula>Product2.Name</formula>
        <name>B2B Product Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_product_code</fullName>
        <field>ProductCode__c</field>
        <formula>PricebookEntry.Product2.ProductCode</formula>
        <name>Copy product code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flag_Materials_Product</fullName>
        <field>Materials_Hidden__c</field>
        <literalValue>1</literalValue>
        <name>Flag Materials Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flag_Teacher_Product</fullName>
        <field>K12_Teachers_Hidden__c</field>
        <literalValue>1</literalValue>
        <name>Flag Teacher Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Name_Update_for_Option</fullName>
        <field>Product_Name__c</field>
        <formula>OptionId__r.Name</formula>
        <name>Product Name Update for Option</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Name_Update_for_Product_Service</fullName>
        <field>Product_Name__c</field>
        <formula>Product2.Name</formula>
        <name>Product Name Update for Product/Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_Flag_Remove_VPA_Price</fullName>
        <description>Removes red flag VPA price.</description>
        <field>VPA_Price__c</field>
        <formula>0</formula>
        <name>Red Flag - Remove VPA Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_Flag_Remove_VP_Price</fullName>
        <description>Removes red flag for VP price.</description>
        <field>VP_Price__c</field>
        <formula>0</formula>
        <name>Red Flag - Remove VP Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_Flag_VPA_Price</fullName>
        <field>VPA_Price__c</field>
        <formula>1</formula>
        <name>Red Flag - VPA Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_Flag_VPA_Product</fullName>
        <field>VPA_Product__c</field>
        <formula>1</formula>
        <name>Red Flag - VPA Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_Flag_VP_Price</fullName>
        <field>VP_Price__c</field>
        <formula>1</formula>
        <name>Red Flag - VP Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Instruction</fullName>
        <description>used to set MIL instruction to yes</description>
        <field>Notify_MIL_Instruction__c</field>
        <literalValue>Yes</literalValue>
        <name>Set MIL Instruction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Products</fullName>
        <field>MIL_Products__c</field>
        <literalValue>Yes</literalValue>
        <name>Set MIL Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notified_for_Non_Standard_to_Yes</fullName>
        <field>Notify_for_Non_Standard_Products__c</field>
        <literalValue>Yes</literalValue>
        <name>Set Notified for Non-Standard to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notify_MIL_Enterprise_to_Yes</fullName>
        <field>Notify_MIL_Enterprise__c</field>
        <literalValue>Yes</literalValue>
        <name>Set Notify MIL Enterprise to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notify_MIL_Site_to_Yes</fullName>
        <field>Notify_MIL_Site__c</field>
        <literalValue>Yes</literalValue>
        <name>Set Notify MIL Site to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notify_Summer_School_to_Yes</fullName>
        <field>Notify_Summer_School__c</field>
        <literalValue>Yes</literalValue>
        <name>Set Notify Summer School to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notify_for_Enterprise</fullName>
        <field>Notify_Aventa_Enterprise__c</field>
        <literalValue>Yes</literalValue>
        <name>Set Notify for Enterprise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Consumption_Model_Option</fullName>
        <field>Product_Consumption_Model_Hidden__c</field>
        <formula>text(OptionId__r.Product_Consumption_Model__c)</formula>
        <name>Update Product Consumption Model Option</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Provider_Hidden</fullName>
        <field>Product_Provider_Hidden__c</field>
        <formula>Product_Provider__c</formula>
        <name>Update Product Provider Hidden</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Provider_Hidden_Option</fullName>
        <field>Product_Provider_Hidden__c</field>
        <formula>text(OptionId__r.Product_Provider__c)</formula>
        <name>Update Product Provider Hidden Option</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Provider_Hidden_Product_S</fullName>
        <field>Product_Provider_Hidden__c</field>
        <formula>text(Product2.Product_Provider__c)</formula>
        <name>Update Product Provider Hidden Product/S</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Provider_Option</fullName>
        <field>Product_Provider_Hidden__c</field>
        <formula>text(OptionId__r.Product_Provider__c)</formula>
        <name>Update Product Provider Option</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Stage</fullName>
        <field>Stage_Hidden__c</field>
        <formula>Stage__c</formula>
        <name>Update Product Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Classification</fullName>
        <field>Program_Classification_Hidden__c</field>
        <formula>Program_Classification__c</formula>
        <name>Update Program Classification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Classification_Option</fullName>
        <field>Program_Classification_Hidden__c</field>
        <formula>text(OptionId__r.Program_Classification__c)</formula>
        <name>Update Program Classification Option</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AEC Products Check</fullName>
        <actions>
            <name>AEC_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <description>Product: Product Provider EQUALS AEC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AEC Products unCheck</fullName>
        <actions>
            <name>AEC_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>notEqual</operation>
            <value>AEC</value>
        </criteriaItems>
        <description>Product: Product Provider NOT EQUAL TO AEC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Set Notif for Middlebury Enterprise</fullName>
        <actions>
            <name>Set_Notify_MIL_Enterprise_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>528</value>
        </criteriaItems>
        <description>Product: PUID EQUALS 528</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Set Notif for Middlebury Site</fullName>
        <actions>
            <name>Set_Notify_MIL_Site_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>662</value>
        </criteriaItems>
        <description>Product: PUID EQUALS 662</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Set Notif for Summer School Models</fullName>
        <actions>
            <name>Set_Notify_Summer_School_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Model_Name_Hidden__c</field>
            <operation>contains</operation>
            <value>Summer</value>
        </criteriaItems>
        <description>Opportunity Product: Model Name Hidden CONTAINS Summer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Set Notified for Legacy to Yes</fullName>
        <actions>
            <name>B2B_2012_Set_Notified_for_Legacy_to_Ye</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Standing__c</field>
            <operation>equals</operation>
            <value>Legacy</value>
        </criteriaItems>
        <description>Opportunity Product: Product Standing EQUALS Legacy</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Set Notified for Non-Standard to Yes</fullName>
        <actions>
            <name>Set_Notified_for_Non_Standard_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Standard_or_Non_Standard__c</field>
            <operation>equals</operation>
            <value>Non-Standard</value>
        </criteriaItems>
        <description>Opportunity Product: Standard or Non-Standard EQUALS Non-Standard</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Set Notify for Aventa Enterprise</fullName>
        <actions>
            <name>Set_Notify_for_Enterprise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.ProductCode</field>
            <operation>startsWith</operation>
            <value>FED-CUR-ENT</value>
        </criteriaItems>
        <description>Opportunity Product: Product Code STARTS WITH FED-CUR-ENT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy product code</fullName>
        <actions>
            <name>Copy_product_code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Total_Price_from_Proposal__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>copies product code to custom product code field for SO creattion</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ELL New Product Notifiction</fullName>
        <actions>
            <name>ELL_New_Product_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Option_Name__c</field>
            <operation>startsWith</operation>
            <value>ELL</value>
        </criteriaItems>
        <description>used to notify Kellie, Lakan and Chad when an ELL product is added to an opp</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LearnBop Notifiction</fullName>
        <actions>
            <name>LearnBop_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Provider__c</field>
            <operation>equals</operation>
            <value>LearnBop</value>
        </criteriaItems>
        <description>used to notify Kellie and Bharani when a learnbop product is added to an opp</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MIL Closed Won with Training Products</fullName>
        <actions>
            <name>Notify_MIL_on_closed_won_with_training</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.ProductCode__c</field>
            <operation>equals</operation>
            <value>MIL-PFS-TRN-STE-ONS,MIL-PFS-TRN-STE-OS3,MIL-PFS-TRN-SWW-WEB,MIL-PFS-TRN-MFP-WEB,MIL-PFS-TRN-MEP-WEB</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.ProductCode__c</field>
            <operation>equals</operation>
            <value>MIL-PFS-TRN-TTP-WEB,MIL-PFS-TRN-SGW-WEB,MIL-PFS-TRN-LGW-WEB,MIL-PFS-TRN-SML-ONS,MIL-PFS-TRN-MBL-SPT,MIL-PFS-TRN-LGW-ONS</value>
        </criteriaItems>
        <description>Notify Theresa Bruns when MIL Opp closed won with training products</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Instruction</fullName>
        <actions>
            <name>Set_MIL_Instruction</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2 or 3 or 4 or 5 or 6 or 7 or 8 or 9 or 10</booleanFilter>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>520</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>522</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>526</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>524</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>529</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>530</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>531</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>532</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>536</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PUID__c</field>
            <operation>equals</operation>
            <value>664</value>
        </criteriaItems>
        <description>Product: PUID Equals 520,522,526,524,529,530,531,532,536,664</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Products</fullName>
        <actions>
            <name>Set_MIL_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <description>Opportunity Product: Product Provider EQUALS Middlebury Interactive</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Product Consumption Model Option</fullName>
        <actions>
            <name>Update_Product_Consumption_Model_Option</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.LineType__c</field>
            <operation>equals</operation>
            <value>Option</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Product Provider Option</fullName>
        <actions>
            <name>Update_Product_Provider_Option</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.LineType__c</field>
            <operation>equals</operation>
            <value>Option</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Program Classification Option</fullName>
        <actions>
            <name>Update_Program_Classification_Option</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.LineType__c</field>
            <operation>equals</operation>
            <value>Option</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
