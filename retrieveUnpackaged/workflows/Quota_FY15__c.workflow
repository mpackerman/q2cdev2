<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculate_YTD_Commish_Earnings</fullName>
        <field>YTDCE__c</field>
        <formula>AE100B__c + AECCQ__c + AENCQ__c + AEAQ__c + AEQQ__c+ AEQCCQ__c + AECCG__c + EC1__c +EC2__c +EC3__c</formula>
        <name>Calculate YTD Commish Earnings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_CST_Box</fullName>
        <field>CST__c</field>
        <literalValue>1</literalValue>
        <name>Check CST Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_ISR_Box</fullName>
        <field>ISR__c</field>
        <literalValue>1</literalValue>
        <name>Check ISR Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_MGR_Box</fullName>
        <field>MGR__c</field>
        <literalValue>1</literalValue>
        <name>Check MGR Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_NAM_Box</fullName>
        <field>NAM__c</field>
        <literalValue>1</literalValue>
        <name>Check NAM Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_OTH_Box</fullName>
        <field>OTH__c</field>
        <literalValue>1</literalValue>
        <name>Check OTH Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_CST_Box</fullName>
        <field>CST__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck CST Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_ISR_Box</fullName>
        <field>ISR__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck ISR Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_MGR_Box</fullName>
        <field>MGR__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck MGR Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_NAM_Box</fullName>
        <field>NAM__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck NAM Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_OTH_Box</fullName>
        <field>OTH__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck OTH Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Earnings_NCQ</fullName>
        <field>AENCQ__c</field>
        <formula>IF ( TEANC__c =0,0,
 ENCR1__c +ENCR2__c +ENCR3__c +ENCR4__c +ENCR1I__c +ENCR2I__c )</formula>
        <name>Update Actual Earnings NCQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Annual_Quota</fullName>
        <field>QAO__c</field>
        <formula>NCQ__c+CCQ__c</formula>
        <name>Update Annual Quota</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P1</fullName>
        <field>CWQP1__c</field>
        <formula>CWQ__c/12</formula>
        <name>Update CW Quota P1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P10</fullName>
        <field>CWQP10__c</field>
        <formula>IF( (CWQ__c-(CWQ1__c+CWQ2__c+CWQ3__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWQ2__c+CWQ3__c))/3)</formula>
        <name>Update CW Quota P10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P11</fullName>
        <field>CWQP11__c</field>
        <formula>IF((CWQ__c-(CWQ1__c+CWQ2__c+CWQ3__c+Closed_Won_P10__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWQ2__c+CWQ3__c+Closed_Won_P10__c))/2)</formula>
        <name>Update CW Quota P11</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P12</fullName>
        <field>CWQP12__c</field>
        <formula>IF ((CWQ__c-(CWQ1__c+CWQ2__c+CWQ3__c+Closed_Won_P10__c+Closed_Won_P11__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWQ2__c+CWQ3__c+Closed_Won_P10__c+Closed_Won_P11__c)))</formula>
        <name>Update CW Quota P12</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P2</fullName>
        <field>CWQP2__c</field>
        <formula>IF ((CWQ__c-CWP1__c)&lt;0,0,
(CWQ__c-CWP1__c)/11)</formula>
        <name>Update CW Quota P2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P3</fullName>
        <field>CWQP3__c</field>
        <formula>IF ((CWQ__c-(CWP1__c+CWP2__c))&lt;0,0,
(CWQ__c-(CWP1__c+CWP2__c))/10)</formula>
        <name>Update CW Quota P3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P4</fullName>
        <field>CWQP4__c</field>
        <formula>IF( (CWQ__c-CWQ1__c)&lt;0,0,
(CWQ__c-CWQ1__c)/9)</formula>
        <name>Update CW Quota P4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P5</fullName>
        <field>CWQP5__c</field>
        <formula>IF ((CWQ__c-(CWQ1__c+CWP4__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWP4__c))/8)</formula>
        <name>Update CW Quota P5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P6</fullName>
        <field>CWQP6__c</field>
        <formula>IF((CWQ__c-(CWQ1__c+CWP4__c+CWP5__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWP4__c+CWP5__c))/7)</formula>
        <name>Update CW Quota P6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P7</fullName>
        <field>CWQP7__c</field>
        <formula>IF( (CWQ__c-(CWQ1__c+CWQ2__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWQ2__c))/6)</formula>
        <name>Update CW Quota P7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P8</fullName>
        <field>CWQP8__c</field>
        <formula>IF ((CWQ__c-(CWQ1__c+CWQ2__c+Closed_Won_P7__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWQ2__c+Closed_Won_P7__c))/5)</formula>
        <name>Update CW Quota P8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CW_Quota_P9</fullName>
        <field>CWQP9__c</field>
        <formula>IF( (CWQ__c-(CWQ1__c+CWQ2__c+ Closed_Won_P7__c+Closed_Won_P8__c))&lt;0,0,
(CWQ__c-(CWQ1__c+CWQ2__c+ Closed_Won_P7__c+Closed_Won_P8__c))/4)</formula>
        <name>Update CW Quota P9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Current_Customer_Quota_for_ISRs</fullName>
        <field>CCQ__c</field>
        <formula>QCQ1__c+QCQ2__c+QCQ3__c+QCQ4__c</formula>
        <name>Update Current Customer Quota for ISRs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Percent_to_Annual</fullName>
        <field>HPA__c</field>
        <formula>IF (QA__c=0,0,
IF (TYTD__c/QA__c&lt;1,
0,1))</formula>
        <name>Update Percent to Annual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Percent_to_Annual_SVP</fullName>
        <field>PASVP__c</field>
        <formula>IF (AYTD__c=0,0,
IF (AYTD__c&lt;0.9,0,
IF (AYTD__c&lt;1,0.5,
1)))</formula>
        <name>Update Percent to Annual SVP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Target_Earning_Annual_Current_Cus</fullName>
        <field>TEACC__c</field>
        <formula>IF (CCQ__c = 0,0,
(CCQ__c*0.8*CCR1__c) +(CCQ__c*0.2*CCR2__c))</formula>
        <name>Update Target Earning Annual Current Cus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Target_Earning_NCQ</fullName>
        <field>TEANC__c</field>
        <formula>IF (RNC__c&lt;2,0,
IF (RNC__c&lt;3, NCQ__c*NCR2__c,
IF (RNC__c&lt;4, NCQ__c*NCR1I__c,0)))</formula>
        <name>Update Target Earning NCQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_YTD_QEarnings</fullName>
        <field>YTDCE__c</field>
        <formula>AE100B__c + AECCQ__c + AENCQ__c + AEAQ__c + AEQQ__c+ AEQCCQ1__c + AEQCCQ2__c + AEQCCQ3__c +  AEQCCQ4__c + EC1__c +EC2__c +EC3__c</formula>
        <name>Update YTD Quota Earnings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set Monthly Quotas P11-P12</fullName>
        <actions>
            <name>Update_CW_Quota_P11</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CW_Quota_P12</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quota_FY15__c.CWQ__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Actual Earned NCQ</fullName>
        <actions>
            <name>Update_Actual_Earnings_NCQ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.RNC__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkbox for CST</fullName>
        <actions>
            <name>Check_CST_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_OTH_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Client Service Director,Senior Client Service Manager,Client Service Manager,Educational Consultant</value>
        </criteriaItems>
        <description>Applies to CSD, SCSM, CSM and Educational Consultant</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkbox for Other</fullName>
        <actions>
            <name>Check_OTH_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_CST_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Direct Response Rep,Middlebury Sales Rep,Private School Sales Partner,Renewals Specialist,Channel Sales</value>
        </criteriaItems>
        <description>Applies to DRR, MIL Sales Rep, PS Sales Partner, Renewals Specialist, Channel Managers</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for ISR</fullName>
        <actions>
            <name>Check_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_CST_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_OTH_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Inside Sales Rep</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for Manager</fullName>
        <actions>
            <name>Check_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_CST_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_OTH_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Regional Sales Manager,Sales Vice President,Inside Sales Director,Inside Sales Manager,Middlebury Director</value>
        </criteriaItems>
        <description>Applies to RSM, SVP, ISD, ISM, and MIL Director</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for NAM</fullName>
        <actions>
            <name>Check_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_CST_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_OTH_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>National Account Manager</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Current Customer Quota for ISRs</fullName>
        <actions>
            <name>Update_Current_Customer_Quota_for_ISRs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Inside Sales Rep</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Percent to Annual</fullName>
        <actions>
            <name>Update_Percent_to_Annual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>notEqual</operation>
            <value>Sales Vice President</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Percent to Annual SVP</fullName>
        <actions>
            <name>Update_Percent_to_Annual_SVP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Sales Vice President</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update TE%3A Annual NCQ</fullName>
        <actions>
            <name>Update_Target_Earning_NCQ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.NCQ__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Target Earning Annual Current Customers</fullName>
        <actions>
            <name>Update_Target_Earning_Annual_Current_Cus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.RNC__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Total Quota Earnings</fullName>
        <actions>
            <name>Calculate_YTD_Commish_Earnings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY15__c.RNC__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
