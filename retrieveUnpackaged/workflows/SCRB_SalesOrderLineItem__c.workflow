<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateQuoteLineCheckbox</fullName>
        <field>UpdateQuoteLine__c</field>
        <literalValue>1</literalValue>
        <name>UpdateQuoteLineCheckbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Trigger Update Quote Line</fullName>
        <active>false</active>
        <criteriaItems>
            <field>SCRB_SalesOrderLineItem__c.IsQuoteLineUpdated__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SCRB_SalesOrderLineItem__c.UpdateQuoteLine__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>UpdateQuoteLineCheckbox</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>SCRB_SalesOrderLineItem__c.Time_to_Update_Quote_Line__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
