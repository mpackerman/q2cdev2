<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_Activated_Notification</fullName>
        <description>Contract Activated Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>sbjork@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Contract_Activated_Notification</template>
    </alerts>
    <alerts>
        <fullName>Contract_Approval_Notification</fullName>
        <description>Contract Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Contract_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Contract_Rejection</fullName>
        <description>Contract Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Contract_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Contract_Renewal_Notice</fullName>
        <description>Contract Renewal Notice</description>
        <protected>false</protected>
        <recipients>
            <recipient>kmelfi@kcdistancelearning.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Contract_Renewal_Notice</template>
    </alerts>
    <alerts>
        <fullName>MIL_Contract_Signed_Notification</fullName>
        <ccEmails>ar@middleburyinteractive.com</ccEmails>
        <description>MIL Contract Signed Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/MIL_Contract_Signed_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activate_the_Contract_Record</fullName>
        <field>Status</field>
        <literalValue>Activated</literalValue>
        <name>Activate the Contract Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Contract Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_Closed_Lost</fullName>
        <field>Status</field>
        <literalValue>Closed Lost</literalValue>
        <name>Contract Status - Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_Requested</fullName>
        <field>Status</field>
        <literalValue>Requested</literalValue>
        <name>Contract Status - Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Terminated_Date</fullName>
        <field>Terminate_Date__c</field>
        <formula>now()</formula>
        <name>Contract Terminated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_Renewal_Forecast_Record</fullName>
        <field>SBQQ__RenewalForecast__c</field>
        <literalValue>1</literalValue>
        <name>Create Renewal Forecast Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Legal_Name_of_Entity</fullName>
        <field>Legal_Name_of_Entity__c</field>
        <formula>Account.Organization_Name__c</formula>
        <name>Set Legal Name of Entity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Delivery_State</fullName>
        <field>Service_Delivery_State__c</field>
        <formula>SBQQ__Opportunity__r.Service_Delivery_State__c</formula>
        <name>Update Service Delivery State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contract Activated</fullName>
        <actions>
            <name>Create_Renewal_Forecast_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Activated Notification</fullName>
        <actions>
            <name>Contract_Activated_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Reconciliation Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Legacy_Contract__c</field>
            <operation>notEqual</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends a notification email to the Contract owner (Sales rep) and Contract Admin that the contract was Activated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Created</fullName>
        <actions>
            <name>Activate_the_Contract_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Create_Renewal_Forecast_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Contract_Request_Summary__c != &apos;Test class text to prevent new renewal opportunity&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contract Renewal Notice</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Alarm__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Renewal_Notice</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.Review_By__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Status - Closed Lost</fullName>
        <actions>
            <name>Contract_Status_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract.Opportunity_Stage__c</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Terminated Date</fullName>
        <actions>
            <name>Contract_Terminated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Terminated,Superseded</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MIL Contract Signed Notification</fullName>
        <actions>
            <name>MIL_Contract_Signed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Returned - Customer Executed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.MIL_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>used to notify Holly Fagan of a contract returned signed for MIL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Legal Name of Entity</fullName>
        <actions>
            <name>Set_Legal_Name_of_Entity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>used to copy the Org name from the account to the contract</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Service Delivery State on Contract</fullName>
        <actions>
            <name>Update_Service_Delivery_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.CreatedDate</field>
            <operation>notEqual</operation>
            <value>12/31/1899 9:00 PM</value>
        </criteriaItems>
        <description>Checking for create date not equal to 1900 so workflow fires</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Create_Contract</fullName>
        <assignedTo>kfaison@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please create a contract for this approved contract request.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Create Contract</subject>
    </tasks>
</Workflow>
