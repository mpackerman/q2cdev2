<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Keystone_SA_Final_Attempt_Template</fullName>
        <description>Keystone SA Final Attempt Template</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Keystone_Admissions_Reps/Final_Unable_Following_Up_NEW</template>
    </alerts>
    <alerts>
        <fullName>Keystone_SA_Nice_Speaking_with_You_Template</fullName>
        <description>Keystone SA Nice Speaking with You Template</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Keystone_Admissions_Reps/Keystone_Nice_Speaking_to_you_NEW</template>
    </alerts>
    <alerts>
        <fullName>Keystone_SA_Unable_to_Reach_Template</fullName>
        <description>Keystone SA Unable to Reach Template</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Keystone_Admissions_Reps/TKS_Unable_Following_Up_NEW</template>
    </alerts>
    <alerts>
        <fullName>Keystone_SA_Welcome_Template</fullName>
        <description>Keystone SA Welcome Template</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Keystone_Admissions_Reps/Keystone_Welcome_Email_NEW</template>
    </alerts>
    <fieldUpdates>
        <fullName>AccountTypeLeadUpdate</fullName>
        <field>account_type__c</field>
        <literalValue>Middle School</literalValue>
        <name>Account Type Lead Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Back_to_marketing_to_Bono</fullName>
        <field>OwnerId</field>
        <lookupValue>nbono@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Back to marketing to Bono</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BrandUpdate</fullName>
        <field>Brand__c</field>
        <literalValue>KCR</literalValue>
        <name>Brand Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CampusUpdate</fullName>
        <field>campus__c</field>
        <name>Campus Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CampusUpdateMCoOL</fullName>
        <field>campus__c</field>
        <literalValue>AVP MCoOL</literalValue>
        <name>Campus Update- MCoOL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CompanyUpdate</fullName>
        <field>Company</field>
        <formula>FirstName  &amp; &apos; &apos;  &amp; LastName</formula>
        <name>Company Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Conference_Name_to_Entry_Point</fullName>
        <field>entry_point__c</field>
        <formula>TEXT( Conference_Name__c )</formula>
        <name>Conference Name to Entry Point</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Last_Name_To_School_Household</fullName>
        <field>Company</field>
        <formula>LastName</formula>
        <name>Copy Last Name To School_Household</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_School_Name_To_School_Household</fullName>
        <field>Company</field>
        <formula>School_Name__c</formula>
        <name>Copy School Name To School_Household</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_what_grade_are_you_interested_Update</fullName>
        <field>In_what_grade_are_you_interested_Hide__c</field>
        <formula>fall2008Grade__c</formula>
        <name>In what grade - Hide Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCRLeadRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KCR_Lead_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>KCR LeadRecordType Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneBrandUpdate</fullName>
        <field>Brand_hide__c</field>
        <formula>case( Brand__c ,&quot;Keystone&quot;,&quot;Keystone&quot;,&quot;&quot;)</formula>
        <name>Keystone Brand Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneCampusUpdate</fullName>
        <field>Campus_hide__c</field>
        <formula>case( campus__c ,&quot;KCR&quot;,&quot;KCR&quot;,&quot;TKS&quot;,&quot;TKS&quot;,&quot;&quot;)</formula>
        <name>Keystone Campus Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneEntryPointUpdate</fullName>
        <field>Entry_Point_hide__c</field>
        <formula>entry_point__c</formula>
        <name>Keystone Entry Point Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneLeadBrandCampusUpdate</fullName>
        <description>Action updates the custom field &quot;Lead Brand/Campus&quot; later used to drive workflows upon Lead Conversion.</description>
        <field>Lead_Brand_Campus__c</field>
        <formula>case(Brand__c,&quot;Keystone&quot;,&quot;Keystone&quot;,&quot;&quot;)+case(campus__c,&quot;KCR&quot;,&quot;KCR&quot;,&quot;TKS&quot;,&quot;TKS&quot;,&quot;&quot;)</formula>
        <name>Keystone Lead Brand/Campus Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneLeadRoleUpdate</fullName>
        <field>Lead_Role__c</field>
        <formula>Case( Form_Role_Description__c ,&quot;Parent&quot;,&quot;Parent&quot;,&quot;Student&quot;,&quot;Student&quot;,&quot;School Official&quot;,&quot;School Official&quot;,&quot;&quot;)</formula>
        <name>Keystone Lead Role Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneStudentCityUpdate</fullName>
        <field>Student_City__c</field>
        <formula>City</formula>
        <name>Keystone Student City Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneStudentCountryUpdate</fullName>
        <field>Student_Country__c</field>
        <formula>Country</formula>
        <name>Keystone Student Country Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneStudentStateUpdate</fullName>
        <field>Student_State_Province__c</field>
        <formula>State</formula>
        <name>Keystone Student State Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneStudentStreetUpdate</fullName>
        <description>Updates Student Street with Parent Street if left blank.</description>
        <field>Student_Street__c</field>
        <formula>Street</formula>
        <name>Keystone Student Street Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KeystoneStudentZipUpdate</fullName>
        <field>Student_Zip_Postal_Code__c</field>
        <formula>PostalCode</formula>
        <name>Keystone Student Zip Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LeadCompany</fullName>
        <description>Updates Lead Company with First Name &amp; Last Name.  This will set the Account Name when the Convert button is activated.</description>
        <field>Company</field>
        <formula>FirstName  &amp; &apos; &apos; &amp;  LastName</formula>
        <name>Lead Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LeadRecordTypeKCRPARENT</fullName>
        <description>Assigns Parent lead record type for forms that are entered with a ROLE assigned as PARENT</description>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_Parent_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type KCR - PARENT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LeadRecordTypeLEAD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type- LEAD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LeadRecordTypeTKSPARENT</fullName>
        <description>Assigns PARENT from TKS form to correct lead Record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_Parent_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type- TKS - PARENT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Email_Hide_Update</fullName>
        <field>Lead_Email_Hide__c</field>
        <formula>Email</formula>
        <name>Lead Email Hide Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Record_Type_MMLA_Consumer</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MMLA</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type - MMLA (Consumer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LearnBop_Agent_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>brian@learnbop.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>LearnBop Agent Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordTypeUPdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type UPdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Type_Elementary</fullName>
        <field>account_type__c</field>
        <literalValue>Elementary School</literalValue>
        <name>Set Account Type Elementary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Type_High</fullName>
        <field>account_type__c</field>
        <literalValue>High School</literalValue>
        <name>Set Account Type High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Type_Middle</fullName>
        <field>account_type__c</field>
        <literalValue>Middle School</literalValue>
        <name>Set Account Type Middle</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_AFC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>AFC</literalValue>
        <name>Set IRC AFC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_BBC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>BBC</literalValue>
        <name>Set IRC BBC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_CC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>CC</literalValue>
        <name>Set IRC CC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_CSLT</fullName>
        <field>IRC_Company__c</field>
        <literalValue>CSLT</literalValue>
        <name>Set IRC CSLT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_ETL</fullName>
        <field>IRC_Company__c</field>
        <literalValue>ETL</literalValue>
        <name>Set IRC ETL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_ETP</fullName>
        <field>IRC_Company__c</field>
        <literalValue>ETP</literalValue>
        <name>Set IRC ETP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_EduTech</fullName>
        <field>IRC_Company__c</field>
        <literalValue>EduTech</literalValue>
        <name>Set IRC EduTech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_JST</fullName>
        <field>IRC_Company__c</field>
        <literalValue>JST</literalValue>
        <name>Set IRC JST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_Montage</fullName>
        <field>IRC_Company__c</field>
        <literalValue>Montage</literalValue>
        <name>Set IRC Montage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_PT</fullName>
        <field>IRC_Company__c</field>
        <literalValue>PT</literalValue>
        <name>Set IRC PT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_SFL</fullName>
        <field>IRC_Company__c</field>
        <literalValue>SFL</literalValue>
        <name>Set IRC SFL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_TE21</fullName>
        <field>IRC_Company__c</field>
        <literalValue>TE21</literalValue>
        <name>Set IRC TE21</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_TLS</fullName>
        <field>IRC_Company__c</field>
        <literalValue>TLS</literalValue>
        <name>Set IRC TLS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_Tech_Ex</fullName>
        <field>IRC_Company__c</field>
        <literalValue>Tech Ex</literalValue>
        <name>Set IRC Tech Ex</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_UMA</fullName>
        <field>IRC_Company__c</field>
        <literalValue>UMA</literalValue>
        <name>Set IRC UMA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Score_60_to_True</fullName>
        <field>Lead_Score_was_60__c</field>
        <literalValue>1</literalValue>
        <name>Set Lead Score &gt;60 to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_BTM</fullName>
        <description>Used to set lead status Back to Marketing on Keystone&apos;s final attempt.</description>
        <field>Status</field>
        <literalValue>Back to Marketing</literalValue>
        <name>Set Lead Status BTM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_In_Progress</fullName>
        <description>Use to set lead status for Keystone lead process.</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Lead Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKSDataProcessingEntryPointUpdate</fullName>
        <field>entry_point__c</field>
        <formula>&quot;Direct Enrollment&quot;</formula>
        <name>TKS Data Processing Entry Point Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKSDataProcessingLeadSourceUpdate</fullName>
        <description>Updates Lead Source to &quot;Direct Enrollment&quot; if the Lead is created by Data Processing.</description>
        <field>LeadSource</field>
        <literalValue>Direct Enrollment</literalValue>
        <name>TKS Data Processing Lead Source Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKSLeadCreatedDateCopy</fullName>
        <field>Lead_Created_Date__c</field>
        <formula>CreatedDate</formula>
        <name>TKS: Lead Created Date Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAccountTypeinLeadForm</fullName>
        <field>account_type__c</field>
        <literalValue>High School</literalValue>
        <name>Update Account Type in Lead Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateKCRSchoolOfficialLeadRecType</fullName>
        <description>Update KCR School Official Lead Rec Type from KCR website forms</description>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_School_Official_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update KCR School Official Lead Rec Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateKCRStudentLeadRecordType</fullName>
        <description>Update KCR Student Lead Record Type based on form criteria from website</description>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update KCR Student Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRepRatingCold</fullName>
        <field>Rep_Rating__c</field>
        <literalValue>Cold</literalValue>
        <name>Update Rep Rating Cold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRepRatingHot</fullName>
        <field>Rep_Rating__c</field>
        <literalValue>Hot</literalValue>
        <name>Update Rep Rating Hot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRepRatingWarm</fullName>
        <field>Rep_Rating__c</field>
        <literalValue>Warm</literalValue>
        <name>Update Rep Rating Warm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTKSSchoolOfficialLeadRecType</fullName>
        <description>Update TKS School Official Lead Rec Type</description>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_School_Official_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update TKS School Official Lead Rec Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTKSStudentLeadRecType</fullName>
        <description>Update TKS Student Lead Rec Type</description>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update TKS Student Lead Rec Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Activity</fullName>
        <field>First_Activity__c</field>
        <formula>now()</formula>
        <name>Update First Activity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Brand_Aventa</fullName>
        <field>Brand__c</field>
        <literalValue>Aventa</literalValue>
        <name>Update Program Brand Aventa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Brand_Keystone</fullName>
        <field>Brand__c</field>
        <literalValue>Keystone</literalValue>
        <name>Update Program Brand Keystone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Campu_KCR</fullName>
        <field>campus__c</field>
        <literalValue>KCR</literalValue>
        <name>Update Program Campus  KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Campus_Aventa</fullName>
        <field>campus__c</field>
        <name>Update Program Campus Aventa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Campus_TKS</fullName>
        <field>campus__c</field>
        <literalValue>TKS</literalValue>
        <name>Update Program Campus TKS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Owner_Aventa</fullName>
        <field>OwnerId</field>
        <lookupValue>dianab@aventalearning.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Program Owner Aventa</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Owner_KCR</fullName>
        <field>OwnerId</field>
        <lookupValue>eletteer@keystonehighschool.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Program Owner KCR</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Owner_TKS</fullName>
        <field>OwnerId</field>
        <lookupValue>TKSNew</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Program Owner TKS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Record_Type_KCR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Program Record Type KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Program_Record_Type_TKS</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Program Record Type TKS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Aventa</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Aventa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Student_City</fullName>
        <field>Student_City__c</field>
        <formula>Student_City__c</formula>
        <name>Update Student City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Student_State</fullName>
        <field>Student_State_Province__c</field>
        <formula>Student_State_Province__c</formula>
        <name>Update Student State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Student_Street</fullName>
        <field>Student_Street__c</field>
        <formula>Street</formula>
        <name>Update Student Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Student_Zip</fullName>
        <field>Student_Zip_Postal_Code__c</field>
        <formula>PostalCode</formula>
        <name>Update Student Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>assign_to_Betsy</fullName>
        <field>OwnerId</field>
        <lookupValue>blichner@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>assign to Betsy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Address Same -Parent</fullName>
        <actions>
            <name>Update_Student_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Student_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Student_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Student_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>equals</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Keystone Credit Recovery - Parent Record Type,Keystone School - Parent Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Program Aventa</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Change_Program__c</field>
            <operation>equals</operation>
            <value>Aventa</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Program_Brand_Aventa</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Campus_Aventa</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Owner_Aventa</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Record_Type_Aventa</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Change Program KCR</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Change_Program__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Program_Brand_Keystone</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Campu_KCR</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Owner_KCR</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Record_Type_KCR</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Change Program iQ TKS</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Change_Program__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Program_Brand_Keystone</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Campus_TKS</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Owner_TKS</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Program_Record_Type_TKS</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Copy Conference Name to Entry Point</fullName>
        <actions>
            <name>Conference_Name_to_Entry_Point</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>equals</operation>
            <value>Aventa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Conference_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Last Name To School_Household</fullName>
        <actions>
            <name>Copy_Last_Name_To_School_Household</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>equals</operation>
            <value>Student,Parent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Last Name to School_Household TKS</fullName>
        <actions>
            <name>Copy_Last_Name_To_School_Household</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy SO Last Name To School_Household</fullName>
        <actions>
            <name>Copy_Last_Name_To_School_Household</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>equals</operation>
            <value>School Official</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.entry_point__c</field>
            <operation>equals</operation>
            <value>KCR Contact Us</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy School Name To School_Household</fullName>
        <actions>
            <name>Copy_School_Name_To_School_Household</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>equals</operation>
            <value>School Official</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.entry_point__c</field>
            <operation>notEqual</operation>
            <value>KCR Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.entry_point__c</field>
            <operation>notEqual</operation>
            <value>Manually Entered</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone Lead Field Updates</fullName>
        <actions>
            <name>KeystoneBrandUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneCampusUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneEntryPointUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneLeadBrandCampusUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneLeadRoleUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>equals</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR,TKS</value>
        </criteriaItems>
        <description>After the record is created the first time this action updates hidden duplicate fields so that data can be converted to both Account and Contact records.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Attempt1</fullName>
        <actions>
            <name>Set_Lead_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Attempt1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Second_Attempt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Attempt1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Attempt2</fullName>
        <actions>
            <name>Set_Lead_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Third_Attempt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Attempt2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Attempt3</fullName>
        <actions>
            <name>Set_Lead_Status_BTM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Attempt3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Left M1</fullName>
        <actions>
            <name>Set_Lead_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Left_M1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Second_Attempt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Left VM1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Left M2</fullName>
        <actions>
            <name>Set_Lead_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Third_Attempt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Left VM2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Left M3</fullName>
        <actions>
            <name>Set_Lead_Status_BTM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Left VM3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Live1</fullName>
        <actions>
            <name>Set_Lead_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Live1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Second_Attempt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Live1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone SA Live2</fullName>
        <actions>
            <name>Set_Lead_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Third_Attempt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Live2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <description>Use to log calls, set up future tasks and set status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Keystone Student Address Update</fullName>
        <actions>
            <name>KeystoneStudentCityUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneStudentCountryUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneStudentStateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneStudentStreetUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KeystoneStudentZipUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Keystone Credit Recovery - Parent Record Type,Keystone School - Parent Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Street</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Student_Street__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates Student Address with the Parent Address if it&apos;s left blank.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type - MMLA %28Consumer%29</fullName>
        <actions>
            <name>Lead_Record_Type_MMLA_Consumer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>equals</operation>
            <value>MMLA</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type- KCR - PARENT</fullName>
        <actions>
            <name>LeadRecordTypeKCRPARENT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>contains</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>contains</operation>
            <value>Parent</value>
        </criteriaItems>
        <description>Assigns PARENT KCR lead Record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type- KCR - SCHOOL OFFICIAL</fullName>
        <actions>
            <name>UpdateKCRSchoolOfficialLeadRecType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>contains</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>contains</operation>
            <value>School Official</value>
        </criteriaItems>
        <description>Assigns SCHOOL OFFICIAL KCR lead Record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type- KCR - STUDENT</fullName>
        <actions>
            <name>UpdateKCRStudentLeadRecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>contains</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>contains</operation>
            <value>Student</value>
        </criteriaItems>
        <description>Assigns STUDENT KCR lead Record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type- TKS - PARENT</fullName>
        <actions>
            <name>LeadRecordTypeTKSPARENT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>contains</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>contains</operation>
            <value>TKS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>contains</operation>
            <value>Parent</value>
        </criteriaItems>
        <description>Assigns PARENT  TKS lead Record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type- TKS - SCHOOL OFFICIAL</fullName>
        <actions>
            <name>UpdateTKSSchoolOfficialLeadRecType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>contains</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>contains</operation>
            <value>School Official</value>
        </criteriaItems>
        <description>Assigns SCHOOL OFFICIAL TKS lead Record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type- TKS - STUDENT</fullName>
        <actions>
            <name>UpdateTKSStudentLeadRecType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>contains</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>contains</operation>
            <value>TKS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Form_Role_Description__c</field>
            <operation>contains</operation>
            <value>Student</value>
        </criteriaItems>
        <description>Assigns STUDENT TKS lead Record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type-FuelEd</fullName>
        <actions>
            <name>LeadRecordTypeLEAD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Brand__c</field>
            <operation>equals</operation>
            <value>K12,Aventa,A+,Middlebury</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Private School Sales</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LearnBop Leads with Learnbop Campaign</fullName>
        <actions>
            <name>LearnBop_Agent_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>LearnBop Leads 2016 - MMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>LearnBop Demo Campaign 2016 - MMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>LearnBop 30 Day Trial 2016 - MMS</value>
        </criteriaItems>
        <description>Assign Learnbop leads when campaign is assigned to:

LearnBop Leads 2016 - MMS
LearnBop 30 Day Trial 2016 - MMS
LearnBop Demo Campaign 2016 - MMS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC AFC</fullName>
        <actions>
            <name>Set_IRC_AFC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>AFC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC BBC</fullName>
        <actions>
            <name>Set_IRC_BBC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Backbone</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC CC</fullName>
        <actions>
            <name>Set_IRC_CC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Colorado</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC CSLT</fullName>
        <actions>
            <name>Set_IRC_CSLT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Cornerstone</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC ETL</fullName>
        <actions>
            <name>Set_IRC_ETL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Technology Learning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC ETP</fullName>
        <actions>
            <name>Set_IRC_ETP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Technology Partners</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC EduTech</fullName>
        <actions>
            <name>Set_IRC_EduTech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>EduTech</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC JST</fullName>
        <actions>
            <name>Set_IRC_JST</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Job Skill</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC Montage</fullName>
        <actions>
            <name>Set_IRC_Montage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Montage</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC PT</fullName>
        <actions>
            <name>Set_IRC_PT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>PTCSI</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC SFL</fullName>
        <actions>
            <name>Set_IRC_SFL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Systems for Learning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC TE21</fullName>
        <actions>
            <name>Set_IRC_TE21</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>TE21</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC TLS</fullName>
        <actions>
            <name>Set_IRC_TLS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>The Learning System</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC Tech Ex</fullName>
        <actions>
            <name>Set_IRC_Tech_Ex</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Tech-Excellence</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC UMA</fullName>
        <actions>
            <name>Set_IRC_UMA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR  2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>UMA</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Learning Partners</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Systems for Learning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Lead Score %3E60 to True</fullName>
        <actions>
            <name>Set_Lead_Score_60_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.mkto2__Lead_Score__c</field>
            <operation>greaterOrEqual</operation>
            <value>60</value>
        </criteriaItems>
        <description>This is used to set the check box to true so it can be used in the lead assignment rules so leads won&apos;t get reassigned back to the queue if the score goes below 60 and  a users has already engaged the lead.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TKS Data Processing Lead Source Update</fullName>
        <actions>
            <name>TKSDataProcessingEntryPointUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Lead Source to &quot;Direct Enrollment&quot; if a Data Processing user creates the lead.</description>
        <formula>CreatedBy.UserRole.Name = &quot;Data Processing&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TKS%3A Lead Created Date Copy</fullName>
        <actions>
            <name>TKSLeadCreatedDateCopy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Created_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update First Activity</fullName>
        <actions>
            <name>Update_First_Activity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( PRIORVALUE(Status),&quot;Open&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Rep Rating Cold</fullName>
        <actions>
            <name>UpdateRepRatingCold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Rating</field>
            <operation>equals</operation>
            <value>Cold</value>
        </criteriaItems>
        <description>used to update rep rating with rating so we can map to contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Rep Rating Hot</fullName>
        <actions>
            <name>UpdateRepRatingHot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Rating</field>
            <operation>equals</operation>
            <value>Hot</value>
        </criteriaItems>
        <description>used to update rep rating with rating so we can map to contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Rep Rating Warm</fullName>
        <actions>
            <name>UpdateRepRatingWarm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Rating</field>
            <operation>equals</operation>
            <value>Warm</value>
        </criteriaItems>
        <description>used to update rep rating with rating so we can map to contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Attempt1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Attempt1</subject>
    </tasks>
    <tasks>
        <fullName>Called_no_answer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Called no answer</subject>
    </tasks>
    <tasks>
        <fullName>Conversation_with_Decision_Maker</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Conversation with decision maker</subject>
    </tasks>
    <tasks>
        <fullName>Fourth_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>4</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Fourth Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Left_M1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Left M1</subject>
    </tasks>
    <tasks>
        <fullName>Left_VM</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Left VM</subject>
    </tasks>
    <tasks>
        <fullName>Live1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Live1</subject>
    </tasks>
    <tasks>
        <fullName>Second_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Second Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Third_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>4</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Third Attempt</subject>
    </tasks>
</Workflow>
