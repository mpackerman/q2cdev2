<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Create_Parature_Ticket_New_Sales_Order</fullName>
        <ccEmails>clientservice@getfueled.com</ccEmails>
        <description>Create Parature Ticket New Sales Order</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_New_Sales_Order</template>
    </alerts>
    <alerts>
        <fullName>Learnbop_SO_Fulfilled_Email</fullName>
        <description>Learnbop SO Fulfilled Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>lcrua@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>muhl@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/learnbop_Sales_Order_Fulfilled</template>
    </alerts>
    <alerts>
        <fullName>New_Sales_Order_for_Account_In_Review_Notification</fullName>
        <ccEmails>clientservice@k12.com</ccEmails>
        <description>New Sales Order for Account In Review Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_Sales_Order_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_SO_Owner</fullName>
        <description>Notify SO Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_New_Owner_SO</template>
    </alerts>
    <alerts>
        <fullName>Notify_Sales_Order_approval_requester_of_approval</fullName>
        <description>Notify Sales Order approval requester of approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Sales_Order_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Sales_Order_rejection</fullName>
        <description>Notify Sales Order rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Sales_Order_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Sales_order_escalation_East_Central</fullName>
        <description>Sales order escalation East Central</description>
        <protected>false</protected>
        <recipients>
            <recipient>CSD</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Sales_Order_Escalation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Bill_To_Name</fullName>
        <field>Bill_To_Name__c</field>
        <formula>Contract__r.Legal_Name_of_Entity__c</formula>
        <name>Set Bill To Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Execution_Date</fullName>
        <field>DocumentDate__c</field>
        <formula>TODAY()</formula>
        <name>Set Execution Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New</fullName>
        <field>StatusCode__c</field>
        <literalValue>New</literalValue>
        <name>Set New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejected</fullName>
        <field>StatusCode__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sales_order_to_Needs_Approval</fullName>
        <field>StatusCode__c</field>
        <literalValue>Needs Approval</literalValue>
        <name>Set Sales order to Needs Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_on_Button_Push</fullName>
        <field>StatusCode__c</field>
        <literalValue>In Fulfillment</literalValue>
        <name>Set Status on Button Push</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_recalled_back_to_new</fullName>
        <field>StatusCode__c</field>
        <literalValue>New</literalValue>
        <name>Set recalled back to new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To_Org_ID</fullName>
        <field>Org_ID__c</field>
        <formula>Bill_To_Org_ID__c</formula>
        <name>Update Bill To Org ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Email</fullName>
        <field>Owner_Email__c</field>
        <formula>Owner_Email_Hidden__c</formula>
        <name>Update Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SO_CS_Owner_Email</fullName>
        <field>Account_Client_Service_Owner_Email__c</field>
        <formula>Account_Client_Service_Owner_EmailHide__c</formula>
        <name>Update SO CS Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SO_to_Approved_In_Fullfillment</fullName>
        <field>StatusCode__c</field>
        <literalValue>Approved - In Fullfillment</literalValue>
        <name>Update SO to Approved - In Fullfillment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account In Review Notification</fullName>
        <actions>
            <name>New_Sales_Order_for_Account_In_Review_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Used to notify the reconciliation team that an SO was created on an account still in review status</description>
        <formula>ISPICKVAL(AccountId__r.Audit_Review_Status__c, &quot;In Review&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Learnbop SO Fulfilled Email</fullName>
        <actions>
            <name>Learnbop_SO_Fulfilled_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.StatusCode__c</field>
            <operation>equals</operation>
            <value>Fulfilled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LearnBop_Products_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Notifies Monica and Lisa Crua when a Sales Order has been fulfilled with LearnBop products.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify New Owner SO</fullName>
        <actions>
            <name>Notify_SO_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.Notify_New_Owner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Bill To Name</fullName>
        <actions>
            <name>Set_Bill_To_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.Bill_To_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Copies Legal_Name_of_Entity from the Contract and populate the Bill to name on the Sales order</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Execution Date</fullName>
        <actions>
            <name>Set_Execution_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.StatusCode__c</field>
            <operation>equals</operation>
            <value>Approved - In Fullfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Bill To Org ID</fullName>
        <actions>
            <name>Update_Bill_To_Org_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.Bill_To_Org_ID__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Owner Email</fullName>
        <actions>
            <name>Update_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.Owner_Email_Hidden__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SO CS Owner Email</fullName>
        <actions>
            <name>Update_SO_CS_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SCRB_SalesOrder__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
