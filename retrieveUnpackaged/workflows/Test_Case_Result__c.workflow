<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notif_for_New_Test_Case_Result</fullName>
        <description>Notif for New Test Case Result</description>
        <protected>false</protected>
        <recipients>
            <recipient>dchen@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>whamilton@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Test_Case_Result_Created</template>
    </alerts>
    <alerts>
        <fullName>Notif_for_Test_Case_Result_Needs_Action</fullName>
        <description>Notif for Test Case Result Needs Action</description>
        <protected>false</protected>
        <recipients>
            <recipient>dchen@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>srutherford@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>whamilton@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Test_Case_Result_Needs_Action</template>
    </alerts>
    <rules>
        <fullName>Notif for Test Case Result Needs Action</fullName>
        <actions>
            <name>Notif_for_Test_Case_Result_Needs_Action</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Case_Result__c.Test_Result__c</field>
            <operation>equals</operation>
            <value>Ready for Re-Test,Clarification Needed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notif for newly created Test Case Result</fullName>
        <actions>
            <name>Notif_for_New_Test_Case_Result</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Case_Result__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Test_Case_Result__c.CreatedById</field>
            <operation>notContain</operation>
            <value>Windy</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
