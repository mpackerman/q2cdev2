<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Coastal_Cloud_Cases_Email_Alert</fullName>
        <description>Coastal Cloud Cases Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Coastal_Cloud</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>rrobinson.fueled@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Coastal_Cloud_New_Case_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_A</fullName>
        <ccEmails>clientservice@getfueled.com, dsullens@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up A+</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_A</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_Aventa</fullName>
        <ccEmails>orders@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up Aventa</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_Aventa</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_ICR</fullName>
        <ccEmails>clientservice@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up ICR</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_ICR</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_ICR_K_5_Instruction</fullName>
        <ccEmails>clientservice@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up: ICR K-5 Instruction</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_ICR_K_5_Instruction</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_K12</fullName>
        <ccEmails>clientservice@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up K12</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_K12</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_LearnBop</fullName>
        <ccEmails>clientservice@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up: LearnBop</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_LearnBop</template>
    </alerts>
    <alerts>
        <fullName>Create_Parature_Ticket_Set_Up_MIL</fullName>
        <ccEmails>clientservice@getfueled.com</ccEmails>
        <description>Create Parature Ticket Set Up MIL</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Create_Parature_Ticket_Set_Up_MIL</template>
    </alerts>
    <alerts>
        <fullName>MIL_PD_notification</fullName>
        <description>MIL PD notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>tbruns@middil.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_MIL_PD_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_EC_of_training_case_with_Consulting_Day</fullName>
        <description>Notify EC of training case with Consulting Day</description>
        <protected>false</protected>
        <recipients>
            <recipient>dcawthorn@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mtrappe@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rabboud@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>roffterdinger@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_EC_of_New_Training</template>
    </alerts>
    <alerts>
        <fullName>Notify_Marketing_of_case_with_Marketing_info_complete</fullName>
        <description>Notify Marketing of case with Marketing info complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>ksullivan1@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Notify_marketing_of_Case_with_completed_info</template>
    </alerts>
    <alerts>
        <fullName>Notify_Orders_of_New_Case</fullName>
        <ccEmails>orders@getfueled.com</ccEmails>
        <description>Notify Orders of New Case</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Training_Request_is_Ready_for_Sales_Order</template>
    </alerts>
    <alerts>
        <fullName>Notify_Training_of_New_Case</fullName>
        <description>Notify Training of New Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>jmarron@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lballew@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lcrunden@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ljones@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mfluharty@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Notify_Training_of_New_Case</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Training_Case_to_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Training_Requests</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Training Case to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Audit_to_Change</fullName>
        <field>Reason</field>
        <literalValue>Change request</literalValue>
        <name>Set Account Audit to Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Audit_to_New</fullName>
        <field>Reason</field>
        <literalValue>New Account</literalValue>
        <name>Set Account Audit to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Subject</fullName>
        <field>Subject</field>
        <formula>Case_Subject_Hidden__c</formula>
        <name>Set Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Subject_new</fullName>
        <field>Subject</field>
        <formula>New_Case_Subject_Hidden__c</formula>
        <name>Set Subject (new)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Training_Case_Subject</fullName>
        <field>Subject</field>
        <formula>&quot;Training Request:&quot; &amp;&quot; &quot;&amp; Account.Name</formula>
        <name>Set Training Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Billing_Address</fullName>
        <field>Billing_Address__c</field>
        <formula>Account.BillingStreet &amp; BR() &amp;
Account.BillingCity &amp; &quot;, &quot; &amp; Account.BillingState &amp; &quot; &quot; &amp;  Account.BillingPostalCode &amp;BR()  &amp;  Account.BillingCountry</formula>
        <name>Update Case Billing Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_School_End_Date</fullName>
        <field>School_End_Date__c</field>
        <formula>Opportunity__r.Estimated_Service_End_Date__c</formula>
        <name>Update Case School End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_School_Start_Date</fullName>
        <field>School_Start_Date__c</field>
        <formula>Opportunity__r.Estimated_Service_Start_Date__c</formula>
        <name>Update Case School Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Shipping_Address</fullName>
        <field>Shipping_Address__c</field>
        <formula>Account.ShippingStreet &amp; BR() &amp;
Account.ShippingCity &amp; &quot;, &quot; &amp; Account.ShippingState &amp; &quot; &quot; &amp;  Account.ShippingPostalCode &amp;BR()  &amp;  Account.ShippingCountry</formula>
        <name>Update Case Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Training_Address</fullName>
        <field>Training_Address__c</field>
        <formula>Account.BillingStreet &amp; BR() &amp;
Account.BillingCity &amp; &quot;, &quot; &amp; Account.BillingState &amp; &quot; &quot; &amp; Account.BillingPostalCode &amp;BR() &amp; Account.BillingCountry</formula>
        <name>Update Case Training Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO</fullName>
        <field>Needs_Custom_Development__c</field>
        <literalValue>1</literalValue>
        <name>Update PO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign Training Case to Queue and send email</fullName>
        <actions>
            <name>Notify_Training_of_New_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Training_Case_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Training Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Coastal Cloud New Case Assignment</fullName>
        <actions>
            <name>Coastal_Cloud_Cases_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Coastal Cloud Cases</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket Set Up%3A A%2B</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_A</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: A+</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket Set Up%3A Aventa</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_Aventa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: Aventa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket Set Up%3A K12</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_K12</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket Set Up%3A LearnBop</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_LearnBop</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: LearnBop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket Set Up%3A MIL</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_MIL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket for ICR</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_ICR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Instruction_Info_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Parature Ticket for ICR K-5 Instruction</fullName>
        <actions>
            <name>Create_Parature_Ticket_Set_Up_ICR_K_5_Instruction</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Instruction_Model__c</field>
            <operation>equals</operation>
            <value>Blended,We Teach</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Grade_Levels_Served__c</field>
            <operation>includes</operation>
            <value>K-5</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Product Update Tasks</fullName>
        <actions>
            <name>Contact_marketing_team_for_updates_to_internal_external_documentation</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Create_product_in_PEAK_or_applicable_system</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Review_Approve_product_attributes_for_Finance</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Review_approve_product_attributes_for_Implementation_and_Renewals</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Submit_Salesforce_Support_Case_for_Creating_Updating_Product_Details_in_Salesfor</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Submit_Salesforce_Support_Case_for_Steelbrick</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Update_Contract_related_information_and_language_for_product_updates_case</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Update_Oracle</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Create_Product_Update_Tasks__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Product Update Tasks 2</fullName>
        <actions>
            <name>Coordinate_change_management_for_open_opps</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Review_approve_product_attributes_for_Sales_Ops</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Submit_Salesforce_Support_Case_for_Reporting_Dashboards</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Submit_Ticket_for_BEE_NDS</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Create_Product_Update_Tasks__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Task for Clever ID</fullName>
        <actions>
            <name>Ensure_Client_Creates_Clever_Account_and_Provides_Clever_ID_for_Setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SIS_Integration_New__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify EC of New Training Case</fullName>
        <actions>
            <name>Notify_EC_of_training_case_with_Consulting_Day</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Training Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Training_Type__c</field>
            <operation>equals</operation>
            <value>Consulting Day</value>
        </criteriaItems>
        <description>Notifies EC users when the training type is Consulting Day</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Orders Team of New Training Case</fullName>
        <actions>
            <name>Notify_Orders_of_New_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Training Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Training_Type__c</field>
            <operation>notEqual</operation>
            <value>On Demand</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify marketing of Case with completed info</fullName>
        <actions>
            <name>Notify_Marketing_of_case_with_Marketing_info_complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5 OR 6 OR 7 OR 8)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: Aventa,Set Up: K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Ready for Fulfillment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.School_Name_for_website__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SAMS_Acronym__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.School_Home_Page_URL_K12_to_build__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.School_Home_Page_URL_School_Site__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Model__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Availability__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A A%2B Fulfill Shipment</fullName>
        <actions>
            <name>Onboarding_Fulfill_Shipment_A</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: A+</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.A_Products__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Creates a task to validate the PO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create MIL Training Tasks for PD</fullName>
        <actions>
            <name>Schedule_PD</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Training_Purchase_Notes__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create MIL Training Tasks for PD Mid Atlantic</fullName>
        <actions>
            <name>Schedule_Program_Planning_Call_Kelly</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 5 AND (2 or 3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PM_Manager_of__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Teacher_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Custom_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>KY,NC,VA,DC,DE,MD</value>
        </criteriaItems>
        <description>create tasks for Kelly Sokol</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create MIL Training Tasks for PD MidWest</fullName>
        <actions>
            <name>Schedule_Program_Planning_Call_Stephanie</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 5 AND (2 or 3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PM_Manager_of__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Teacher_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Custom_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>IN,IL,MO,KS,NE,IA,WI,MN,SD,ND</value>
        </criteriaItems>
        <description>create tasks for Stephanie Ramos</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create MIL Training Tasks for PD North</fullName>
        <actions>
            <name>Schedule_Program_Planning_Call_Cory</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 5 AND (2 or 3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PM_Manager_of__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Teacher_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Custom_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>NY,ME,VT,NH,MA,RI,CT,NJ,PA,WV,OH,MI</value>
        </criteriaItems>
        <description>create tasks for Abigail Mendenhall</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create MIL Training Tasks for PD South</fullName>
        <actions>
            <name>Schedule_Program_Planning_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 5 AND (2 or 3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PM_Manager_of__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Teacher_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Custom_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>SC,TN,GA,AL,MS,AR,FL,LA,OK,TX</value>
        </criteriaItems>
        <description>create tasks for Marie Keith</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create MIL Training Tasks for PD West</fullName>
        <actions>
            <name>Schedule_Program_Planning_Call_Katie</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 5 AND (2 or 3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: MIL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PM_Manager_of__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Teacher_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Custom_Training_of_Teachers__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>CA,AZ,NM,CO,UT,NV,WY,ID,OR,MT,WA,AK,HI</value>
        </criteriaItems>
        <description>create tasks for Katie Peluso</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for Aventa Products</fullName>
        <actions>
            <name>Submit_PEAK_Account_Administrator_Access_Form</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Aventa_Products__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>used to trigger tasks for all Aventa products</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for Aventa Products no Instruction</fullName>
        <actions>
            <name>Onboarding_Provide_Course_Enrollment_Spreadsheet_to_Client_Aventa</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Aventa_Products__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Aventa_Instruction__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for Contact Instructions and Account Summary for MIL</fullName>
        <actions>
            <name>Onboarding_Send_Enrollment_Spreadsheet_and_Instructions_to_Customer_MIL</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Submit_Account_Summary_Information_Form_MIL</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Powerspeak_MIL_Products__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for Full Marketing</fullName>
        <actions>
            <name>Onboarding_EPR_for_Schools_with_Full_Marketing_Recruitment_is_completed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 or 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.K12_Full_Marketing_Student_Recruitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Aventa_Full_Mktg_Student_Recruitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>In a client onboarding case, this create a task for tasks assigned to marketing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for Instruction for K12 Products</fullName>
        <actions>
            <name>Onboarding_Schedule_Instruction_Onboarding_with_ICR_K12</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Validation_of_Fullfillment_of_Instructional_Resources_K12</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.K12_Instruction__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for K12 Instruction</fullName>
        <actions>
            <name>Onboarding_SARF_for_IST_K12</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Validation_of_Instructional_Resources_K12</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.K12_Instruction__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Tasks for K12 SARF</fullName>
        <actions>
            <name>Submit_SARF_for_Main_Administrator_K12</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.K12__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create Welcome Tasks</fullName>
        <actions>
            <name>Onboarding_Complete_Welcome_Call</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Schedule_Welcome_Call</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Send_Client_Welcome_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <description>In a client onboarding case, this creates welcome tasks for all Onboards.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Create a Task for the Marketing Call</fullName>
        <actions>
            <name>Onboarding_Marketing_Website_is_Completed</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Schedule_Marketing_Call</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Setup_Student_Recruitment</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 or 3 or 4 or 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.K12_Full_Marketing_Student_Recruitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Aventa_Full_Mktg_Student_Recruitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Aventa_Online_Marketing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.K12_Online_Marketing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>In a client onboarding case, this create a task for scheduling a marketing call if the Opportunity has student recruitment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB%3A Notify  for PD training MIL</fullName>
        <actions>
            <name>MIL_PD_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MIL_Onsite_Training__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send alert to Molly Whitmire &amp; Becky Rhea when PD is purchased—heads up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding%3A LearnBop Submit Training Case</fullName>
        <actions>
            <name>OnboardingSubmit_Onsite_Training_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.LearnBop_Onsite_Training__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding%3A LearnBop Training</fullName>
        <actions>
            <name>Onboarding_Provide_Training_Resources_to_Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: LearnBop</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding%3A LearnBop Training Aventa Set Up</fullName>
        <actions>
            <name>Onboarding_Provide_Training_Resources_to_Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Set Up: Aventa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.LearnBop_Students__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Audit to Change</fullName>
        <actions>
            <name>Set_Account_Audit_to_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Salesforce Account Audit: Account Info Update Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Audit to New</fullName>
        <actions>
            <name>Set_Account_Audit_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Salesforce Account Audit: A new Account has been created</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Subject %28new%29</fullName>
        <actions>
            <name>Set_Subject_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Onboard: CSM,Set Up: A+,Set Up: Aventa,Set Up: K12,Set Up: MIL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Training Case Subject</fullName>
        <actions>
            <name>Set_Training_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Training Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Billing Address</fullName>
        <actions>
            <name>Update_Case_Billing_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Shipping Address</fullName>
        <actions>
            <name>Update_Case_Shipping_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Contact_marketing_team_for_updates_to_internal_external_documentation</fullName>
        <assignedToType>creator</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contact marketing team for updates to internal/external documentation</subject>
    </tasks>
    <tasks>
        <fullName>Coordinate_action_needed_for_previously_contracted_clients</fullName>
        <assignedTo>mjeldridge@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Coordinate action needed for previously contracted clients</subject>
    </tasks>
    <tasks>
        <fullName>Coordinate_change_management_for_open_opps</fullName>
        <assignedTo>dodavid@getfueled.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Coordinate change management for open opps</subject>
    </tasks>
    <tasks>
        <fullName>Create_product_in_PEAK_or_applicable_system</fullName>
        <assignedTo>jsantini@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Create product in PEAK (or applicable system)</subject>
    </tasks>
    <tasks>
        <fullName>Ensure_Client_Creates_Clever_Account_and_Provides_Clever_ID_for_Setup</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ensure Client Creates Clever Account and Provides Clever ID for Setup</subject>
    </tasks>
    <tasks>
        <fullName>OnboardingSubmit_Onsite_Training_Case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Submit Onsite Training Case</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Complete_Welcome_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Complete Welcome Call</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_EPR_for_Schools_with_Full_Marketing_Recruitment_is_completed</fullName>
        <assignedTo>ksullivan1@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: EPR for Schools with Full Marketing/Recruitment is completed</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Ensure_Client_Creates_Clever_Account_and_Provides_Clever_ID_for_Setup</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Ensure Client Creates Clever Account and Provides Clever ID for Setup</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Fulfill_Shipment_A</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Fulfill Shipment (A+)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Marketing_Website_is_Completed</fullName>
        <assignedTo>ksullivan1@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Marketing Website is Completed</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Phone_and_Marketing_Follow_Up_10_Months_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder to follow up with the client and email them marketing materials. This task recurs every two months for the first 10 months after enrollment.</description>
        <dueDateOffset>300</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Actual_Enrollment_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Phone and Marketing Follow Up, 10 Months (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Phone_and_Marketing_Follow_Up_2_Months_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder to follow up with the client and email them marketing materials. This task recurs every two months for the first 10 months after enrollment.</description>
        <dueDateOffset>60</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Actual_Enrollment_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Phone and Marketing Follow Up, 2 Months (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Phone_and_Marketing_Follow_Up_4_Months_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder to follow up with the client and email them marketing materials. This task recurs every two months for the first 10 months after enrollment.</description>
        <dueDateOffset>120</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Actual_Enrollment_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Phone and Marketing Follow Up, 4 Months (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Phone_and_Marketing_Follow_Up_6_Months_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder to follow up with the client and email them marketing materials. This task recurs every two months for the first 10 months after enrollment.</description>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Actual_Enrollment_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Phone and Marketing Follow Up, 6 Months (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Phone_and_Marketing_Follow_Up_8_Months_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder to follow up with the client and email them marketing materials. This task recurs every two months for the first 10 months after enrollment.</description>
        <dueDateOffset>240</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Actual_Enrollment_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Phone and Marketing Follow Up, 8 Months (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Provide_Course_Enrollment_Spreadsheet_to_Client_Aventa</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Provide Course Enrollment Spreadsheet to Client  (Aventa)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Provide_Training_Resources_to_Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Provide Training Resources to Customer</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_SARF_for_IST_K12</fullName>
        <assignedToType>owner</assignedToType>
        <description>Link for SARF: http://support.onlineschoolsolutions.com/FileManagement/Download/8c1334afec7e404086e492e5d8742f31</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: SARF for IST (K12)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Schedule_Instruction_Onboarding_with_ICR_K12</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Schedule Instruction Onboarding with ICR (K12)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Schedule_Marketing_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Schedule Marketing Call</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Schedule_Program_Consulting_A</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Schedule Program Consulting (A+)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Schedule_Welcome_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Schedule Welcome Call</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Send_Client_Welcome_Email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Send Client Welcome Email</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Send_Enrollment_Spreadsheet_and_Instructions_to_Customer_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <description>Use the &quot;Contact and Enrollment&quot; template based on the number of SCE - greater or less than 45 SCE.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Send Enrollment Spreadsheet and Instructions to Customer (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Send_alert_to_Molly_Whitmire_Becky_Rhea_when_PD_is_purchased_heads_up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Send alert to Molly Whitmire &amp; Becky Rhea when PD is purchased—heads up</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Setup_Student_Recruitment</fullName>
        <assignedTo>ksullivan1@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Setup Student Recruitment</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Submit_Account_Summary_Information_Form_MIL</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Submit Account Summary Information Form (MIL)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Validation_of_Fullfillment_of_Instructional_Resources_K12</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Validation of Fullfillment of Instructional Resources (K12)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Validation_of_Instructional_Resources_K12</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Validation of Instructional Resources (K12)</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Verify_Scheduling_of_All_On_site_Trainings_A</fullName>
        <assignedToType>owner</assignedToType>
        <description>Once the appropriate information for on-site training has been completed on the setup case and the case has been set to “Ready for Fulfillment”, the training team will solve applicable Parature ticket, verifying the date they have chosen for the training</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding: Verify Scheduling of All On-site Trainings (A+)</subject>
    </tasks>
    <tasks>
        <fullName>Review_Approve_product_attributes_for_Finance</fullName>
        <assignedTo>azier1@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review/Approve product attributes for Finance</subject>
    </tasks>
    <tasks>
        <fullName>Review_Approve_product_attributes_for_Legal_Contracts</fullName>
        <assignedTo>mjeldridge@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review/Approve product attributes for Legal/Contracts</subject>
    </tasks>
    <tasks>
        <fullName>Review_approve_product_attributes_for_Implementation_and_Renewals</fullName>
        <assignedTo>jsantini@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review/approve product attributes for Implementation and Renewals</subject>
    </tasks>
    <tasks>
        <fullName>Review_approve_product_attributes_for_Sales_Ops</fullName>
        <assignedTo>dodavid@getfueled.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review/approve product attributes for Sales Ops</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_PD</fullName>
        <assignedTo>tbruns@middil.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule PD</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Program_Planning_Call</fullName>
        <assignedTo>mkeith@middil.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Program Planning Call</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Program_Planning_Call_Cory</fullName>
        <assignedTo>amendenhall@middil.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Program Planning Call</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Program_Planning_Call_Katie</fullName>
        <assignedTo>kpeluso@middleburyinteractive.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Program Planning Call</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Program_Planning_Call_Kelly</fullName>
        <assignedTo>ksokol@middil.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Program Planning Call</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Program_Planning_Call_Stephanie</fullName>
        <assignedTo>sramos1@middleburyinteractive.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Program Planning Call</subject>
    </tasks>
    <tasks>
        <fullName>Submit_PEAK_Account_Administrator_Access_Form</fullName>
        <assignedToType>owner</assignedToType>
        <description>Attach form to SF Account page. Submit a Parature ticket ONLY if attached after Aventa Program is onboarded. Link for Peak Access Form: http://support.onlineschoolsolutions.com/FileManagement/Download/0c89937f5c604cd892152b317e01019d</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit PEAK Account Administrator Access Form</subject>
    </tasks>
    <tasks>
        <fullName>Submit_SARF_for_Main_Administrator_K12</fullName>
        <assignedToType>owner</assignedToType>
        <description>Link for SARF: http://support.onlineschoolsolutions.com/FileManagement/Download/8c1334afec7e404086e492e5d8742f31</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit SARF for Main Administrator (K12)</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Salesforce_Support_Case_for_Creating_Updating_Product_Details_in_Salesfor</fullName>
        <assignedTo>efaris@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Salesforce Support Case for Creating/Updating Product Details in Salesforce</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Salesforce_Support_Case_for_Reporting_Dashboards</fullName>
        <assignedTo>dodavid@getfueled.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Salesforce Support Case for Reporting/Dashboards</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Salesforce_Support_Case_for_Steelbrick</fullName>
        <assignedToType>creator</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Salesforce Support Case for Steelbrick</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Salesforce_Support_Case_for_language_on_auto_generated_contract_template</fullName>
        <assignedTo>mjeldridge@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Salesforce Support Case for language on auto-generated contract template</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Ticket_for_BEE_NDS</fullName>
        <assignedTo>kpearson@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Ticket for BEE/NDS</subject>
    </tasks>
    <tasks>
        <fullName>Update_Contract_related_information_and_language_for_product_updates_case</fullName>
        <assignedTo>kfaison@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Update Contract related information and language for product updates case</subject>
    </tasks>
    <tasks>
        <fullName>Update_Oracle</fullName>
        <assignedTo>azier1@k12.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Update Oracle</subject>
    </tasks>
</Workflow>
