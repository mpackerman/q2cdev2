<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <fieldUpdates>
        <fullName>Assign_CR_task_to_Ada</fullName>
        <description>Used for CR instruction contract project</description>
        <field>OwnerId</field>
        <lookupValue>aortiz-mckee@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign CR task to Ada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complete_Date_Time</fullName>
        <field xsi:nil="true"/>
        <formula>NOW()</formula>
        <name>Complete Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_New_Account_Request_reassign_to_Shan</fullName>
        <field>OwnerId</field>
        <lookupValue>rrobinson.fueled@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC New Account Request reassign to Shan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_contract_reassign</fullName>
        <description>used to assign IRC contract request to Shanna</description>
        <field>OwnerId</field>
        <lookupValue>lacook@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC contract reassign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_contract_reassign_Erik</fullName>
        <field>OwnerId</field>
        <lookupValue>eelorriaga@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC contract reassign Erik</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_contract_reassign_Justin</fullName>
        <field>OwnerId</field>
        <lookupValue>jflaitz@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC contract reassign Justin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_contract_reassign_Shanna</fullName>
        <field>OwnerId</field>
        <lookupValue>rrobinson.fueled@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC contract reassign Shanna</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_pricing_reassign</fullName>
        <description>used to reassign pricing request to Shanna</description>
        <field>OwnerId</field>
        <lookupValue>lacook@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC pricing reassign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_pricing_reassign_Erik</fullName>
        <field>OwnerId</field>
        <lookupValue>eelorriaga@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC pricing reassign Erik</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_pricing_reassign_Justin</fullName>
        <field>OwnerId</field>
        <lookupValue>jflaitz@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC pricing reassign Justin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRC_pricing_reassign_Shanna</fullName>
        <field>OwnerId</field>
        <lookupValue>rrobinson.fueled@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>IRC pricing reassign Shanna</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Call_Outcome_to_Attempt</fullName>
        <field>Call_Outcome__c</field>
        <literalValue>Attempt</literalValue>
        <name>Set Call Outcome to Attempt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Call_Outcome_to_LVM</fullName>
        <field>Call_Outcome__c</field>
        <literalValue>Left Voicemail</literalValue>
        <name>Set Call Outcome to LVM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Call_Outcome_to_Live_Conversation</fullName>
        <field>Call_Outcome__c</field>
        <literalValue>Live Conversation</literalValue>
        <name>Set Call Outcome to Live Conversation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Recruitment_Approval_Task</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Student_Recruitment_Approval</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Recruitment Approval Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Aventa_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CallSmart_Task</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Task - Call Smart Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>B2B_Onboarding_Task_Record</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Task Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_for_Email</fullName>
        <field>Aventa_Activity_Type__c</field>
        <literalValue>Email</literalValue>
        <name>Update Type for Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign CR task to Ada</fullName>
        <actions>
            <name>Assign_CR_task_to_Ada</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>CR Instruction Contracts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Send letter</value>
        </criteriaItems>
        <description>Used for CR instruction contract project</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IRC New Account Request reassign to Shanna</fullName>
        <actions>
            <name>IRC_New_Account_Request_reassign_to_Shan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC New Account Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>mpear,roha,gglos,stier,gkugh,sreev,rhein,jbuch,dledf,trich,llitt,svan,jleit,rblac,lkemp,rchoa,lcrum,rjone</value>
        </criteriaItems>
        <description>used to assign IRC New Account Request tasks to Shanna</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IRC contract reassign Erik</fullName>
        <actions>
            <name>IRC_contract_reassign_Erik</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC Contract Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>svan,gkugh</value>
        </criteriaItems>
        <description>used to assign IRC contract request tasks to Erik</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IRC contract reassign Justin</fullName>
        <actions>
            <name>IRC_contract_reassign_Justin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC Contract Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>jleit,rblac,lkemp,rchoa,lcrum,rjone</value>
        </criteriaItems>
        <description>used to assign IRC contract request tasks to Justin</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IRC contract reassign Shanna</fullName>
        <actions>
            <name>IRC_contract_reassign_Shanna</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC Contract Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>mpear,roha,gglos,stier,sreev,rhein,jbuch,dledf,trich,llitt</value>
        </criteriaItems>
        <description>used to assign IRC contract request tasks to Shanna</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IRC pricing reassign Erik</fullName>
        <actions>
            <name>IRC_pricing_reassign_Erik</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>svan,gkugh</value>
        </criteriaItems>
        <description>used to assign IRC pricing request tasks to Erik</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IRC pricing reassign Justin</fullName>
        <actions>
            <name>IRC_pricing_reassign_Justin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>jleit,rblac,lkemp,rchoa,lcrum,rjone</value>
        </criteriaItems>
        <description>used to assign IRC pricing request tasks to Justin</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IRC pricing reassign Shanna</fullName>
        <actions>
            <name>IRC_pricing_reassign_Shanna</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRC Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>mpear,roha,gglos,stier,sreev,jbuch,dledf,trich,llitt</value>
        </criteriaItems>
        <description>used to assign IRC pricing request tasks to Shanna</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TKS Learning Coach%3A Call Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Learning Coach: 1st Call,Learning Coach: 2nd Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Next_Call_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LearningCoachCallReminder</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Task.Next_Call_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Task Record Type</fullName>
        <actions>
            <name>Update_Task_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>used to set B2B rec type on tasks created by action plans</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Type for Email</fullName>
        <actions>
            <name>Update_Type_for_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Email:</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>LearningCoachCallReminder</fullName>
        <assignedToType>owner</assignedToType>
        <description>You indicated that it is time to call this Student again!</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Learning Coach: Call Reminder</subject>
    </tasks>
</Workflow>
