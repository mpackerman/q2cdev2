<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Auto_Renew_check</fullName>
        <field>Auto_Renew__c</field>
        <literalValue>1</literalValue>
        <name>Set Auto Renew check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Auto_Renew_uncheck</fullName>
        <field>Auto_Renew__c</field>
        <literalValue>0</literalValue>
        <name>Set Auto Renew uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set Auto Renew check</fullName>
        <actions>
            <name>Set_Auto_Renew_check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Auto_Renewal_Option__c</field>
            <operation>equals</operation>
            <value>and any renewal period (if applicable).</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Auto Renew uncheck</fullName>
        <actions>
            <name>Set_Auto_Renew_uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Auto_Renewal_Option__c</field>
            <operation>equals</operation>
            <value>and is not eligible for a renewal period.</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
