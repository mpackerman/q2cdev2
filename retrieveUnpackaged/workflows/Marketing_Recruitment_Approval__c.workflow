<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Marketing_Approval_Complete</fullName>
        <description>Marketing Approval Complete</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Marketing_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Approval_Notif</fullName>
        <description>Marketing Approval Notif</description>
        <protected>false</protected>
        <recipients>
            <recipient>jfeatherman1@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Marketing_Approval_Notification_to_Marketing</template>
    </alerts>
    <fieldUpdates>
        <fullName>Copy_Opp_Name</fullName>
        <field>Name</field>
        <formula>Opp_Name_Hidden__c</formula>
        <name>Copy Opp Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Opp Name</fullName>
        <actions>
            <name>Copy_Opp_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Recruitment_Approval__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to Copy Opp name to Mktg request name</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Recruitment Approval Complete</fullName>
        <actions>
            <name>Marketing_Approval_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Recruitment_Approval__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved Online,Approved Full,Approved Downgrade to Online</value>
        </criteriaItems>
        <description>Used to notify users that a approval is complete.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Recruitment Approval Notif</fullName>
        <actions>
            <name>Marketing_Approval_Notif</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Recruitment_Approval__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not yet approved</value>
        </criteriaItems>
        <description>Used to notify marketing that a new approval is waiting.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
