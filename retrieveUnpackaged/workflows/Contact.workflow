<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ContactBrandUpdateAventa</fullName>
        <field>Brand__c</field>
        <literalValue>Aventa</literalValue>
        <name>Contact Brand Update- Aventa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContactBrandUpdateKCR</fullName>
        <field>Brand__c</field>
        <literalValue>KCR</literalValue>
        <name>Contact Brand Update- KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContactBrandUpdateKeystone</fullName>
        <field>Brand__c</field>
        <literalValue>Keystone</literalValue>
        <name>Contact Brand Update- Keystone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContactCityUpdate</fullName>
        <field>MailingCity</field>
        <formula>Account.BillingCity</formula>
        <name>Contact City Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContactStateUpdate</fullName>
        <field>MailingState</field>
        <formula>MailingState</formula>
        <name>Contact State Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContactZipUpdate</fullName>
        <field>MailingPostalCode</field>
        <formula>Account.BillingPostalCode</formula>
        <name>Contact Zip Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_ID_Hide_Update</fullName>
        <field>Contact_ID_Hide__c</field>
        <formula>Id</formula>
        <name>Contact ID Hide Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCRParentRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_Parent_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>KCR Parent Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCRSchoolOfficialRecordTypeCheck</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_School_Official_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>KCR School Official Record Type Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCR_Student_Record_Type_Check</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>KCR Student Record Type Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PersonTypeUpdate</fullName>
        <field>Person_Type__c</field>
        <literalValue>Student</literalValue>
        <name>Person Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Gatekeeper</fullName>
        <field>TransRecvdWorkflowGatekeeper__c</field>
        <literalValue>1</literalValue>
        <name>Set Gatekeeper</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKSParentRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_Parent_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TKS Parent Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKSSchoolOfficialRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_School_Official_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TKS School Official Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKS_Student_Record_Type_Check</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_Student_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TKS Student Record Type Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_created_by_Role</fullName>
        <field>Created_by_Role_Hide__c</field>
        <formula>Created_by_Role__c</formula>
        <name>Update created by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contact Brand Update- Aventa</fullName>
        <actions>
            <name>ContactBrandUpdateAventa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>Aventa</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>Aventa Account Executive</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact Brand Update- Keystone</fullName>
        <actions>
            <name>ContactBrandUpdateKeystone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>Keystone</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>KCR Parent Record Type Check</fullName>
        <actions>
            <name>KCRParentRecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Form_Role_Desc__c</field>
            <operation>equals</operation>
            <value>Parent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone Credit Recovery - Parent Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Brand_Campus__c</field>
            <operation>equals</operation>
            <value>KeystoneKCR</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>KCR School Official Record Type Check</fullName>
        <actions>
            <name>KCRSchoolOfficialRecordTypeCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Form_Role_Desc__c</field>
            <operation>equals</operation>
            <value>School Official</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone Credit Recovery - School Official Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Brand_Campus__c</field>
            <operation>equals</operation>
            <value>KeystoneKCR</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>KCR Student Record Type Check</fullName>
        <actions>
            <name>KCR_Student_Record_Type_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Form_Role_Desc__c</field>
            <operation>equals</operation>
            <value>Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone Credit Recovery - Student Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Brand_Campus__c</field>
            <operation>equals</operation>
            <value>KeystoneKCR</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Person Type Update</fullName>
        <actions>
            <name>PersonTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.parent_first_name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.student_first_name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>contains</operation>
            <value>Plus,iQ</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TKS Parent Record Type Check</fullName>
        <actions>
            <name>TKSParentRecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Form_Role_Desc__c</field>
            <operation>equals</operation>
            <value>Parent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone School - Parent Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Brand_Campus__c</field>
            <operation>equals</operation>
            <value>KeystoneTKS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TKS School Official Record Type Check</fullName>
        <actions>
            <name>TKSSchoolOfficialRecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Form_Role_Desc__c</field>
            <operation>equals</operation>
            <value>School Official</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone School - School Official Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Brand_Campus__c</field>
            <operation>equals</operation>
            <value>KeystoneTKS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TKS Student Record Type Check</fullName>
        <actions>
            <name>TKS_Student_Record_Type_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Form_Role_Desc__c</field>
            <operation>equals</operation>
            <value>Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone School - Student Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Brand_Campus__c</field>
            <operation>equals</operation>
            <value>KeystoneTKS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Transcript Recvd Task</fullName>
        <actions>
            <name>Set_Gatekeeper</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trans_Rec_d</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Transcript_Received_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Student_Status__c</field>
            <operation>notEqual</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TransRecvdWorkflowGatekeeper__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Contact ID Hide</fullName>
        <actions>
            <name>Contact_ID_Hide_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Brand__c</field>
            <operation>equals</operation>
            <value>Keystone</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update created by Role</fullName>
        <actions>
            <name>Update_created_by_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Created_by_Role__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Trans_Rec_d</fullName>
        <assignedToType>owner</assignedToType>
        <description>Used to Auto create Tasks for Outbound reps to follow up</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Trans Rec&apos;d</subject>
    </tasks>
</Workflow>
