<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_Opp_Close_date_reminder</fullName>
        <description>A+ Opp Close date reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>rrobinson.fueled@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>FuelEd_Emails/Opp_Close_date_in_the_past</template>
    </alerts>
    <alerts>
        <fullName>All_FuelEd_Closed_Won_Notification</fullName>
        <ccEmails>joysmith@getfueled.com;</ccEmails>
        <ccEmails>larogers@getfueled.com;</ccEmails>
        <ccEmails>bajones@getfueled.com</ccEmails>
        <description>All FuelEd Closed Won Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>scuccuini@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Aventa_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>B2B_2012_Notify_for_Legacy_Products</fullName>
        <description>B2B 2012 - Notify for Legacy Products</description>
        <protected>false</protected>
        <recipients>
            <recipient>efaris@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_2012_Notify_for_Legacy_Products</template>
    </alerts>
    <alerts>
        <fullName>B2B_2012_Notify_for_MIL_Site_or_Enterprise</fullName>
        <description>B2B 2012 - Notify for MIL Site or Enterprise</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>georgewarren@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MIL_Templates/B2B_MIL_Opp_Verification</template>
    </alerts>
    <alerts>
        <fullName>B2B_A_Closed_Won_Opportunity</fullName>
        <description>B2B - A+ Closed Won Opportunity</description>
        <protected>false</protected>
        <recipients>
            <recipient>hpoints@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>whamilton@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_A_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>B2B_Closed_Lost_NAM</fullName>
        <description>B2B Closed Lost - NAM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>B2B_Closed_Won_NAM</fullName>
        <description>B2B Closed Won - NAM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Aventa_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>B2B_Early_Notif_for_Opportunities_with_Non_Standard_Products</fullName>
        <description>B2B -Early Notif for Opportunities with Non-Standard Products</description>
        <protected>false</protected>
        <recipients>
            <recipient>efaris@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sbjork@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_Non_Standard_Product_Verification</template>
    </alerts>
    <alerts>
        <fullName>B2B_MIL_Early_Notif_for_Site_or_Enterprise_License</fullName>
        <description>B2B - MIL Early Notif for Site or Enterprise License</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MIL_Templates/B2B_MIL_Opp_Verification</template>
    </alerts>
    <alerts>
        <fullName>CS_Blank_Region_Closed_Won_Opportunity_Notification</fullName>
        <description>CS Blank Region Closed Won Opportunity Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CSD</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>dchen@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>glevin@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlinden@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kmelfi@kcdistancelearning.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ksullivan1@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lcollins@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mslone@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sbjork@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>scuccuini@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_Closed_Won_Blank_Area_or_CS_Territory</template>
    </alerts>
    <alerts>
        <fullName>CS_Evans_Services_Region_Closed_Won_Opportunity_Email</fullName>
        <description>CS Evans Services Region Closed Won Opportunity Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>dodavid@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>glevin@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jievans@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lcollins@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mslone@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pneeman@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_Ready_for_Onboarding_Sales_Team</template>
    </alerts>
    <alerts>
        <fullName>CS_Fuhrmeister_Region_Closed_Won_Opportunity_Email</fullName>
        <description>CS FuhrmeisterServices Region Closed Won Opportunity Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>dodavid@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>glevin@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lcollins@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lfuhrmeister@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mslone@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pneeman@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_Ready_for_Onboarding_Sales_Team</template>
    </alerts>
    <alerts>
        <fullName>CS_Johnson_Services_Region_Closed_Won_Opportunity_Email</fullName>
        <description>CS Johnson Services Region Closed Won Opportunity Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>dodavid@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>glevin@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kjohnson@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lcollins@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mslone@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pneeman@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_Ready_for_Onboarding_Sales_Team</template>
    </alerts>
    <alerts>
        <fullName>Closed_Lost_Notification_CSD</fullName>
        <description>Closed Lost Notification CSD</description>
        <protected>false</protected>
        <recipients>
            <recipient>jievans@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Closed_Lost_Notification_MIL</fullName>
        <description>Closed Lost Notification: MIL</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_MIL_MMLA/MIL_OPP_Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Commitment_notification_if_it_is_unchecked</fullName>
        <description>Commitment notification if it is unchecked</description>
        <protected>false</protected>
        <recipients>
            <recipient>bwhiteaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jflaitz@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>knix@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kshurig@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>toddstevenson@power-glide.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Commitment_notification</template>
    </alerts>
    <alerts>
        <fullName>Competitor_Chosen_Needed</fullName>
        <description>Competitor Chosen Needed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Choose_a_Competitor_Needed</template>
    </alerts>
    <alerts>
        <fullName>DRR_Closed_Opp_Notification</fullName>
        <description>DRR Closed Opp Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstalnaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/DRR_Closed_Opp</template>
    </alerts>
    <alerts>
        <fullName>DRR_Stage_Alert</fullName>
        <description>DRR Stage Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstalnaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/DRR_Opp_Stage_Alert</template>
    </alerts>
    <alerts>
        <fullName>ELL_Closed_Won_Notification</fullName>
        <description>ELL Closed Won Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lacook@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_ELL_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Opportunities_where_stage_changed_from_Closed_Won</fullName>
        <description>Email Alert for Opportunities where stage changed from &apos;Closed Won&apos;</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dchen@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dodavid@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rrobinson.fueled@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/FuelEd_Closed_Won_Opp_has_changed_Status</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_commitment_in_opportunity_object</fullName>
        <description>Email Notification commitment in opportunity object</description>
        <protected>false</protected>
        <recipients>
            <recipient>bwhiteaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jflaitz@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>knix@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kshurig@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>toddstevenson@power-glide.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Commitment_notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_alert_Josh_for_New_opportunity_for_same_account</fullName>
        <description>Email Notification to alert Josh for New opportunity for same account</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstalnaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/New_opp_created_for_same_account</template>
    </alerts>
    <alerts>
        <fullName>IRC_Closed_Lost_notif</fullName>
        <description>IRC Closed Lost notif</description>
        <protected>false</protected>
        <recipients>
            <recipient>jrailey@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lacook@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tshively@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/IRC_Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>IRC_new_Opp_notifcation</fullName>
        <description>IRC new Opp notifcation</description>
        <protected>false</protected>
        <recipients>
            <recipient>alanderson@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jrailey@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lacook@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tshively@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_IRC_New_Opp_Created</template>
    </alerts>
    <alerts>
        <fullName>ISR_Closed_Won_Opportunities_Alert</fullName>
        <description>ISR Closed Won Opportunities Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstalnaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ISR_Closed_Won_Opportunities</template>
    </alerts>
    <alerts>
        <fullName>ISR_DRR_ISS_closed_won_notification</fullName>
        <description>ISR/DRR/ISS closed won notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstalnaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/ISR_DRR_ISS_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>LearnBop_Closed_Won_Notification</fullName>
        <description>LearnBop Closed Won Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhammel@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bharani1@learnbop.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mslone@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>muhl@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_LB_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>MIL_Legacy_Product_Notification</fullName>
        <description>MIL Legacy Product Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lacook@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_MIL_MMLA/MIL_Legacy_Product_Notification</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Approval_reminder</fullName>
        <description>Marketing Approval reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Marketing_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>New_Opp_on_Old_Account</fullName>
        <ccEmails>accountsupport@k12.com</ccEmails>
        <description>New Opp on Old Account</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_opportunty_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_Renewal_Opportunity_Created</fullName>
        <description>New Renewal Opportunity Created</description>
        <protected>false</protected>
        <recipients>
            <field>Owner_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Owner_s_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SteelBrick_Templates/New_Renewal_Forecast_Opportunity_Created</template>
    </alerts>
    <alerts>
        <fullName>Notify_MIL_Instruction</fullName>
        <description>Notify MIL Instruction</description>
        <protected>false</protected>
        <recipients>
            <recipient>bgaunce@middleburyinteractive.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_MIL_Instruction_Products</template>
    </alerts>
    <alerts>
        <fullName>Notify_for_Aventa_Enterprise</fullName>
        <description>Notify for Aventa Enterprise</description>
        <protected>false</protected>
        <recipients>
            <recipient>harmstrong@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lcollins@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_Enterprise_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_on_ISR_DRR_ISS_Closed_Lost</fullName>
        <description>Notify on ISR/DRR/ISS Closed Lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstalnaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ISR_DRR_ISS_Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Closed_Won_Notification_for_PS_MIL</fullName>
        <description>Onboarding: Closed Won Notification for PS-MIL</description>
        <protected>false</protected>
        <recipients>
            <recipient>cschaedler@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lwarden@middleburyinteractive.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/B2B_MIL_Ready_for_Onboarding</template>
    </alerts>
    <alerts>
        <fullName>Opp_Close_date_reminder</fullName>
        <description>Opp Close date reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Opp_Close_date_in_the_past</template>
    </alerts>
    <alerts>
        <fullName>Opp_Product_reminder</fullName>
        <description>Opp Product reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Aventa_Opportunty_Product_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Opportunty_Hot_pending_Account_In_Review_Notification</fullName>
        <ccEmails>clientservice@k12.com</ccEmails>
        <description>Opportunty Hot/pending Account In Review Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/Opp_Stage_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>PresenceLearning_Closed_Won</fullName>
        <ccEmails>sophie@presencelearning.com, sdr@presencelearning.com, steve@presencelearning.com,julie@presencelearning.com</ccEmails>
        <description>PresenceLearning Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>aleamon@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_PresenseLearning_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>PresenceLearning_Pending</fullName>
        <ccEmails>sophie@presencelearning.com,mark@presencelearning.com, steve@presencelearning.com,julie@presencelearning.com</ccEmails>
        <description>PresenceLearning Pending</description>
        <protected>false</protected>
        <recipients>
            <recipient>aleamon@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/B2B_PresenseLearning_Pending</template>
    </alerts>
    <alerts>
        <fullName>Renewal_Closed_Lost_Notification_Sales</fullName>
        <description>Renewal Closed Lost Notification:  Sales</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>VP_Sales_and_CS</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Renewal_Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Renewal_Closed_Lost_Notification_West_Sales</fullName>
        <description>Renewal Closed Lost Notification: West Sales</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Renewal_Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Renewal_Opportunity_Closed_Lost_Notification</fullName>
        <ccEmails>renewals@k12.com</ccEmails>
        <description>Renewal Opportunity Closed Lost Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Order_Processing/Renewal_Opportunity_Cl_notif</template>
    </alerts>
    <alerts>
        <fullName>Renewal_Prospect_Notification_East</fullName>
        <description>Renewal Prospect Notification: East Sales</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>VP_Sales_and_CS</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Renewal_Prospect</template>
    </alerts>
    <alerts>
        <fullName>Renewal_Prospect_Notification_East_Coastal_Client_Services</fullName>
        <description>Renewal Prospect Notification: East Central Client Services</description>
        <protected>false</protected>
        <recipients>
            <recipient>CSD</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Renewal_Prospect</template>
    </alerts>
    <alerts>
        <fullName>Renewal_Prospect_Notification_West_Sales</fullName>
        <description>Renewal Prospect Notification: West Sales</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Renewal_Prospect</template>
    </alerts>
    <alerts>
        <fullName>Sales_East_Midwest_Region_Closed_Won_Email</fullName>
        <description>Sales East/Midwest Region Closed Won Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eelorriaga@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kshurig@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Aventa_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Sales_Region_Flaitz_Closed_Lost</fullName>
        <description>Sales Region Flaitz Closed Lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>jflaitz@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>knix@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Sales_Region_Shurig_Closed_Lost</fullName>
        <description>Sales Region Shurig Closed Lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>eelorriaga@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kshurig@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Sales_Region_Stevenson_Closed_Lost</fullName>
        <description>Sales Region Stevenson Closed Lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>bwhiteaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>toddstevenson@power-glide.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost</template>
    </alerts>
    <alerts>
        <fullName>Sales_Southeast_Southwest_Region_Closed_Won_Email</fullName>
        <description>Sales Southeast/Southwest Region Closed Won Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jflaitz@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>knix@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Aventa_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Sales_West_Region_Closed_Won_Email</fullName>
        <description>Sales West Region Closed Won Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bwhiteaker@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>toddstevenson@power-glide.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Onboarding_Notifs/Aventa_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Start_Date_Close_Date_Exception</fullName>
        <description>Start Date Close Date Exception</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Opp_Close_date_Start_Date_Exception</template>
    </alerts>
    <alerts>
        <fullName>Won_reason_Required</fullName>
        <description>Won Reason Required</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Won_Reason_Required</template>
    </alerts>
    <alerts>
        <fullName>opp_status_closed_won_requested_send_email</fullName>
        <description>when opportunity status closed won requested - send email to queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>kfaison@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SteelBrick_Templates/Closed_Won_Requested</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Ascertain_Pain</fullName>
        <field>Ascertain_Pain_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Check Ascertain Pain Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_DNA_Complete</fullName>
        <field>DNA_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Check DNA Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Discovery</fullName>
        <field>Discovery_Qualification_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Check Discovery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Needs_Complete</fullName>
        <field>Needs_Problems_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Check Needs Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Solutions_Complete</fullName>
        <field>Solutions_Benefit_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Check Solutions Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_lost_date_change</fullName>
        <description>Change close date to current date</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Closed lost date change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Commitment_Date_Update</fullName>
        <field>Commitment_Date__c</field>
        <formula>NOW()</formula>
        <name>Commitment Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ever_Committed</fullName>
        <field>Ever_Committed__c</field>
        <literalValue>1</literalValue>
        <name>Ever Committed?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Insert_FY16_Quota_ID</fullName>
        <field>Quota_FY16_ID__c</field>
        <formula>Account.Owner.Quota_FY16_ID__c</formula>
        <name>Insert FY16 Quota ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Change_Date</fullName>
        <field>Monthly_Close_Status_Change__c</field>
        <formula>today()</formula>
        <name>MCS - Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Closed_Status</fullName>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>MCS - Closed Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Closed_Won_Status</fullName>
        <description>Update Monthly Close Status to Closed Won</description>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Won</literalValue>
        <name>MCS - Closed Won Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Current_Status</fullName>
        <description>Set Monthly Close Status to Current</description>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Current</literalValue>
        <name>MCS - Current Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Future_Status</fullName>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Future</literalValue>
        <name>MCS - Future Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Lost_Status</fullName>
        <description>Update Monthly Close Status to Lost</description>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Lost</literalValue>
        <name>MCS - Lost Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_Push_Status</fullName>
        <description>Monthly Close Status Update to Push</description>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Push</literalValue>
        <name>MCS - Push Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCS_RESET</fullName>
        <field>Monthly_Close_Status__c</field>
        <literalValue>RESET</literalValue>
        <name>MCS - RESET</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>McS_Created</fullName>
        <description>Update Monthly Close Status to Created</description>
        <field>Monthly_Close_Status__c</field>
        <literalValue>Created</literalValue>
        <name>MCS - Created Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Price_Qoute_Proposal</fullName>
        <field>StageName</field>
        <literalValue>Proposal</literalValue>
        <name>Milestone - Price Qoute - Proposal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Price_Quote_Probability</fullName>
        <description>B2B</description>
        <field>Probability</field>
        <formula>0.50</formula>
        <name>Milestone-Price Quote-Probability 50</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Requested_Contract_Contract</fullName>
        <description>B2B</description>
        <field>StageName</field>
        <literalValue>Contract</literalValue>
        <name>Milestone-Requested Contract-Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Requested_Contract_Pr75</fullName>
        <description>B2B</description>
        <field>Probability</field>
        <formula>0.75</formula>
        <name>Milestone-Requested Contract-Pr75</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Verbal_Pr80</fullName>
        <description>B2B</description>
        <field>Probability</field>
        <formula>0.80</formula>
        <name>Milestone-Verbal-Pr80</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Webinar_Probability</fullName>
        <description>B2B</description>
        <field>StageName</field>
        <literalValue>Active Meetings</literalValue>
        <name>Milestone - Webinar - Active Meetings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_Webinar_Probability_25</fullName>
        <description>B2B</description>
        <field>Probability</field>
        <formula>0.25</formula>
        <name>Milestone-Webinar-Pr25</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OppRecordTypeKCR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp Record Type KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Name_Update</fullName>
        <field>Name</field>
        <formula>Account.Name &amp; &quot; - &quot; &amp; TEXT(Type) &amp; &quot; - &quot; &amp; TEXT(Preliminary_Interest__c) &amp; &quot; - &quot; &amp; Launch_Quarter_Hidden__c</formula>
        <name>Opportunity Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReSet_Notify_A</fullName>
        <field>Notify_A__c</field>
        <literalValue>No</literalValue>
        <name>ReSet Notify A+</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_FuelEd</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type - FuelEd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contracted_to_True</fullName>
        <field>SBQQ__Contracted__c</field>
        <literalValue>1</literalValue>
        <name>Set Contracted to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_AFC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>AFC</literalValue>
        <name>Set IRC AFC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_BBC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>BBC</literalValue>
        <name>Set IRC BBC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_CC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>CC</literalValue>
        <name>Set IRC CC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_CSLT</fullName>
        <field>IRC_Company__c</field>
        <literalValue>CSLT</literalValue>
        <name>Set IRC CSLT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_ETL</fullName>
        <field>IRC_Company__c</field>
        <literalValue>ETL</literalValue>
        <name>Set IRC ETL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_ETP</fullName>
        <field>IRC_Company__c</field>
        <literalValue>ETP</literalValue>
        <name>Set IRC ETP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_EduTech</fullName>
        <field>IRC_Company__c</field>
        <literalValue>EduTech</literalValue>
        <name>Set IRC EduTech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_JST</fullName>
        <field>IRC_Company__c</field>
        <literalValue>JST</literalValue>
        <name>Set IRC JST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_Montage</fullName>
        <field>IRC_Company__c</field>
        <literalValue>Montage</literalValue>
        <name>Set IRC Montage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_NKEC</fullName>
        <field>IRC_Company__c</field>
        <literalValue>NKEC</literalValue>
        <name>Set IRC NKEC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_PT</fullName>
        <field>IRC_Company__c</field>
        <literalValue>PT</literalValue>
        <name>Set IRC PT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_SFL</fullName>
        <field>IRC_Company__c</field>
        <literalValue>SFL</literalValue>
        <name>Set IRC SFL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_TE21</fullName>
        <field>IRC_Company__c</field>
        <literalValue>TE21</literalValue>
        <name>Set IRC TE21</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_TLS</fullName>
        <field>IRC_Company__c</field>
        <literalValue>TLS</literalValue>
        <name>Set IRC TLS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IRC_UMA</fullName>
        <field>IRC_Company__c</field>
        <literalValue>UMA</literalValue>
        <name>Set IRC UMA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notify_A</fullName>
        <field>Notify_A__c</field>
        <literalValue>Yes</literalValue>
        <name>Set Notify A+</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Op_gate_keeper</fullName>
        <field>OppWorkflowGatekeeper__c</field>
        <literalValue>1</literalValue>
        <name>Set Op gate keeper</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_Email</fullName>
        <field>Owner_s_Email__c</field>
        <formula>Owner.Email</formula>
        <name>Set Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_Manager_Email</fullName>
        <field>Owner_s_Manager_s_Email__c</field>
        <formula>Owner.Manager.Email</formula>
        <name>Set Owner Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Renewal_Type</fullName>
        <field>Type</field>
        <literalValue>Renewal</literalValue>
        <name>Set Renewal Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sety_IRC_Tech</fullName>
        <field>IRC_Company__c</field>
        <literalValue>Tech Ex</literalValue>
        <name>Sety IRC TechEx</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StagetoQualification</fullName>
        <description>Updates the Stage field to Qualification when the Enrollment Form is recieved for iQ AZ</description>
        <field>StageName</field>
        <literalValue>Qualification</literalValue>
        <name>Stage to Qualification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPDATE_SCE_OPP</fullName>
        <field>Semester_Course_Enrollments__c</field>
        <formula>ESTIMATED_SCE__c</formula>
        <name>UPDATE SCE - OPP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnCheck_Ascertain_Pain_Complete</fullName>
        <field>Ascertain_Pain_Complete__c</field>
        <literalValue>0</literalValue>
        <name>UnCheck Ascertain Pain Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnCheck_Discovery_Complete</fullName>
        <field>Discovery_Qualification_Complete__c</field>
        <literalValue>0</literalValue>
        <name>UnCheck Discovery Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnCheck_Needs_Complete</fullName>
        <field>Needs_Problems_Complete__c</field>
        <literalValue>0</literalValue>
        <name>UnCheck Needs Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_DNA_Complete</fullName>
        <field>DNA_Complete__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck DNA Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOppRecordTypeAventa</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Opp Record Type-B2B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateShiptoName</fullName>
        <description>Update Ship to Name using Opportunity Name</description>
        <field>Name</field>
        <formula>Name</formula>
        <name>Update Ship to Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CS1_EMP_ID</fullName>
        <field>CS1EID__c</field>
        <formula>Account.Client_Service_Owner__r.Employee_number__c</formula>
        <name>Update CS1 EMP ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CS2_Percent</fullName>
        <field>CS2p__c</field>
        <formula>1</formula>
        <name>Update CS2 Percent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CSM1_Name</fullName>
        <field>CS1__c</field>
        <formula>Account.Client_Service_Owner__r.FirstName &amp;&quot; &quot;&amp; Account.Client_Service_Owner__r.LastName</formula>
        <name>Update CSM1 Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Date_Closed_Won</fullName>
        <field>CloseDate</field>
        <formula>NOW()</formula>
        <name>Update Closed Date Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Cs1p</fullName>
        <field>CS1_p__c</field>
        <formula>1</formula>
        <name>Update Cs1p</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FTS_EID_to_CS2</fullName>
        <field>CS2EID__c</field>
        <formula>Account.FTS_Manager__r.Employee_number__c</formula>
        <name>Update FTS EID to CS2 EID field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FTS_Name_to_CS_2_field</fullName>
        <field>CS2__c</field>
        <formula>Account.FTS_Manager__r.FirstName &amp;&quot; &quot;&amp; Account.FTS_Manager__r.LastName</formula>
        <name>Update FTS Name to CS 2 field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ISS_EID</fullName>
        <field>ISSEID__c</field>
        <formula>Account.ISS_Owner__r.Employee_number__c</formula>
        <name>Update ISS EID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ISS_Name</fullName>
        <field>ISS__c</field>
        <formula>Account.ISS_Owner__r.FirstName &amp;&quot; &quot;&amp; Account.ISS_Owner__r.LastName</formula>
        <name>Update ISS Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ISSp</fullName>
        <field>ISSp__c</field>
        <formula>1</formula>
        <name>Update ISSp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NAM1_ID</fullName>
        <field>NAM1_EID__c</field>
        <formula>Account.Owner.Employee_number__c</formula>
        <name>Update NAM1 ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NAM1_Name</fullName>
        <field>NAM1__c</field>
        <formula>Account.Owner.FirstName&amp;&quot; &quot; &amp;Account.Owner.LastName</formula>
        <name>Update NAM1 Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NAM1p</fullName>
        <field>NAM1p__c</field>
        <formula>1</formula>
        <name>Update NAM1p</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NAM_Owner_Request</fullName>
        <field>Renewal_Opp_Owner__c</field>
        <formula>&quot;True&quot;</formula>
        <name>Update NAM Owner Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Total_Price_SPIFF_1_as_Total_Pric</fullName>
        <field>Spiff_1_Amount__c</field>
        <formula>Total_Price_ELL__c</formula>
        <name>Update Total Price SPIFF 1 as Total Pric</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>unCheck_Solutions_Complete</fullName>
        <field>Solutions_Benefit_Complete__c</field>
        <literalValue>0</literalValue>
        <name>unCheck Solutions Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>ISDC_Opportunity_Won</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://powerstandings.insidesales.com/kpi/oppwon</endpointUrl>
        <fields>Id</fields>
        <fields>LastModifiedById</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>kmelfi@kcdistancelearning.com</integrationUser>
        <name>ISDC - Opportunity Won</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>ISDC_Qualified_Opportunity</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://powerstandings.insidesales.com/kpi/qualopp</endpointUrl>
        <fields>Id</fields>
        <fields>LastModifiedById</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>kmelfi@kcdistancelearning.com</integrationUser>
        <name>ISDC - Qualified Opportunity</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Add FY16 Quota ID to Opportunity</fullName>
        <actions>
            <name>Insert_FY16_Quota_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The ID is only added at the time of creation. It must be updated if someone requests a change to the Sales 1 person.</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>All FuelEd Closed Won</fullName>
        <actions>
            <name>All_FuelEd_Closed_Won_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify Sharlene of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - A%2B Closed Won</fullName>
        <actions>
            <name>B2B_A_Closed_Won_Opportunity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_A__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>Notify Windy and Heather when there&apos;s A+ and is Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - Closed Lost Notification to NAM</fullName>
        <actions>
            <name>B2B_Closed_Lost_NAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>send email to a NAM when an Opp they own is marked as Closed Lost</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Closed Lost&quot;),  CONTAINS(Owner.UserRole.Name,&quot;NAM&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - Closed Won Notification to NAM</fullName>
        <actions>
            <name>B2B_Closed_Won_NAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>send email to a NAM when an Opp they own is marked as Closed Won</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Closed Won&quot;),  CONTAINS(Owner.UserRole.Name,&quot;NAM&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - IRC Closed Lost</fullName>
        <actions>
            <name>IRC_Closed_Lost_notif</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or  2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IRC_Company__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IRC_Rep__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Notify when there&apos;s and IRC Company or Rep and is Closed Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - LearnBop Closed Won</fullName>
        <actions>
            <name>LearnBop_Closed_Won_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.LearnBop_Products_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>LearnBop Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - Milestone - Contract Requested</fullName>
        <actions>
            <name>Milestone_Requested_Contract_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>75</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Milestone__c</field>
            <operation>includes</operation>
            <value>Contract Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - Milestone - Price Quote %2F Proposal</fullName>
        <actions>
            <name>Milestone_Price_Qoute_Proposal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>50</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Milestone__c</field>
            <operation>includes</operation>
            <value>Price Quote/Proposal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - Milestone - Verbal Agreement</fullName>
        <actions>
            <name>Milestone_Requested_Contract_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>80</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Milestone__c</field>
            <operation>includes</operation>
            <value>Verbal Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - Milestone - Webinar</fullName>
        <actions>
            <name>Milestone_Webinar_Probability</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Milestone__c</field>
            <operation>includes</operation>
            <value>Webinar</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - PL Closed Won</fullName>
        <actions>
            <name>PresenceLearning_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.PL_Product_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>Notify when there&apos;s PL and is Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B - PL Pending</fullName>
        <actions>
            <name>PresenceLearning_Pending</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.PL_Product_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>Notify when there&apos;s PL and is Pending Stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Notify for Aventa Enterprise</fullName>
        <actions>
            <name>Notify_for_Aventa_Enterprise</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_Aventa_Enterprise__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Notify_MIL_Enterprise__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <description>Sends notification to Lisa Collins and Heather Armstrong when the &quot;Notify for Aventa Enterprise&quot; field is Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Notify for Legacy Products</fullName>
        <actions>
            <name>B2B_2012_Notify_for_Legacy_Products</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_for_Legacy_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <description>Sends notification to ED (for now - later will change to Opp Owner and Opp Last Modified By) when the &quot;Notify for Legacy Products&quot; field is Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Notify for MIL Enterprise</fullName>
        <actions>
            <name>B2B_2012_Notify_for_MIL_Site_or_Enterprise</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_MIL_Enterprise__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST</value>
        </criteriaItems>
        <description>Sends notification to Chad when the &quot;Notify for MIL Enterprise&quot; field is Yes (and account name doesn&apos;t contain TEST)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012 - Notify for MIL Site</fullName>
        <actions>
            <name>B2B_2012_Notify_for_MIL_Site_or_Enterprise</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_MIL_Site__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST</value>
        </criteriaItems>
        <description>Sends notification to Chad when the &quot;Notify for MIL Site&quot; field is Yes (and account name doesn&apos;t contain TEST)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B 2012%3A Notify when Non-std product has been added</fullName>
        <actions>
            <name>B2B_Early_Notif_for_Opportunities_with_Non_Standard_Products</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_for_Non_Standard_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST</value>
        </criteriaItems>
        <description>Sends notification to Ed and Stephanie when the &quot;Notify for Non-Standard Products&quot; field is Yes (and Account Name doesn&apos;t contain TEST&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>B2B A%2B Opp Close date in the past</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Shanna Baccus-Wilson</value>
        </criteriaItems>
        <description>sends notification to the Shanna that an open Opp has a close date in the past.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>A_Opp_Close_date_reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>B2B Opp Close date in the past</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
            <value>Shanna Baccus-Wilson,Darin Wilson</value>
        </criteriaItems>
        <description>sends notification to the opp owner that an open Opp has a close date in the past.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opp_Close_date_reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS Evans Services Region Closed Won Email</fullName>
        <actions>
            <name>CS_Evans_Services_Region_Closed_Won_Opportunity_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>contains</operation>
            <value>Evans</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>Used to notify key users in the Evans region of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Fuhrmeister Services Region Closed Won Email</fullName>
        <actions>
            <name>CS_Fuhrmeister_Region_Closed_Won_Opportunity_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>contains</operation>
            <value>Florida,Fuhrmeister,FL</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>Used to notify key users in the Fuhrmeister region of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Johnson Services Region Closed Won Email</fullName>
        <actions>
            <name>CS_Johnson_Services_Region_Closed_Won_Opportunity_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>contains</operation>
            <value>Johnson,NYC,CHI</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>Used to notify key users in the Johnson and NYC region of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Territory Evans Closed Lost</fullName>
        <actions>
            <name>Closed_Lost_Notification_CSD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>equals</operation>
            <value>Evans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify the Jill in the Evans region of new closed lost opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check Ascertain Pain Complete</fullName>
        <actions>
            <name>Check_Ascertain_Pain</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Ascertain_Pain_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>used by WF rule to check if the fields is complete. Since the Ascertain Pain Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Contracted when Fulfilled</fullName>
        <actions>
            <name>Set_Contracted_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(RecordType.Name = &quot;FuelEd Order&quot;,        ISCHANGED(StageName),        ISPICKVAL(StageName,&quot;Order Fulfilled&quot;)        )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check DNA Complete</fullName>
        <actions>
            <name>Check_DNA_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Discovery_Qualification_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Needs_Problems_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Ascertain_Pain_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Solutions_Benefit_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qualified_By__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qualified_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>used by WF rule to check if the fields is complete. Since the DNA Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Discovery Complete</fullName>
        <actions>
            <name>Check_Discovery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Discovery_Qualification_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>used to check if the fields is complete. Since the Discovery Qualification Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Needs Complete</fullName>
        <actions>
            <name>Check_Needs_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Needs_Problems_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>used to check if the field is complete. Since the Needs / Problems field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Solutions Complete</fullName>
        <actions>
            <name>Check_Solutions_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Solutions_Benefit_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>used by WF rule to check if the fields is complete. Since the Solutions/Benefit Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Choose Competitor Exceptions</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Competitor_Chosen__c</field>
            <operation>equals</operation>
            <value>Currently Unknown,Waiting for RFP Response</value>
        </criteriaItems>
        <description>used to send email to opp owner 30 days after Currently Unknown or Waiting for RFP Response are set on the Opp</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Competitor_Chosen_Needed</name>
                <type>Alert</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Closed Detail Required</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Won_Reason__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used to notify Opp owner that closed won reason is needed for a closed won Opp</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Won_reason_Required</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Closed Won Requested Send Email</fullName>
        <actions>
            <name>opp_status_closed_won_requested_send_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>when opportinity status closed won requested - send email to queue</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Closed Won - Requested&quot;), NOT(PRIORVALUE(StageName) = &quot;Closed Won - Requested&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed lost date change</fullName>
        <actions>
            <name>Closed_lost_date_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Whenever the stage is closed lost then close date field is current date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Commit checkbox Notification</fullName>
        <actions>
            <name>Email_Notification_commitment_in_opportunity_object</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Commitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This email alert is send when opportunity is committed   and uncommitted to sales team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Commitment Date Update</fullName>
        <actions>
            <name>Commitment_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Commitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to update Commitment Date when the Commitment checkbox is updated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Commitment unchecked notification</fullName>
        <actions>
            <name>Commitment_notification_if_it_is_unchecked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Commitment__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DRR Opp Stage Alert</fullName>
        <actions>
            <name>DRR_Stage_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Created_by_Role__c</field>
            <operation>equals</operation>
            <value>Direct Response Representative</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Hot,Pending,Delete</value>
        </criteriaItems>
        <description>sends Josh and email when a DRR created opp is set to Delete, Hot or Pending</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ELL Closed Won</fullName>
        <actions>
            <name>ELL_Closed_Won_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.ELL_Product_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <description>ELL Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ever Committed%3F</fullName>
        <actions>
            <name>Ever_Committed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Commitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISDC - Opportunity Won</fullName>
        <actions>
            <name>ISDC_Opportunity_Won</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISDC - Qualified Opportunity</fullName>
        <actions>
            <name>ISDC_Qualified_Opportunity</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISR%2FDRR%2FISS Created Closed Lost</fullName>
        <actions>
            <name>Notify_on_ISR_DRR_ISS_Closed_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Created_by_Role__c</field>
            <operation>equals</operation>
            <value>Direct Response Representative,Inside Sales Representative,Inside Sales Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>sends Josh and email when a ISR/DRR/ISS Created opp is closed Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISR%2FDRR%2FISS Created Closed Won</fullName>
        <actions>
            <name>ISR_DRR_ISS_closed_won_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Created_by_Role__c</field>
            <operation>equals</operation>
            <value>Direct Response Representative,Inside Sales Representative,Inside Sales Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>sends Josh and email when a ISR/DRR/ISS Created opp is closed won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISR%2FDRR%2FISS Owned Closed Lost</fullName>
        <actions>
            <name>Notify_on_ISR_DRR_ISS_Closed_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>notifies Joshua Stalnaker when an ISR/DRR/ISS Owned opp is closed Lost</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Closed Lost&quot;), OR (Owner.UserRole.Name = &quot;Inside Sales Representative&quot;, Owner.UserRole.Name = &quot;Direct Response Representative&quot;, Owner.UserRole.Name = &quot;Inside Sales Support&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISR%2FDRR%2FISS Owned Closed Won</fullName>
        <actions>
            <name>ISR_DRR_ISS_closed_won_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>notifies Joshua Stalnaker when an ISR/DRR/ISS Owned opp is closed won</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Closed Won&quot;), OR (Owner.UserRole.Name = &quot;Inside Sales Representative&quot;, Owner.UserRole.Name = &quot;Direct Response Representative&quot;, Owner.UserRole.Name = &quot;Inside Sales Support&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Change Date</fullName>
        <actions>
            <name>MCS_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Future,Created,Closed,Lost,Push,Won,Reset,Current</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Closed Lost Status</fullName>
        <actions>
            <name>MCS_Lost_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <description>Update Monthly Close Status to Closed Lost.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Closed Statusv2</fullName>
        <actions>
            <name>MCS_Closed_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) or (1 and 2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Close_Month_Current_Month__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Reset</value>
        </criteriaItems>
        <description>Update Monthly Close Status to Closed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Closed Won Status</fullName>
        <actions>
            <name>MCS_Closed_Won_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) or (3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Future,Created,Push,Reset</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Close_Month_Current_Month__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Monthly Close Status Update to Closed Won</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Create Status</fullName>
        <actions>
            <name>McS_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Monthly Close Status - Update to Create Status</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Current Statusv2</fullName>
        <actions>
            <name>MCS_Current_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) or (3 and 4 and 5)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Close_Month_Current_Month__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Reset</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Close_Month_Current_Month__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <description>Monthly Close Status - Update to Current Status</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Future Statusv2</fullName>
        <actions>
            <name>MCS_Future_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Close_Month_Current_Month__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Reset</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - Push Status</fullName>
        <actions>
            <name>MCS_Push_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Close_Month_Current_Month__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <description>Monthly Close Status - Update to Current Status</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCS - RESET</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Today_Month_First_Day__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Monthly_Close_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MCS_RESET</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Today_Month_First_Day__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MIL Closed Lost Notification</fullName>
        <actions>
            <name>Closed_Lost_Notification_MIL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.MIL_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify Chad when MIL opp is closed lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MIL Closed Won Notification</fullName>
        <actions>
            <name>Onboarding_Closed_Won_Notification_for_PS_MIL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.MIL_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify Chad and Liz when MIL opp is closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MIL Legacy Product Notification</fullName>
        <actions>
            <name>MIL_Legacy_Product_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.MIL_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Notify_for_Legacy_Products__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <description>used to notify Chad when a legacy MIL product is added to an Opp</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Recruitment  Approval Required</fullName>
        <active>true</active>
        <booleanFilter>(1 or 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Notify_Student_Recruitment_Registration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Notify_Student_Recruitment_Online__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Marketing_Approvals__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Notifies Opp owner if an online or full marketing product is added to an Opp and 1 day has passed and still no marketing section filled out on the opp</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Marketing_Approval_reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New Opp on Old Account</fullName>
        <actions>
            <name>New_Opp_on_Old_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>lessThan</operation>
            <value>6/30/2011 9:00 PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Audit_Review_Status__c</field>
            <operation>equals</operation>
            <value>In Review</value>
        </criteriaItems>
        <description>Sends email to the account audit team when an Opp is created on an Account that was created prior to 7/1/11</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Renewal Opportunity</fullName>
        <actions>
            <name>Set_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Owner_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Renewal_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Renewal Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New opp created for same account</fullName>
        <actions>
            <name>Email_Notification_to_alert_Josh_for_New_opportunity_for_same_account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email alert to Josh when new opportunity is created when there are more than one open opportunity for that Account</description>
        <formula>AND( Account.Number_of_Open_Opps__c &gt;= 1,OR (Owner.UserRole.Name = &quot;Inside Sales Representative&quot;, Owner.UserRole.Name = &quot;Direct Response Representative&quot; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify MIL Instruction</fullName>
        <actions>
            <name>Notify_MIL_Instruction</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Notify_MIL_Instruction__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify Chad and Beth if a MIL instruction product is added to an Opp</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunities Closed Won assigned to ISR</fullName>
        <actions>
            <name>ISR_Closed_Won_Opportunities_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.NAM1__c</field>
            <operation>equals</operation>
            <value>Kimberly Zerby,Jabar Rolle,Jonathan Timmons,Ali Beauchamp,Jeff Ward,Christina Johnson,Betsy Lichner,Derek Kopacz,Paige Pierce-Phillips,Ryan Jacob</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunities where stage changed from %27Closed Won%27</fullName>
        <actions>
            <name>Email_Alert_for_Opportunities_where_stage_changed_from_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Used to notify key users when Opportunities are moved from the &quot;Closed Won&quot; stage</description>
        <formula>AND ( Account.Name &lt;&gt; &apos;*TEST*&apos;, RecordTypeId = &apos;012700000001DikAAE&apos;, ISCHANGED(StageName), TEXT(PRIORVALUE(StageName)) = &quot;Closed Won&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Created and MSA Signed</fullName>
        <actions>
            <name>Record_Type_FuelEd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>MSA Signed</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Name</fullName>
        <actions>
            <name>Opportunity_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <description>Creates the Opp name with Account.Name, Preliminary Interest, Type and Launch_Quarter_Hidden</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Record Type-FuelEd</fullName>
        <actions>
            <name>UpdateOppRecordTypeAventa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Brand__c</field>
            <operation>equals</operation>
            <value>Blended,K12,A+,Aventa,Middlebury,PowerSpeak</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>B2B</value>
        </criteriaItems>
        <description>B2B Opportunity Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ReSet Notify A%2B</fullName>
        <actions>
            <name>ReSet_Notify_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.AEC_Products_Roll_Up__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Unchecks  the Notify A+ check box if the AEC product is removed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Renewal Closed Lost Notification%3A Sales</fullName>
        <actions>
            <name>Renewal_Closed_Lost_Notification_Sales</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Institutional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Kromar,Shurig,Flaitz,Stevenson</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>TEST Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Created_by_Role__c</field>
            <operation>equals</operation>
            <value>Order Processing</value>
        </criteriaItems>
        <description>Used to notify Brian and Account Owner of a renewal closed lost.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Region Flaitz Closed Lost</fullName>
        <actions>
            <name>Sales_Region_Flaitz_Closed_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Flaitz</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify the sales manager in the Flaitz region of new closed lsot opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Region Flaitz Closed Won</fullName>
        <actions>
            <name>Sales_Southeast_Southwest_Region_Closed_Won_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Flaitz</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify key users in the Southeast and Southwest region of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Region Shurig Closed Lost</fullName>
        <actions>
            <name>Sales_Region_Shurig_Closed_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Shurig</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify the sales manager in the Shurig region of new closed lsot opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Region Shurig Closed Won Email</fullName>
        <actions>
            <name>Sales_East_Midwest_Region_Closed_Won_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Shurig</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify key users in the East/Midwest region of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Region Stevenson Closed Lost</fullName>
        <actions>
            <name>Sales_Region_Stevenson_Closed_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Stevenson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify the sales manager in the Stevenson region of new closed lsot opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Region Stevenson Closed Won Email</fullName>
        <actions>
            <name>Sales_West_Region_Closed_Won_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Stevenson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Test Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>EdF,JerronT,swehb,KevinM</value>
        </criteriaItems>
        <description>Used to notify key users in the West region of new closed won opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Closed Date on Closed Won</fullName>
        <actions>
            <name>Set_Op_gate_keeper</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Closed_Date_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OppWorkflowGatekeeper__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>sets closed date to the current date when opp is closed won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC AFC</fullName>
        <actions>
            <name>Set_IRC_AFC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>AFC</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC BBC</fullName>
        <actions>
            <name>Set_IRC_BBC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Backbone</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC CC</fullName>
        <actions>
            <name>Set_IRC_CC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Colorado</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC CSLT</fullName>
        <actions>
            <name>Set_IRC_CSLT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Cornerstone</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC ETL</fullName>
        <actions>
            <name>Set_IRC_ETL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Technology Learning</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC ETP</fullName>
        <actions>
            <name>Set_IRC_ETP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Technology Partners</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC EduTech</fullName>
        <actions>
            <name>Set_IRC_EduTech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>EduTech</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC JST</fullName>
        <actions>
            <name>Set_IRC_JST</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Job Skill</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC Montage</fullName>
        <actions>
            <name>Set_IRC_Montage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Montage</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC NKEC</fullName>
        <actions>
            <name>Set_IRC_NKEC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Northern Kane</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC PT</fullName>
        <actions>
            <name>Set_IRC_PT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>PTCSI</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC SFL</fullName>
        <actions>
            <name>Set_IRC_SFL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Systems for Learning</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC TE21</fullName>
        <actions>
            <name>Set_IRC_TE21</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>TE21</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC TLS</fullName>
        <actions>
            <name>Set_IRC_TLS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>The Learning System</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC Tech Ex</fullName>
        <actions>
            <name>Sety_IRC_Tech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Tech-Excellence</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set IRC UMA</fullName>
        <actions>
            <name>Set_IRC_UMA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or  2 or 3</booleanFilter>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>UMA</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Learning Partners</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Systems for Learning</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Notify A%2B</fullName>
        <actions>
            <name>Set_Notify_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.AEC_Products_Roll_Up__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Start Date Close Date Exception</fullName>
        <actions>
            <name>Start_Date_Close_Date_Exception</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>used to notify users if they set the closed date after the start date for non renewal Opps</description>
        <formula>AND(NOT(ISPICKVAL(Type, &quot;Renewal&quot;)),  Estimated_Service_Start_Date__c &lt; CloseDate)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UPDATE SCE - OPP</fullName>
        <actions>
            <name>UPDATE_SCE_OPP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.ESTIMATED_SCE__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnCheck Ascertain Pain Complete</fullName>
        <actions>
            <name>UnCheck_Ascertain_Pain_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Ascertain_Pain_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used by WF rule to check if the fields is complete. Since the Ascertain Pain Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnCheck Discovery Complete</fullName>
        <actions>
            <name>UnCheck_Discovery_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Discovery_Qualification_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used to check if the fields is complete. Since the Discovery Qualification Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnCheck Needs Complete</fullName>
        <actions>
            <name>UnCheck_Needs_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Needs_Problems_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used to check if the field is complete. Since the Needs / Problems field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck DNA Complete</fullName>
        <actions>
            <name>Uncheck_DNA_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2 or 3 or 4 or 5 or 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Discovery_Qualification_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Needs_Problems_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Ascertain_Pain_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Solutions_Benefit_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qualified_By__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qualified_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used by WF rule to check if the fields is complete. Since the DNA Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CS1</fullName>
        <actions>
            <name>Update_CS1_EMP_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CSM1_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Cs1p</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update Xactly-Delta integration field: client services owner</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CS1 and CS2 fields</fullName>
        <actions>
            <name>Update_CS1_EMP_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CS2_Percent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CSM1_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Cs1p</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_FTS_EID_to_CS2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_FTS_Name_to_CS_2_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update Xactly-Delta integration field: client services owner and FTS Manager</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update ISS</fullName>
        <actions>
            <name>Update_ISS_EID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_ISS_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_ISSp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ISS_Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to update Xactly-Delta integration field: ISS credit</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update NAM Owner Request</fullName>
        <actions>
            <name>Update_NAM_Owner_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.isEmailSend__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update NAM1</fullName>
        <actions>
            <name>Update_NAM1_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_NAM1_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_NAM1p</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Owner_Role__c</field>
            <operation>contains</operation>
            <value>NAM,Inside Sales Representative,Partner User</value>
        </criteriaItems>
        <description>Used to update Xactly-Delta integration field: Primary sales person</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Total Price SPIFF 1 as Total Price ELL</fullName>
        <actions>
            <name>Update_Total_Price_SPIFF_1_as_Total_Pric</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Customer: New Program,Current Customer: New Program,Renewal</value>
        </criteriaItems>
        <description>For DELTA integration</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>unCheck Solutions Complete</fullName>
        <actions>
            <name>unCheck_Solutions_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Solutions_Benefit_Information__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used by WF rule to check if the fields is complete. Since the Solutions/Benefit Information field is long text it can&apos;t be used in formulas or pulled in data loader IO to automate the pull of quota data</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
