<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_AEC_Con_Products_Check</fullName>
        <field>AEC_Con_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AEC Con Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Con_Products_Un_Check</fullName>
        <field>AEC_Con_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AEC Con Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Course_Products_Check</fullName>
        <field>AEC_Course_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AEC Course Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Course_Products_Un_Check</fullName>
        <field>AEC_Course_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AEC Course Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Enrolled_Products_Check</fullName>
        <field>AEC_Enrolled_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AEC Enrolled Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Enrolled_Products_Un_Check</fullName>
        <field>AEC_Enrolled_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AEC Enrolled Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Hosted_Products_Check</fullName>
        <field>AEC_Hosted_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AEC Hosted Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Hosted_Products_Un_Check</fullName>
        <field>AEC_Hosted_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AEC Hosted Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Non_Hosted_Products_Check</fullName>
        <field>AEC_Non_Hosted_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AEC Non Hosted Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Non_Hosted_Products_Un_Check</fullName>
        <field>AEC_Non_Hosted_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AEC Non Hosted Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Products_Check</fullName>
        <field>AEC_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AEC Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AEC_Products_UnCheck</fullName>
        <field>AEC_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AEC Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Course_Products_Check</fullName>
        <field>AV_Course_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Course Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Course_Products_Un_Check</fullName>
        <field>AV_Course_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Course Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Enrolled_Products_Check</fullName>
        <field>AV_Enrolled_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Enrolled Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Enrolled_Products_UnCheck</fullName>
        <field>AV_Enrolled_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Enrolled Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Enterprise_Products_Check</fullName>
        <field>AV_Enterprise_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Enterprise Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Enterprise_UnProducts_Check</fullName>
        <field>AV_Enterprise_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Enterprise UnProducts Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Hosted_Products_Check</fullName>
        <field>AV_Hosting_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Hosted Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Hosted_Products_unCheck</fullName>
        <field>AV_Hosting_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Hosted Products unCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Non_Hosted_Products_Check</fullName>
        <field>AV_Non_Hosted_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Non Hosted Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Non_Hosted_Products_UnCheck</fullName>
        <field>AV_Non_Hosted_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Non Hosted Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_PEAK_Products_Check</fullName>
        <field>AV_PEAK_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV PEAK Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_PEAK_Products_Un_Check</fullName>
        <field>AV_PEAK_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV PEAK Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Perpetual_Products_Check</fullName>
        <field>AV_Perpetual_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Perpetual Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Perpetual_Products_UnCheck</fullName>
        <field>AV_Perpetual_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Perpetual Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Products_Check</fullName>
        <field>AV_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Products_UnCheck</fullName>
        <field>AV_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Reusable_Check</fullName>
        <field>AV_Reusable_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set AV Reusable Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AV_Reusable_UnCheck</fullName>
        <field>AV_Reusable_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set AV Reusable UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Computer_Products_Check</fullName>
        <field>Computers__c</field>
        <literalValue>1</literalValue>
        <name>Set Computer Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Computer_Products_UnCheck</fullName>
        <field>Computers__c</field>
        <literalValue>0</literalValue>
        <name>Set Computer Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_EmbarK12_PD_Products_Check</fullName>
        <field>Embark_PD_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set EmbarK12 PD Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_EmbarK12_PD_Products_Un_Check</fullName>
        <field>Embark_PD_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set EmbarK12 PD Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_EmbarK12_Products_Check</fullName>
        <field>EmbarK12_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set EmbarK12 Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_EmbarK12_Products_UnCheck</fullName>
        <field>EmbarK12_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set EmbarK12 Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Heal_Products_Check</fullName>
        <field>Heal_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set Heal Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Heal_Products_Un_Check</fullName>
        <field>Heal_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set Heal Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_K12_Course_Products_Check</fullName>
        <field>K12_Course_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set K12 Course Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_K12_Course_Products_UnCheck</fullName>
        <field>K12_Course_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set K12 Course Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_K12_FullTime_Products_Check</fullName>
        <field>K12_Full_Time_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set K12 FullTime Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_K12_FullTime_Products_UnCheck</fullName>
        <field>K12_Full_Time_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set K12 FullTime Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_K12_Products_Check</fullName>
        <field>K12_Products__c</field>
        <literalValue>1</literalValue>
        <name>Set K12 Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_K12_Products_UnCheck</fullName>
        <field>K12_Products__c</field>
        <literalValue>0</literalValue>
        <name>Set K12 Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Course_Products_Check</fullName>
        <field>MIL_Course_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set MIL Course Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Course_Products_un_Check</fullName>
        <field>MIL_Course_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set MIL Course Products un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Enterprise_Products_Check</fullName>
        <field>MIL_Enterprise_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set MIL Enterprise Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Enterprise_Products_Un_Check</fullName>
        <field>MIL_Enterprise_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set MIL Enterprise Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Lang_Club_Products_Check</fullName>
        <field>MIL_Lang_Club_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set MIL Lang Club Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Lang_Club_Products_Un_Check</fullName>
        <field>MIL_Lang_Club_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set MIL Lang Club Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Products_Check</fullName>
        <field>MIL_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set MIL Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Products_UnCheck</fullName>
        <field>MIL_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set MIL Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Session_Products_Check</fullName>
        <field>MIL_Session_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set MIL Session Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Session_Products_UnCheck</fullName>
        <field>MIL_Session_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set MIL Session Products UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Site_Products_Check</fullName>
        <field>MIL_Site_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set MIL Site Products Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MIL_Site_Products_Un_Check</fullName>
        <field>MIL_Site_Product__c</field>
        <literalValue>0</literalValue>
        <name>Set MIL Site Products Un Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Non_Standard_Contract_Congfig_Use_Ch</fullName>
        <field>Non_Standard_Contract_Configuration_Used__c</field>
        <literalValue>1</literalValue>
        <name>Set Non Standard Contract Congfig Use Ch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Non_Standard_Contract_Congfig_Use_Un</fullName>
        <field>Non_Standard_Contract_Configuration_Used__c</field>
        <literalValue>0</literalValue>
        <name>Set Non Standard Contract Congfig Use Un</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set AEC Con Products Check</fullName>
        <actions>
            <name>Set_AEC_Con_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Concurrent Seat</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Con Products Un Check</fullName>
        <actions>
            <name>Set_AEC_Con_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Concurrent Seat</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Course Products Check</fullName>
        <actions>
            <name>Set_AEC_Course_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Course Products Un Check</fullName>
        <actions>
            <name>Set_AEC_Course_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Enrolled Products Check</fullName>
        <actions>
            <name>Set_AEC_Enrolled_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Reusable Seat: Student</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Enrolled Products Un Check</fullName>
        <actions>
            <name>Set_AEC_Enrolled_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Reusable Seat: Student</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Hosted Products Check</fullName>
        <actions>
            <name>Set_AEC_Hosted_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Hosting</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Hosted Products Un Check</fullName>
        <actions>
            <name>Set_AEC_Hosted_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Hosting</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Non Hosted Products Check</fullName>
        <actions>
            <name>Set_AEC_Non_Hosted_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Content Only</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Non Hosted Products Un Check</fullName>
        <actions>
            <name>Set_AEC_Non_Hosted_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Content Only</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Products Check</fullName>
        <actions>
            <name>Set_AEC_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>AEC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AEC Products UNCheck</fullName>
        <actions>
            <name>Set_AEC_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>notEqual</operation>
            <value>AEC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Course Products Check</fullName>
        <actions>
            <name>Set_AV_Course_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Course Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Course_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Enrolled Products Check</fullName>
        <actions>
            <name>Set_AV_Enrolled_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Reusable Seat: Student</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Enrolled Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Enrolled_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Reusable Seat: Student</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Enterprise Products Check</fullName>
        <actions>
            <name>Set_AV_Enterprise_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Enterprise Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Enterprise_UnProducts_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Hosted Products Check</fullName>
        <actions>
            <name>Set_AV_Hosted_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Hosting</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Hosted Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Hosted_Products_unCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Hosting</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Non Hosted Products Check</fullName>
        <actions>
            <name>Set_AV_Non_Hosted_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Content Only</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Non Hosted Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Non_Hosted_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Content Only</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV PEAK Products Check</fullName>
        <actions>
            <name>Set_AV_PEAK_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Enhanced</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV PEAK Products Un Check</fullName>
        <actions>
            <name>Set_AV_PEAK_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Enhanced</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Perpetual Products Check</fullName>
        <actions>
            <name>Set_AV_Perpetual_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Perpetual</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Perpetual Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Perpetual_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Perpetual</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Products Check</fullName>
        <actions>
            <name>Set_AV_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Products UnCheck</fullName>
        <actions>
            <name>Set_AV_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>notEqual</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Reusable Check</fullName>
        <actions>
            <name>Set_AV_Reusable_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Reusable Seat: Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set AV Reusable UnCheck</fullName>
        <actions>
            <name>Set_AV_Reusable_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Aventa Learning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Reusable Seat: Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Computer Products Check</fullName>
        <actions>
            <name>Set_Computer_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Computer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Computer Products UnCheck</fullName>
        <actions>
            <name>Set_Computer_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Computer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set EmbarK12 %26 K12 Products UnCheck</fullName>
        <actions>
            <name>Set_EmbarK12_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_K12_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>notEqual</operation>
            <value>K12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set EmbarK12 PD Products Check</fullName>
        <actions>
            <name>Set_EmbarK12_PD_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>EmbarK12 Online Training,EmbarK12 Onsite Training</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set EmbarK12 PD Products Un Check</fullName>
        <actions>
            <name>Set_EmbarK12_PD_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>EmbarK12 Online Training,EmbarK12 Onsite Training</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set EmbarK12 Products Check</fullName>
        <actions>
            <name>Set_EmbarK12_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>EmbarK12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Heal Products Check</fullName>
        <actions>
            <name>Set_Heal_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Homebound</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Heal Products Un Check</fullName>
        <actions>
            <name>Set_Heal_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Homebound</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set K12 Course Products Check</fullName>
        <actions>
            <name>Set_K12_Course_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set K12 Course Products UnCheck</fullName>
        <actions>
            <name>Set_K12_Course_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set K12 FullTime Products Check</fullName>
        <actions>
            <name>Set_K12_FullTime_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Full-Time Student</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set K12 FullTime Products UnCheck</fullName>
        <actions>
            <name>Set_K12_FullTime_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Full-Time Student</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set K12 Products Check</fullName>
        <actions>
            <name>Set_K12_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>K12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>EmbarK12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Course Products Check</fullName>
        <actions>
            <name>Set_MIL_Course_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Course Products Un Check</fullName>
        <actions>
            <name>Set_MIL_Course_Products_un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Course Enrollment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Enterprise Products Check</fullName>
        <actions>
            <name>Set_MIL_Enterprise_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Enterprise Products Un Check</fullName>
        <actions>
            <name>Set_MIL_Enterprise_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Lang Club Check</fullName>
        <actions>
            <name>Set_MIL_Lang_Club_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Language Club</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Lang Club Un Check</fullName>
        <actions>
            <name>Set_MIL_Lang_Club_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Language Club</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Products Check</fullName>
        <actions>
            <name>Set_MIL_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notContain</operation>
            <value>Language Club</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Products UnCheck</fullName>
        <actions>
            <name>Set_MIL_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>notEqual</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Session Products Check</fullName>
        <actions>
            <name>Set_MIL_Session_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Session</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Session Products Un Check</fullName>
        <actions>
            <name>Set_MIL_Session_Products_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Session</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Site Products Check</fullName>
        <actions>
            <name>Set_MIL_Site_Products_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>equals</operation>
            <value>Site</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MIL Site Products Un Check</fullName>
        <actions>
            <name>Set_MIL_Site_Products_Un_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Provider__c</field>
            <operation>equals</operation>
            <value>Middlebury Interactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Product_Consumption_Model__c</field>
            <operation>notEqual</operation>
            <value>Site</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
