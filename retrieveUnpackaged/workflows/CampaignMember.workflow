<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_DRR_or_Website_Lead</fullName>
        <description>Notify DRR or Website Lead</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Campaign_Member_Contacted_Date</fullName>
        <field>Contacted_Date__c</field>
        <formula>TODAY()</formula>
        <name>Campaign Member Contacted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Attempted</fullName>
        <field>Contact_Attempted__c</field>
        <literalValue>1</literalValue>
        <name>Contact Attempted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Attempted_Date</fullName>
        <field>Contact_Attempt_Date__c</field>
        <formula>now()</formula>
        <name>Contact Attempted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Notify DRR for Website Leads</fullName>
        <actions>
            <name>Notify_DRR_or_Website_Lead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>startsWith</operation>
            <value>Website: Contact,Website: Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
