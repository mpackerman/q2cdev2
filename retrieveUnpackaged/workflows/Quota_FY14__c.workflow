<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Select_Use_FY14_Calc_Checkbox</fullName>
        <field>FY14__c</field>
        <literalValue>1</literalValue>
        <name>Select Use FY14 Calc Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPdate_CCQ_YTD</fullName>
        <field>CCQA__c</field>
        <formula>IF(CCA__c=0,0, 
IF(CCA__c&lt;0.8,0,
IF(CCA__c&lt;1,CCA__c,
1)))</formula>
        <name>UPdate % CCQ YTD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_FY14_Checkbox</fullName>
        <field>FY14__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck FY14 Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_ISR_Box</fullName>
        <field>ISR__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck ISR Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_MGR_Box</fullName>
        <field>MGR__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck MGR Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_NAM_Box</fullName>
        <field>NAM__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck NAM Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Annual_Quota</fullName>
        <field>AnQ__c</field>
        <formula>CC_Q__c+NC_Q__c+AQM__c</formula>
        <name>Update Annual Quota</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Blended_Rate_Commission_Earned</fullName>
        <field>BRCE__c</field>
        <formula>BRE1__c+BRE2__c+BRE3__c</formula>
        <name>Update Blended Rate Commission Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CCQ1</fullName>
        <description>This sets CC Q1 % to either 0, 0.8 to 1, or 1 (&lt;80%, 80%-100%, above 100%)</description>
        <field>CCQ1P__c</field>
        <formula>IF(ISR__c=False,0, 
IF(CCTQ1__c/CCQ1__c&lt;0.8,0, 
IF(CCTQ1__c/CCQ1__c&lt;1,CCTQ1__c/CCQ1__c,1)))</formula>
        <name>Update CCQ1%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CCQ2</fullName>
        <description>This sets CC Q2 % to either 0, 0.8 to 1, or 1 (&lt;80%, 80%-100%, above 100%)</description>
        <field>CCQ2P__c</field>
        <formula>IF(ISR__c=False,0, 
IF(CCTQ2__c/CCQ2__c&lt;0.8,0, 
IF(CCTQ2__c/CCQ2__c&lt;1,CCTQ2__c/CCQ2__c,1)))</formula>
        <name>Update CCQ2%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CCQ3</fullName>
        <description>This sets CC Q3 % to either 0, 0.8 to 1, or 1 (&lt;80%, 80%-100%, above 100%)</description>
        <field>CCQ3P__c</field>
        <formula>IF(ISR__c=False,0, 
IF(CCTQ3__c/CCQ3__c&lt;0.8,0, 
IF(CCTQ3__c/CCQ3__c&lt;1,CCTQ3__c/CCQ3__c,1)))</formula>
        <name>Update CCQ3%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CCQ4</fullName>
        <description>This sets CC Q4 % to either 0, 0.8 to 1, or 1 (&lt;80%, 80%-100%, above 100%)</description>
        <field>CCQ4P__c</field>
        <formula>IF(ISR__c=False,0, 
IF(CCTQ4__c/CCQ4__c&lt;0.8,0, 
IF(CCTQ4__c/CCQ4__c&lt;1,CCTQ4__c/CCQ4__c,1)))</formula>
        <name>Update CCQ4%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CCQ_Achievement</fullName>
        <field>CQA__c</field>
        <formula>IF (MGR__c = True, 1,
IF (NAM__c = True, 2,
IF (ISR__c = True, 3,0)))</formula>
        <name>Update CCQ Achievement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CC_Commission_Earned</fullName>
        <field>CCCE__c</field>
        <formula>IF(NAM__c=True,CCE1__c + CCE2__c + CCE3__c,CCEQ1__c+CCEQ2__c+CCEQ3__c+CCEQ4__c+CCAI__c)</formula>
        <name>Update CC Commission Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CC_Quota</fullName>
        <field>CC_Q__c</field>
        <formula>CCQ1__c+CCQ2__c+CCQ3__c+CCQ4__c</formula>
        <name>Update CC Quota</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Current_Sales_Coll_Earned</fullName>
        <field>ADEC__c</field>
        <formula>IF(MGR__c=True,0, IF(CCA__c&lt;0.8, NQTC__c*CCR1__c,NQTC__c * CCR2__c))</formula>
        <name>Update Current Sales Collab Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Current_Sales_Collab_Earned</fullName>
        <field>ADEN__c</field>
        <formula>IF(CCA__c&lt;80, NQTC__c*CCR1__c,
IF(CCA__c&lt;100, NQTC__c * CCR2__c,NQTC__c*CCR3__c))</formula>
        <name>Update Current Sales Collab Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DRR_Earnings</fullName>
        <description>Updates the DRR earnings bc a formula in this field makes the compile size of YTD commission earned too big.</description>
        <field>DRRCE__c</field>
        <formula>DRC__c + DRC2__c +DRC3__c</formula>
        <name>Update DRR Earnings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ISR_Checkbox</fullName>
        <field>ISR__c</field>
        <literalValue>1</literalValue>
        <name>Update ISR Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MGR_Checkbox</fullName>
        <field>MGR__c</field>
        <literalValue>1</literalValue>
        <name>Update MGR Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NAM_Checkbox</fullName>
        <field>NAM__c</field>
        <literalValue>1</literalValue>
        <name>Update NAM Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Cust_Commish_Earned</fullName>
        <field>NCCE__c</field>
        <formula>IF(RNC__c =2,NRE1__c+NRE2__c+NRE3__c+NRE4__c,
IF(RNC__c =3, NRE1ISR__c + NRE2ISR__c,
0))</formula>
        <name>Update New Cust Commish Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Sales_Collab_Earned</fullName>
        <field>ADEN__c</field>
        <formula>IF(MGR__c=True,0,IF(NCA__c&lt;1, NQTN__c* NCR1__c, NQTN__c*NCR2__c))</formula>
        <name>Update New Sales Collab Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Q1</fullName>
        <description>This sets Q1 % (hidden) to either 0, 0.8 to 1, or 1 (&lt;80%, 80%-100%, above 100%)</description>
        <field>Q1P__c</field>
        <formula>IF(QQ1__c=0,0,
IF(TQ1__c/QQ1__c&lt;0.8,0,
IF(TQ1__c/QQ1__c&lt;1,TQ1__c/QQ1__c,1)))</formula>
        <name>Update Q1%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Q2</fullName>
        <field>Q2P__c</field>
        <formula>IF(QQ2__c=0,0, 
IF(TQ2__c/QQ2__c&lt;0.8,0, 
IF(TQ2__c/QQ2__c&lt;1,TQ2__c/QQ2__c,1)))</formula>
        <name>Update Q2%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Q3</fullName>
        <field>Q3P__c</field>
        <formula>IF(QQ3__c=0,0, 
IF(TQ3__c/QQ3__c&lt;0.8,0, 
IF(TQ3__c/QQ3__c&lt;1,TQ3__c/QQ3__c,1)))</formula>
        <name>Update Q3%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Q4</fullName>
        <field>Q4P__c</field>
        <formula>IF(QQ4__c=0,0, 
IF(TQ4__c/QQ4__c&lt;0.8,0, 
IF(TQ4__c/QQ4__c&lt;1,TQ4__c/QQ4__c,1)))</formula>
        <name>Update Q4%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Role_Number</fullName>
        <description>This updates the role number field which is used for calculations</description>
        <field>Role_Number__c</field>
        <formula>If(NAM__c=True,0,1)</formula>
        <name>Update Role Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SDR_Earnings</fullName>
        <field>SDRCE__c</field>
        <formula>SDC__c+SDC2__c</formula>
        <name>Update SDR Earnings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_S_Collab_Earned</fullName>
        <field>ACET__c</field>
        <formula>IF(MGR__c=FALSE,ADEC__c+ADEN__c, NQT__c*MGR_AR__c)</formula>
        <name>Update Sales Collab Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_YTD_Commish_Earned</fullName>
        <field>YTD_CE__c</field>
        <formula>CCCE__c + NCCE__c + 
MC__c +
SDRCE__c + DRRCE__c +
AY__c + ACET__c</formula>
        <name>Update YTD Commish Earned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Check FY14 Checkbox</fullName>
        <actions>
            <name>Select_Use_FY14_Calc_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>equals</operation>
            <value>2014</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck FY14 Checkbox</fullName>
        <actions>
            <name>Uncheck_FY14_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Blended Rate Commission Earned</fullName>
        <actions>
            <name>Update_Blended_Rate_Commission_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.BRQ__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.BRQ__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CCQ Achievement</fullName>
        <actions>
            <name>Update_CCQ_Achievement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.CC_Q__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>This rule updates the CCQ Achievement field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for DRR-SDR</fullName>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Direct Response Rep,Sales Development Rep</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>For DRR or SDR, uncheck MGR, NAM, ISR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for ISR</fullName>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_ISR_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Inside Sales Rep</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>For Inside Sales Rep, check ISR; uncheck NAM, MGR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for Manager</fullName>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_NAM_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_MGR_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>Regional Sales Manager,Sales Vice President,Inside Sales Director,Inside Sales Manager,Professional Services Director,Middlebury Director,Channel Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>For RSM, SVP, PSD, MIL Director, ISM, ISD, Channel sales, check MGR; uncheck NAM, ISR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Checkboxes for NAM</fullName>
        <actions>
            <name>Uncheck_ISR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_MGR_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_NAM_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Quota_Role__c</field>
            <operation>equals</operation>
            <value>National Account Manager,Middlebury Sales Rep</value>
        </criteriaItems>
        <description>For National Account Manager or MIL Rep, check NAM; uncheck MRG, ISR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Current Customer Quota</fullName>
        <actions>
            <name>Update_CC_Quota</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.ISR__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Current Customer Quota Commission Earned</fullName>
        <actions>
            <name>Update_CC_Commission_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.RNC__c</field>
            <operation>greaterOrEqual</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Current Sales Collab Earned</fullName>
        <actions>
            <name>Update_Current_Sales_Coll_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.NQTC__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.NQTC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update DRR Earnings</fullName>
        <actions>
            <name>Update_DRR_Earnings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.DRRT__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.DRRT__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule calculates DRR Commission Earnings field using field update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Earning Fields</fullName>
        <actions>
            <name>Update_Annual_Quota</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_YTD_Commish_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>This is for non-FY14 quotas</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update New Cust Commish Earned</fullName>
        <actions>
            <name>Update_New_Cust_Commish_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update New Sales Collab Earned</fullName>
        <actions>
            <name>Update_New_Sales_Collab_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.NQTN__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.NQTN__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quarterly Earnings Percent</fullName>
        <actions>
            <name>Update_Q1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Q2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Q3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Q4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.QT__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.QT__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule sets the &quot;% of Quarterly PEQ for Qx&quot; field percentage rate using field update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quarterly Earnings Percent for ISR</fullName>
        <actions>
            <name>UPdate_CCQ_YTD</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CCQ1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CCQ2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CCQ3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CCQ4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.ISR__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule sets the &quot;% of CCQ for Qx&quot; field percentage rate using field update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Role Number</fullName>
        <actions>
            <name>Update_Role_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota_FY14__c.Fiscal_Year__c</field>
            <operation>notEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>This will update the field &quot;Role Number&quot; which is used for calculations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SDR Earnings</fullName>
        <actions>
            <name>Update_SDR_Earnings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.SDRT__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.SDRT__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule calculates SDR Commission Earnings field using field update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Collab Earned</fullName>
        <actions>
            <name>Update_S_Collab_Earned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Quota_FY14__c.NQT__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quota_FY14__c.NQT__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
