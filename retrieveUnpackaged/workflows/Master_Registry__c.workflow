<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_numeric_Global_Id_Value</fullName>
        <field>Global_ID__c</field>
        <formula>Value(Name)</formula>
        <name>Set numeric Global Id Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_numeric_Parent_Global_ID</fullName>
        <field>Parent_Global_ID_Numeric__c</field>
        <formula>value(Parent_Global_ID__r.Name)</formula>
        <name>Set numeric Parent Global ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set numeric Global Id to Value in Name</fullName>
        <actions>
            <name>Set_numeric_Global_Id_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_numeric_Parent_Global_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Text(Global_ID__c) &lt;&gt; Name || Text(Parent_Global_ID_Numeric__c) &lt;&gt;  Parent_Global_ID__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
