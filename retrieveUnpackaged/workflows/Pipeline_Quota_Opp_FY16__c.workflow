<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Expired_on_Pipe_Opp_FY16</fullName>
        <field>Expired__c</field>
        <literalValue>1</literalValue>
        <name>Check Expired on Pipe Opp FY16</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Pipeline Quota Opp FY16 to Expired</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Pipeline_Quota_Opp_FY16__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Expired_on_Pipe_Opp_FY16</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Pipeline_Quota_Opp_FY16__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
