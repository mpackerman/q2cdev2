<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Commission_owner_fields_update_alert</fullName>
        <description>Commission owner fields update alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>dchen@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Commission_Owner_Fields_Chnaged</template>
    </alerts>
    <alerts>
        <fullName>New_Account_to_Review_Notification</fullName>
        <ccEmails>accountsupport@k12.com</ccEmails>
        <description>New Account to Review Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_Account_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_Account_to_Review_Notification_CSD_Clayton</fullName>
        <description>New Account to Review Notification CSD Clayton</description>
        <protected>false</protected>
        <recipients>
            <recipient>kclayton@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_Account_NotificationAll</template>
    </alerts>
    <alerts>
        <fullName>New_Account_to_Review_Notification_CSD_ECN</fullName>
        <description>New Account to Review Notification CSD Fuhrmeister</description>
        <protected>false</protected>
        <recipients>
            <recipient>lfuhrmeister@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_Account_NotificationAll</template>
    </alerts>
    <alerts>
        <fullName>New_Account_to_Review_Notification_CSD_Evans</fullName>
        <description>New Account to Review Notification CSD Evans</description>
        <protected>false</protected>
        <recipients>
            <recipient>jievans@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_Account_NotificationAll</template>
    </alerts>
    <alerts>
        <fullName>New_Account_to_Review_Notification_CSD_Johnson</fullName>
        <description>New Account to Review Notification CSD Johnson</description>
        <protected>false</protected>
        <recipients>
            <recipient>kjohnson@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Audit_on_Entry_and_Billing_Templates/New_Account_NotificationAll</template>
    </alerts>
    <alerts>
        <fullName>Sales_Collab_owner_change_notification</fullName>
        <description>Sales Collab owner change notification (All)</description>
        <protected>false</protected>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>glevin@getfueled.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tshively@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Sales_Collab_Field_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Sales_Collab_owner_change_notification_East</fullName>
        <description>Sales Collab owner change notification (East)</description>
        <protected>false</protected>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Sales_Collab_Field_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Sales_Collab_owner_change_notification_West</fullName>
        <description>Sales Collab owner change notification (West)</description>
        <protected>false</protected>
        <recipients>
            <recipient>1bblaydes@k12.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FuelEd_Emails/Sales_Collab_Field_Change_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>AccountRecordTypeKCR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KCR</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account Record Type-KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AccountShippingCity</fullName>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>Account Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AccountShippingZip</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>Account Shipping Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_In_Good_Standing_Update</fullName>
        <field>Account_Status__c</field>
        <literalValue>In Good Standing</literalValue>
        <name>Account In Good Standing Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCRSchoolOfficialUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_School_Account_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>KCR School Official Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCRSchoolRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_Credit_Recovery_School_Account_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>KCR School Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordTypeKCR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KCR</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type-KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_UpdateCombine_Co_Termed_Contr</fullName>
        <field>SBQQ__CoTermedContractsCombined__c</field>
        <literalValue>1</literalValue>
        <name>SB Account UpdateCombine Co-Termed Contr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_UpdateContract_Co_Termination</fullName>
        <field>SBQQ__ContractCoTermination__c</field>
        <literalValue>Prompt</literalValue>
        <name>SB Account UpdateContract Co-Termination</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_UpdateRenewal_Model</fullName>
        <field>SBQQ__RenewalModel__c</field>
        <literalValue>Contract Based</literalValue>
        <name>SB Account UpdateRenewal Model</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_Update_Co_Termination_Event</fullName>
        <field>SBQQ__CoTerminationEvent__c</field>
        <literalValue>Add-on</literalValue>
        <name>SB Account Update Co-Termination Event</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_Update_Ignore_Parent_Contract</fullName>
        <field>SBQQ__IgnoreParentContractedPrices__c</field>
        <literalValue>1</literalValue>
        <name>SB Account Update Ignore Parent Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_Update_Preserve_Bundle_Struct</fullName>
        <field>SBQQ__PreserveBundle__c</field>
        <literalValue>1</literalValue>
        <name>SB Account Update Preserve Bundle Struct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_Update_Renewing_Pricing_Metho</fullName>
        <field>SBQQ__RenewalPricingMethod__c</field>
        <literalValue>Same</literalValue>
        <name>SB Account Update Renewing Pricing Metho</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SB_Account_Update_SBQQ_AssetQuantitiesC</fullName>
        <description>WF rule to set to True for all FuelEd Accounts - MIGHT apply if we need to use assets for pertetual ALS licenses</description>
        <field>SBQQ__AssetQuantitiesCombined__c</field>
        <literalValue>1</literalValue>
        <name>SB Account Update SBQQ__AssetQuantitiesC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetAventaBrand</fullName>
        <field>Brand__c</field>
        <literalValue>Aventa</literalValue>
        <name>Set Aventa Brand</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetKCRAccountIDtonull</fullName>
        <field>KCR_ID__c</field>
        <name>Set KCR Account ID tonull</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_East_Central</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Northeast</literalValue>
        <name>Set CS East Central</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_East_Coastal</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Southeast</literalValue>
        <name>Set CS Southeast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_FL</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Florida</literalValue>
        <name>Set CS FL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_NYC</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>NYC</literalValue>
        <name>Set CS NYC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Northwest</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Northwest</literalValue>
        <name>Set CS Northwest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Northwest_for_CA</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Northwest</literalValue>
        <name>Set CS Northwest for CA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Brandi_Hammel</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>bhammel@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Brandi Hammel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Ceene_Randle</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>crandle@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Ceene Randle</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Chris_Bialka</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>cbialka@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Chris Bialka</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Denise_Rost</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>drost@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Denise Rost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_East_Central_CSD</fullName>
        <description>set Client Service owner to the CSD</description>
        <field>Client_Service_Owner__c</field>
        <lookupValue>kjohnson@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner East Central CSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_East_Coastal_CSD</fullName>
        <description>sets the Client Service Owner to East Coastal CSD</description>
        <field>Client_Service_Owner__c</field>
        <lookupValue>lfuhrmeister@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner East Coastal CSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_FL_CSD</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>lfuhrmeister@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner FL CSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Ginette_Palazzolo</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>gpalazzolo@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Ginette Palazzolo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Julie_Osvath</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>josvath@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Julie Osvath</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Lauren_Bone</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>lbone@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Lauren Bone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Linnette_Mancuso</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>lmancuso@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Linnette Mancuso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Mariam_Barth_Paul</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>mbarthpaul@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Mariam Barth-Paul</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Michelle_Youngberg</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>myoungberg@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Michelle Youngberg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_NYC_CSD</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>lfuhrmeister@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner NYC CSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Northwest_CSD</fullName>
        <description>sets Client Service Owner to Northwest CSD</description>
        <field>Client_Service_Owner__c</field>
        <lookupValue>jievans@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Northwest CSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Northwest_CSD_Northern_CA</fullName>
        <description>sets Client Service Owner to Northwest (Northern CA) CSD</description>
        <field>Client_Service_Owner__c</field>
        <lookupValue>jievans@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Northwest CSD (Northern CA)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Phoebe_Morris</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>pmorris@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Phoebe Morris</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Renae_Abboud</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>rabboud@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Renae Abboud</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Southwest_CSD</fullName>
        <description>set Client Service Owner to Southwest CSD</description>
        <field>Client_Service_Owner__c</field>
        <lookupValue>kclayton@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Southwest CSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Southwest_CSD_Southern_CA</fullName>
        <description>sets Client Service Owner to Southwest (Southern Cal) CSD</description>
        <field>Client_Service_Owner__c</field>
        <lookupValue>kclayton@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Southwest CSD (Southern CA)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Owner_Stacy_Croft</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>scroft@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set CS Owner Stacy Croft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Southwest</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Southwest</literalValue>
        <name>Set CS Southwest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CS_Southwest_for_CA</fullName>
        <field>Client_Service_Territory__c</field>
        <literalValue>Southwest</literalValue>
        <name>Set CS Southwest for CA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sets_CS_Owner_Jessica_Gardner</fullName>
        <field>Client_Service_Owner__c</field>
        <lookupValue>jegardner@k12.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Sets CS Owner Jessica Gardner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKSSchoolOfficialRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Keystone_School_School_Account_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TKS School Official Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAccountSiteIDKCR</fullName>
        <field>Site</field>
        <formula>KCR_ID__c</formula>
        <name>Update Account Site ID KCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypetoInstitutional</fullName>
        <description>Update Record Type to Institutional</description>
        <field>RecordTypeId</field>
        <lookupValue>FuelEd</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Institutional</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateShippingState</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>Update Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateShippingStreet</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Update Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Utilization</fullName>
        <field>Product_Utilization_Last_Mod_Date__c</field>
        <formula>now()</formula>
        <name>Update Product Utilization</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account In Good Standing</fullName>
        <actions>
            <name>Account_In_Good_Standing_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Account Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Number_of_Opportunities__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>When Account Status is Account lead and a new Opportunity is created, the Status should move to &apos;in Good Standing&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Record Type-Institutional</fullName>
        <actions>
            <name>UpdateRecordTypetoInstitutional</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>K12,A+,Aventa,Middlebury,PowerSpeak</value>
        </criteriaItems>
        <description>Set Records Type to FuelEd</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Record Type-KCR</fullName>
        <actions>
            <name>RecordTypeKCR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <description>Set Record type to KCR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Shipping City</fullName>
        <actions>
            <name>AccountShippingCity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingCity</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Account Shipping Street UPdate with City</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Shipping State</fullName>
        <actions>
            <name>UpdateShippingState</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Account Shipping Street UPdate with State</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Shipping Street</fullName>
        <actions>
            <name>UpdateShippingStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingStreet</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Account Shipping Street UPdate with Billing Street</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Shipping Zip</fullName>
        <actions>
            <name>AccountShippingZip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingPostalCode</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Account Shipping Street UPdate with Zip</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Verified Set Record Type</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Audit_Review_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <description>Workflow to set record type to Account Verified when Audit Review Status is set to Verified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Commission Owner Fields Changed</fullName>
        <actions>
            <name>Commission_owner_fields_update_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends email to Deb if any of the owner fields are changed</description>
        <formula>AND(  RecordTypeId = &quot;012700000001DSU&quot;,  $Profile.Name &lt;&gt;&quot;System Administrator&quot;,  $Profile.Name &lt;&gt;&quot;B2B Sales Ops&quot;,    ISPICKVAL(Audit_Review_Status__c , &quot;Verified&quot;),  OR(   ISCHANGED(OwnerId ),  ISCHANGED(Client_Service_Owner__c),  ISCHANGED(ISS_Owner__c ),  ISCHANGED(CSD_Account_Owner__c ),  ISCHANGED(FTS_Manager__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>KCR School Official Record Type Update</fullName>
        <actions>
            <name>KCRSchoolOfficialUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Lead_Role__c</field>
            <operation>equals</operation>
            <value>School Official</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone Credit Recovery - School Account Layout</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Campus__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Account To Review Notification</fullName>
        <actions>
            <name>New_Account_to_Review_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <description>used to notfiy the reconciliation team that a new account was created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Account To Review Notification CSD Clayton</fullName>
        <actions>
            <name>New_Account_to_Review_Notification_CSD_Clayton</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>equals</operation>
            <value>Clayton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>7/7/2013 9:00 PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Audit_Review_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <description>used to notify the CSD Clayton that a new account was created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account To Review Notification CSD Evans</fullName>
        <actions>
            <name>New_Account_to_Review_Notification_CSD_Evans</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>equals</operation>
            <value>Evans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>7/7/2013 9:00 PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Audit_Review_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <description>used to notfiy the CSD Evans that a new account was created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account To Review Notification CSD Fuhrmeister</fullName>
        <actions>
            <name>New_Account_to_Review_Notification_CSD_ECN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>equals</operation>
            <value>Fuhrmeister,Florida</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>7/7/2013 9:00 PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Audit_Review_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <description>used to notfiy the CSD Fuhrmeister that a new account was created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account To Review Notification CSD Johnson</fullName>
        <actions>
            <name>New_Account_to_Review_Notification_CSD_Johnson</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Service_Territory__c</field>
            <operation>contains</operation>
            <value>Johnson,NYC,CHI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>7/7/2013 9:00 PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Audit_Review_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <description>used to notify the CSD Johnson that a new account was created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SB Account Update Fields</fullName>
        <actions>
            <name>SB_Account_UpdateCombine_Co_Termed_Contr</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SB_Account_UpdateRenewal_Model</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SB_Account_Update_Co_Termination_Event</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SB_Account_Update_Ignore_Parent_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SB_Account_Update_Preserve_Bundle_Struct</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SB_Account_Update_SBQQ_AssetQuantitiesC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <description>WF rule to set to True for all FuelEd Accounts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SB Account Update New Only</fullName>
        <actions>
            <name>SB_Account_UpdateContract_Co_Termination</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SB_Account_Update_Renewing_Pricing_Metho</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FuelEd</value>
        </criteriaItems>
        <description>WF rule to set to default  for NEW FuelEd Accounts.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Collab Change %28All%29</fullName>
        <actions>
            <name>Sales_Collab_owner_change_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Used to notify key users that the field changed</description>
        <formula>ISCHANGED( Sales_Collab_Program_Owner__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set FY16 Retention Count Owner</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Count_for_FY16_Retention__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Parent Id for hierarchy</fullName>
        <active>false</active>
        <formula>Parent_Org_Record_ID__c &lt;&gt; ParentId</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TKS School Official Record Type Update</fullName>
        <actions>
            <name>TKSSchoolOfficialRecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Lead_Role__c</field>
            <operation>equals</operation>
            <value>School Official</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Keystone School - School Account Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>Keystone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Campus__c</field>
            <operation>equals</operation>
            <value>TKS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Site ID KCR</fullName>
        <actions>
            <name>UpdateAccountSiteIDKCR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Brand__c</field>
            <operation>equals</operation>
            <value>KCR</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Product Utilization</fullName>
        <actions>
            <name>Update_Product_Utilization</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Current_Product_Utilization__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
