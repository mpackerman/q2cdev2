Hello everyone,
I hope you are all having a nice start to Spring!  For many of you, the new season means you are getting closer to finishing your courses.  If you are concerned that you have fallen off track, please contact me so that we can come up with a new plan and get you back on track. For those of you that are on track, keep the momentum going, you can do it! 
Just as a reminder, Blackboard will be unavailable on April 4th-April 8th due to an upgrade that we are doing.  I suggest that on Wednesday, April 3rd, you get the exams and assignments that you’ve been working on turned in so they are out of the way.  Please do not wait to the last minute to turn all of the work in.  Aim to have everything turned in by the afternoon or early evening on April 3rd.  Although you will not be able to get into your online classroom, that doesn’t mean you can be working on your school work during this time.  There are many activities that you can do outside of Blackboard. Here are just a few: 

1.	Print off the directions for any writing assignments that you have coming up.  You can work on research papers, essays, journals and other projects outside of the online classroom.  Imagine how nice it will fee to submit them when Blackboard comes back up!
2.	Are you taking a science course that has a lab component?  Print off the instructions for the lab and get that done during this time. 
3.	Remember those books you have to read for your English course (or any other course for that matter)? Spend this time catching up on reading or even reading ahead.  You’ll thank yourself that you did!
4.	You can print off exams and quizzes and work on them offline.  If you choose to do this, all you will need to do when you can get back into your course is plug in the answers.  
5.	Is there a topic that you covered recently that you could use a little more practice with?  Pull out the textbook or workbook for that course (if there is one) and spend some time practicing. 
6.	If you feel you have fallen behind, take a look at your Dynamic Course Schedule and start working on a plan to get back on track. 

As always, if you have any questions or concerns, don’t hesitate to reach out. 

Sincerely,
Mrs. Jones