<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Read</externalSharingModel>
    <fields>
        <fullName>Agency_Type__c</fullName>
        <externalId>false</externalId>
        <label>Agency Type</label>
        <length>60</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Charter_School__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Charter School</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>County_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>County Name from NCES</inlineHelpText>
        <label>County Name</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Full_Time_Equivalent_FTE_Teachers__c</fullName>
        <externalId>false</externalId>
        <label>Full-Time Equivalent (FTE) Teachers</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Global_ID__c</fullName>
        <externalId>true</externalId>
        <label>Global_ID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Grades_Offered__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Grade offered data from NCES.  PK = Pre-Kindergarten, K = Kindergarten, UG = Un-graded</inlineHelpText>
        <label>Grades Offered</label>
        <picklist>
            <picklistValues>
                <fullName>PK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>K</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>7</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>8</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>9</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>11</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>12</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>UG</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Individualized_Education_Program_Student__c</fullName>
        <externalId>false</externalId>
        <label>Individualized Education Program Student</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Institution_NCES_ID__c</fullName>
        <externalId>false</externalId>
        <label>Institution NCES ID</label>
        <length>12</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Institution_Name__c</fullName>
        <externalId>false</externalId>
        <label>Institution Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Institution_Name_in_NCES__c</fullName>
        <description>Limited to 50 to match NCES definition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Name exactly as it appears in NCES.  Note: NCES only supports 50 characters, so some names are truncated.</inlineHelpText>
        <label>Institution Name in NCES</label>
        <length>60</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Institution_Type__c</fullName>
        <externalId>false</externalId>
        <label>Institution Type</label>
        <length>55</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LEP_ELL_Students__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Limited English Proficient (LEP) / English Language Learners (ELL) Students</inlineHelpText>
        <label>LEP/ELL Students</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Legacy_Master_Registry_ID__c</fullName>
        <description>Stores the production SF id from Master Registry</description>
        <externalId>true</externalId>
        <label>Legacy Master Registry ID</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Magnet_School__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Magnet School</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Mailing_Address__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Limited to 75 characters by NCES</inlineHelpText>
        <label>Mailing Address</label>
        <length>75</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mailing_City__c</fullName>
        <externalId>false</externalId>
        <label>Mailing City</label>
        <length>32</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mailing_State__c</fullName>
        <description>Mailing State Abbreviation</description>
        <externalId>false</externalId>
        <inlineHelpText>Mailing State Abbreviation</inlineHelpText>
        <label>Mailing State</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mailing_Zip_Plus_4__c</fullName>
        <externalId>false</externalId>
        <label>Mailing Zip Plus 4</label>
        <length>4</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mailing_Zip__c</fullName>
        <externalId>false</externalId>
        <label>Mailing Zip</label>
        <length>5</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NCES_Link__c</fullName>
        <externalId>false</externalId>
        <formula>if(Institution_NCES_ID__c= &quot;&quot;,&quot;&quot;,
    if(len(Institution_NCES_ID__c)=12, &quot;Public: &quot; + HYPERLINK(&quot;https://nces.ed.gov/ccd/schoolsearch/school_detail.asp?Search=1&amp;ID=&quot; + Institution_NCES_ID__c , Institution_NCES_ID__c),
    if(len(Institution_NCES_ID__c)= 8, &quot;Private: &quot; + HYPERLINK(&quot;https://nces.ed.gov/surveys/pss/privateschoolsearch/school_detail.asp?Search=1&amp;ID=&quot; + Institution_NCES_ID__c ,  Institution_NCES_ID__c),
    if(len(Institution_NCES_ID__c)= 7 , &quot;District: &quot; + HYPERLINK(&quot;https://nces.ed.gov/ccd/districtsearch/district_detail.asp?Search=2&amp;details=1&amp;ID2=&quot; + Institution_NCES_ID__c ,  Institution_NCES_ID__c),
   &quot;?&quot; 
 ))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Click on this link to view the detail for this institution on the NCES website.</inlineHelpText>
        <label>NCES Link</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>No_Parent__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates that a Master Registry record has no parent record associated with it</description>
        <externalId>false</externalId>
        <label>No Parent</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Parent_Global_ID_Numeric__c</fullName>
        <externalId>false</externalId>
        <label>Parent Global ID Numeric</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent_Global_ID__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Parent Global ID</label>
        <referenceTo>Master_Registry__c</referenceTo>
        <relationshipLabel>Master Registry</relationshipLabel>
        <relationshipName>Master_Registry_Parent</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Parent_NCES__c</fullName>
        <externalId>false</externalId>
        <label>Parent NCES</label>
        <length>12</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Phone_Number__c</fullName>
        <externalId>false</externalId>
        <label>Phone Number</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Physical_Address__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Limited to 75 characters in NCES</inlineHelpText>
        <label>Physical Address</label>
        <length>75</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Physical_City__c</fullName>
        <externalId>false</externalId>
        <label>Physical City</label>
        <length>32</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Physical_State__c</fullName>
        <externalId>false</externalId>
        <label>Physical State</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Physical_Zip_Plus_4__c</fullName>
        <externalId>false</externalId>
        <label>Physical Zip Plus 4</label>
        <length>4</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Physical_Zip__c</fullName>
        <externalId>false</externalId>
        <label>Physical Zip</label>
        <length>5</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pupil_Teacher_Ratio__c</fullName>
        <externalId>false</externalId>
        <label>Pupil/Teacher Ratio</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shared_Time_School__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Shared Time School</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Total_Free_and_Reduced_Lunch_Students__c</fullName>
        <externalId>false</externalId>
        <label>Total Free and Reduced Lunch Students</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Number_Operational_Schools__c</fullName>
        <externalId>false</externalId>
        <label>Total Number Operational Schools</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Students__c</fullName>
        <externalId>false</externalId>
        <label>Total Students</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Urban_centric_Locale__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Value from NCES</inlineHelpText>
        <label>Urban-centric Locale</label>
        <length>25</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Virtual_School_Status__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Virtual School Status</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Master Registry</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Institution_Name__c</columns>
        <columns>Institution_NCES_ID__c</columns>
        <columns>Physical_State__c</columns>
        <columns>Phone_Number__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Global ID</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Master Registry</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Institution_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Institution_NCES_ID__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Physical_State__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Phone_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Parent_Global_ID__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Institution_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Institution_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Phone_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Physical_State__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Physical_City__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Physical_Zip__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Global_ID__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Institution_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Institution_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Physical_City__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Physical_State__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Parent_Global_ID__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Cannot_Change_Global_ID</fullName>
        <active>true</active>
        <description>If you do not have the permission selected on your user name you cannot update the Global ID on a Master Registry object</description>
        <errorConditionFormula>$User.Can_Change_MR_Global_ID__c = FALSE &amp;&amp; ISCHANGED( Name ) &amp;&amp; NOT(ISNEW())</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>You do not have permission to change the Global ID</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Global_Ids_must_be_numeric</fullName>
        <active>true</active>
        <errorConditionFormula>or(Name    = &quot;&quot;,
    Not(ISNUMBER(Name)),
    len(Name)  &lt;&gt; 7)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>Master Registry Global Id is required and must be 7 numeric characters.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_Parent_Checked_If_No_Parent</fullName>
        <active>true</active>
        <description>the No Parent checkbox needs to be checked if the Parent Org ID is blank</description>
        <errorConditionFormula>No_Parent__c = false &amp;&amp; ISBLANK( Parent_Global_ID__c )</errorConditionFormula>
        <errorDisplayField>No_Parent__c</errorDisplayField>
        <errorMessage>The &quot;No Parent&quot; checkbox needs to be checked if this Master Registry has no parent</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_Parent_ID_If_No_Parent</fullName>
        <active>true</active>
        <description>If No Parent is checked, do not allow a Parent Global ID value</description>
        <errorConditionFormula>No_Parent__c = true &amp;&amp; NOT(ISBLANK( Parent_Global_ID__c ))</errorConditionFormula>
        <errorDisplayField>Parent_Global_ID__c</errorDisplayField>
        <errorMessage>You cannot have a Parent Global ID if the No Parent is checked</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Create_Account</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Create Account</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/flow/Create_Account_from_Master_Registry?mrId={!Master_Registry__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Create_Account_pg</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Create Account</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/partners/Create_Account_from_Master_Registry_pg?mrId={!Master_Registry__c.Id}</url>
    </webLinks>
</CustomObject>
