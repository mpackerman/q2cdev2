trigger syncSalesOrder on SBQQ__Quote__c (after update) {

    for(SBQQ__Quote__c quote : Trigger.new){

        SBQQ__Quote__c oldQuote = Trigger.oldMap.get(quote.Id);
        
        if( quote.Sales_Order__c != null && oldQuote.Sales_Order__c != null && (quote.Record_Type_Name__c == 'Order' || quote.Record_Type_Name__c == 'Order - Approved') )
        {
            
            SCRB_SalesOrder__c salesOrder = [select id, Type__c, Bill_To_Name__c, Bill_to_Account__c, BillingStreet__c, BillingCity__c, BillingState__c, BillingPostalCode__c, BillingCountry__c, BillingPhone__c, BillingFax__c, PO_Number__c, Contract__c, Description__c, DocumentDate__c, Payment_Terms_SBQQ__c, Refund_Drop_Policy__c, Service_Start_Date__c, Service_End_Date__c, ShippingCity__c, ShippingContact__c, ShippingCountry__c, ShippingFax__c, ShippingLocation__c, ShippingPhone__c, ShippingPostalCode__c, ShippingState__c, ShippingStreet__c from SCRB_SalesOrder__c where id = :quote.Sales_Order__c];
            
            salesOrder.Bill_To_Name__c = quote.SBQQ__BillingName__c;
            salesOrder.Bill_to_Account__c = quote.Bill_To_Account__c;
            salesOrder.BillingStreet__c = quote.SBQQ__BillingStreet__c;
            salesOrder.BillingCity__c = quote.SBQQ__BillingCity__c;
            salesOrder.BillingState__c = quote.SBQQ__BillingState__c;
            salesOrder.BillingPostalCode__c = quote.SBQQ__BillingPostalCode__c;
            salesOrder.BillingCountry__c = quote.SBQQ__BillingCountry__c;
            salesOrder.BillingPhone__c = quote.BillingPhone__c;
            salesOrder.BillingFax__c = quote.BillingFax__c;
            salesOrder.PO_Number__c = quote.PO_Number__c;
            salesOrder.Contract__c = quote.SBQQ__MasterContract__c;
            salesOrder.Description__c = quote.Description__c;
            salesOrder.DocumentDate__c = quote.DocumentDate__c;
            salesOrder.Payment_Terms_SBQQ__c = quote.SBQQ__PaymentTerms__c;
            salesOrder.Refund_Drop_Policy__c = quote.Drop_Policy__c;
            salesOrder.Service_Start_Date__c = quote.SBQQ__StartDate__c;
            salesOrder.Service_End_Date__c = quote.SBQQ__EndDate__c;
            salesOrder.ShippingCity__c = quote.SBQQ__ShippingCity__c;
            salesOrder.ShippingContact__c = quote.Shipping_Contact__c;
            salesOrder.ShippingCountry__c = quote.SBQQ__ShippingCountry__c;
            salesOrder.ShippingFax__c = quote.Shipping_Fax__c;
            salesOrder.ShippingLocation__c = quote.Shipping_Location__c;
            salesOrder.ShippingPhone__c = quote.Shipping_Phone__c;
            salesOrder.ShippingPostalCode__c = quote.SBQQ__ShippingPostalCode__c;
            salesOrder.ShippingState__c = quote.SBQQ__ShippingState__c;
            salesOrder.ShippingStreet__c = quote.SBQQ__ShippingStreet__c;
            salesOrder.Type__c = quote.Order_Type__c;

            update salesOrder;
            
            
        }
    }

}