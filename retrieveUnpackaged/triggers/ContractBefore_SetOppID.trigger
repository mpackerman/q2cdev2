trigger ContractBefore_SetOppID on Contract (before insert, before update) {

    for (Contract c : Trigger.new)
    {
        //Get record type id for Legacy Contracts
        ID rectypeid = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Legacy Contract').getRecordTypeId();
        
        //Set Opportunity__c to SBQQ__Opportunity__c
        if (c.recordtypeid != rectypeid )
        {
            c.Opportunity3__c = c.SBQQ__Opportunity__c;
            
            Id oppId = c.SBQQ__Opportunity__c;
        
            if (Trigger.isInsert && c.SBQQ__Opportunity__c != null) {
                String refundPolicy = [select Id, Refund_Drop_Policy__c From Opportunity where id = :oppId].Refund_Drop_Policy__c;
                c.Refund_Drop_Policy__c = refundPolicy;
            }
        }
    
    }
    
}