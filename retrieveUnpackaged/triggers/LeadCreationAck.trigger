trigger LeadCreationAck on Lead (after insert) 
{
    System.Debug('ENTERED INTO LeadCreationAck TRIGGER');

    for (integer i=0; i<Trigger.new.size(); i++)
    {
        if (trigger.isInsert) 
        {
            System.Debug('BEGIN trigger.isInsert');
            System.Debug('DRUPAL_GUID: '+Trigger.new[i].GUID__c);
            System.Debug('SALEFORCE_ID: '+Trigger.new[i].ID);
            
            if( Trigger.new[i].GUID__c != null)
           		touchAckServlet.ackServletCallout(Trigger.new[i].GUID__c, Trigger.new[i].ID);
            
            System.Debug('END trigger.isInsert');
        }
    }
}