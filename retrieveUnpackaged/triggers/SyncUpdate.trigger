trigger SyncUpdate on OpportunityLineItem (before insert, before update) {

for (OpportunityLineItem OptyLineItem : Trigger.new) { 
 if ( OptyLineItem.LineType__c == 'Product/Service') {
        OptyLineItem.TotalPrice = OptyLineItem.Total_Price_from_Proposal__c ;
        OptyLineItem.Program_Classification_Hidden__c = OptyLineItem.Program_Classification__c ;
        OptyLineItem.Product_Provider_Hidden__c = OptyLineItem.Product_Provider__c ;
        optylineItem.Product_Consumption_Model_Hidden__c = OptyLineItem.Product_Consumption_Model__c;
        system.debug('@@ oppty line item'+OptyLineItem );
    }

}
}