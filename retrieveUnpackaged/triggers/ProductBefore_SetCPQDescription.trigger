trigger ProductBefore_SetCPQDescription on Product2 (before insert, before update) {

    for (Product2 p : Trigger.new)
    {
        
        if ( !String.isBlank(p.CPQ_Description__c) )
        {
            //Set CPQ Description Short from CPQ Description
            String shortDescription;
            String fullDescription = p.CPQ_Description__c;
            Integer fullDescriptionLength = fullDescription.length();
            if ( fullDescriptionLength < 255 )
            {
                shortDescription = fullDescription.substring(0,fullDescriptionLength);
            }
            else {
                shortDescription = fullDescription.substring(0,252) + '...';
            }        
            p.CPQ_Description_Short__c = shortDescription;
        }
        else {
            p.CPQ_Description_Short__c = '';
        }
    }

}