trigger getContractId on SCRB_SalesOrder__c (before update) {
    
    for(SCRB_SalesOrder__c SO : Trigger.new){
        
        if (Trigger.oldMap.get(SO.Id).StatusCode__c != SO.StatusCode__c && SO.StatusCode__c == 'Fulfilled')
        {
            try {
                String contractId = [Select id from Contract where Accountid = :SO.AccountId__c order by id desc limit 1].id;
                SO.Contract__c = contractId;
            }
            catch(Exception ex){
                //nothing
            }
            
        }
    }

}