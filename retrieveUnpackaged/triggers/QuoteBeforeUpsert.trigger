trigger QuoteBeforeUpsert on SBQQ__Quote__c  (before insert, before update) 
{
// SB provides a way to filter contracted pricing on just one account - by adding ContractedAccountID__c which needs to store the contracted pricing Account.ID
// so users do not have to lookup an account, we have a picklist (Contracted_Pricing_Account__c) that provides 3 options (quoted account, quoted account parent and alliance account)
//  this trigger will set ContractedAccountID__c (contracted pricing filter) based on Contracted_Pricing_Account__c (pickist where user sets account type to filter on)
// if the user selects other than "quoted account" this trigger validates existence of parent/alliance account; if validation fails, it rolls back to "quoted account"
// resources http://blog.jeffdouglas.com/2009/04/20/writing-bulk-triggers-for-salesforce/
// SB documentation on filtering contracted pricing: http://community.steelbrick.com/t5/Contracts-Renewals-Subscriptions/Apply-Contracted-Price-from-a-Different-Account/ta-p/183
// In tandem with filtering contracted pricing, also need to ignore hierarchy pricing: http://community.steelbrick.com/t5/Prices-Discounts-Knowledge-Base/Ignore-Parent-Contracted-Prices/ta-p/161

// constants defining possible account types used for ContractedAccountID__c (should never have to change these lines)
    String strQuotedAccount = 'Quoted Account';
    String strParentAccount = 'Quoted Parent Account';
    String strAllianceAccount = 'Alliance Account';
    
// create map of picklist values - serves to isolate changes to this trigger if you change picklist values later
// first string is PickList Value, second string is code reference, if no match is found then error

    Map<String, String> mapContractedPricingAccount = new Map<String, String>();
    mapContractedPricingAccount.put( 'Quoted Account', strQuotedAccount);
    mapContractedPricingAccount.put( 'Quoted Account Parent', strParentAccount);
    mapContractedPricingAccount.put( 'Alliance Account', strAllianceAccount);    
    
// when quotes need contracted pricing from a different account (parent or alliance), stores the relevant Account IDs
    Set<ID> setAccount_ID = new Set<ID>(); 
   


// cycle through quotes affected by the trigger, handle the simple case when contracted pricing account = quoted account
// for all quotes affected by the trigger
    for (SBQQ__Quote__c SBQuoteTriggerNew : Trigger.new) 
    {
        // validate expected Picklist value was received and throw an error if not
            if (mapContractedPricingAccount.get(SBQuoteTriggerNew.Contracted_Pricing_Account__c) == null)
            {
                SBQuoteTriggerNew.Contracted_Pricing_Account__c.addError('Unexpected Picklist Value for "Contracted Pricing Account" ecountered in trigger "setContractedPriceAccount on SBQQ__Quote__c." Please contact your administrator');
                //Next line was a test to try setting contracted account id to a dummy account so that it wouldn't pick up contracted prices
                //SBQuoteTriggerNew.ContractedAccountID__c = '0011900000LtcBn';
            }
        // If we want "contracted pricing account" to be the same as the "account being quoted" (aka native contract)
            if( mapContractedPricingAccount.get(SBQuoteTriggerNew.Contracted_Pricing_Account__c) == strQuotedAccount)
            {
                // Then set ContractedAccountID__c to SBQQ__Account__c which means contracted pricing will be filtered on the quoted account (SBQQ__Account__c)
                    SBQuoteTriggerNew.ContractedAccountID__c = SBQuoteTriggerNew.SBQQ__Account__c;
            } 
        // Else, add the "account being quoted" to the set of accounts which we will use to pull parents/alliances
            else 
            {
                setAccount_ID.add( SBQuoteTriggerNew.SBQQ__Account__c);
            }
    }
    
// create a map of Accounts that pulls the Account.ParentID and Account.Alliance_Parent_Account__c 
    Map<ID, Account> mapAccount = new Map<ID, Account>([ Select ParentID, Alliance_Parent_Account__c from Account where ID in :setAccount_ID]);


    // Picklist value: Quoted Account Parent 
    // Account's Parent of SBQQ__Account__c - Account.ParentID where ID = SBQQ__Account__c
    
    // Picklist Value: Alliance Account
    // filters on the account's alliance (aka consortia account) - 
    
// cycle through quotes affected by trigger and update the ContractedAccountID__c
        for (SBQQ__Quote__c SBQuoteTriggerNew : Trigger.new) 
        {
            // If Parent Account then set it to parent else If Alliance set it to Alliance Account ("quoted account" ignored, already handled above)
            if( mapContractedPricingAccount.get(SBQuoteTriggerNew.Contracted_Pricing_Account__c) == strParentAccount)
            {
                // if value is not null then use it, else throw an error 
                if ( mapAccount.get(SBQuoteTriggerNew.SBQQ__Account__c).ParentID != null)
                {
                    SBQuoteTriggerNew.ContractedAccountID__c = mapAccount.get(SBQuoteTriggerNew.SBQQ__Account__c).ParentID;
                }
                else
                {
                     SBQuoteTriggerNew.Contracted_Pricing_Account__c.addError( strParentAccount + ' not found for quoted Account. Please contact your administrator.');
                }
            }
            if( mapContractedPricingAccount.get(SBQuoteTriggerNew.Contracted_Pricing_Account__c) == strAllianceAccount)
            {
                // if value is not null then use it, else throw an error 
                if ( mapAccount.get(SBQuoteTriggerNew.SBQQ__Account__c).Alliance_Parent_Account__c != null)
                {
                    SBQuoteTriggerNew.ContractedAccountID__c = mapAccount.get(SBQuoteTriggerNew.SBQQ__Account__c).Alliance_Parent_Account__c;
                }
                else
                {
                    SBQuoteTriggerNew.Contracted_Pricing_Account__c.addError( strAllianceAccount + ' not found for quoted Account. Please contact your administrator.');
                }
            }
        }    


}