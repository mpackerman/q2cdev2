trigger updateOppAndSalesOrderOnFulfilled on SBQQ__Quote__c (after update) {

    for(SBQQ__Quote__c quote : Trigger.new){

        SBQQ__Quote__c oldQuote = Trigger.oldMap.get(quote.Id);
        
        if( quote.SBQQ__Status__c == 'Fulfilled' && oldQuote.SBQQ__Status__c != 'Fulfilled' && (quote.Record_Type_Name__c == 'Order' || quote.Record_Type_Name__c == 'Order - Approved') )
        {
            Opportunity quoteOpp = [select id, stagename from opportunity where id = :quote.SBQQ__Opportunity2__c];
            quoteOpp.stagename = 'Order Fulfilled';
            update quoteOpp;
            
            SCRB_SalesOrder__c salesOrder = [select id, StatusCode__c from SCRB_SalesOrder__c where id = :quote.Sales_Order__c];
            salesOrder.StatusCode__c = 'Fulfilled';
            update salesOrder;
            
            final Map <Id, SCRB_SalesOrderLineItem__c> orderLineItems = new Map <Id, SCRB_SalesOrderLineItem__c> ([select Id, Fullfilled__c From SCRB_SalesOrderLineItem__c where SalesOrderId__c = :quote.Sales_Order__c]);
            
            for (SCRB_SalesOrderLineItem__c sOI: orderLineItems.values()) {
                sOI.Fullfilled__c = true;
                update sOI;
            }  
            
            
            
            
            
            
        }
    }

}