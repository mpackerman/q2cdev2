trigger AccountBeforeInsertUpdate on Account (before insert, before update) 
{ // for accounts in the trigger, sets the parent ID based on master registry
// for accounts outside the trigger: if an Account inside the trigger is linked to a Master Registry Parent Org - all the accounts that are children of the account being added/changed need to be updated
// This trigger will fire recursively for cases when you modify the Account related to a Master Registry Parent Org (an Org with related children). When you modify the parent, we have to make sure the child Accounts get their ParentID updated and these accounts would be outside the original trigger's scope. The trigger will fires up to the number of generations in the Master Registry - I think there were only a handful of 3 generations. I don't think running 3 times will be an issue. 

string strTestChildAccountID = '00123000002R3Su'; // this should be a child account, the phone field will be updated to show which trigger elements fired /did not fire

    // retrieve all master registry ID's from upserted accounts
        Set<ID> setMasterRegistryCurrent_ID = new Set<ID>(); 
                
        for (Account accountTriggerNew : Trigger.new) 
        {
            // testing only to see if ASFS gets updated when you add the District; asfs = 
            if (accountTriggerNew.ID == strTestChildAccountID && accountTriggerNew.Org_ID2__c != null && accountTriggerNew.ParentID == null && accountTriggerNew.Parent_Org_ID2__c != null  )
            {
                accountTriggerNew.Phone = 'section 1 fired!';
            }
            
        
            // only add to the set if org ID is not null and: (inserting a new record OR Org_ID2__c is changed OR (Org has a parent AND either (ParentID is Null or ParentID is changed) )
            if ( 
                accountTriggerNew.Org_ID2__c != null && 
                ( 
                    Trigger.isInsert || 
                    Trigger.oldMap.get(accountTriggerNew.ID).Org_ID2__c != accountTriggerNew.Org_ID2__c || 
                    ( accountTriggerNew.Parent_Org_ID2__c != null && accountTriggerNew.ParentID == null) // don't know why this failed accountTriggerNew.Org_ID2__r.Parent_Global_ID__c != null
                )
            )
            
            {
                setMasterRegistryCurrent_ID.add( accountTriggerNew.Org_ID2__c);
                if (accountTriggerNew.ID == strTestChildAccountID)
                {
                    accountTriggerNew.Phone = 'section 2 fired!';
                }
                    
            // testing only to see if ASFS gets updated when you add the District; asfs = 00123000002R3Su
            if (accountTriggerNew.ID == strTestChildAccountID)
            {
                accountTriggerNew.Phone = 'section 1a fired!';
            }

            }
             // if the Org ID is changed and the old value is not null grab the old values of the Org ID bc if the old value had children, the children need to be reparented
            if 
            ( 
                Trigger.isUpdate && 
                Trigger.oldMap.get(accountTriggerNew.ID).Org_ID2__c != null && 
                Trigger.oldMap.get(accountTriggerNew.ID).Org_ID2__c != accountTriggerNew.Org_ID2__c 
            )
            {
                setMasterRegistryCurrent_ID.add( Trigger.oldMap.get(accountTriggerNew.ID).Org_ID2__c); //addign the old Mater Registyr Org ID value

            // testing only to see if ASFS gets updated when you add the District; asfs = 00123000002R3Su
            if (accountTriggerNew.ID == strTestChildAccountID)
            {
                accountTriggerNew.Phone = 'section 1b fired!';
            }

            }
           
        }
        
    // retrieve master registry records for all the above
        Map<ID, Master_Registry__c> mapMasterRegistryCurrentParent = new Map<ID, Master_Registry__c> ([SELECT Parent_Global_ID__c FROM Master_Registry__c WHERE ID in :setMasterRegistryCurrent_ID]);

    // build set of just the Master Registry Parent ID's (need this set to avoid SOQL loop)
        Set<ID> setMasterRegistryParent_ID = new Set<ID>(); // all parent master registry ID's related to setMasterRegistryChild
        for (Master_Registry__c objMasterRegistryParent : mapMasterRegistryCurrentParent.values()) 
        {
            if( objMasterRegistryParent.Parent_Global_ID__c != null) // must skip the nulls else you will retrieve Accounts with NULL master registry org IDs in a later step
            {    
                setMasterRegistryParent_ID.add( objMasterRegistryParent.Parent_Global_ID__c ); 
            }
        }
    // use set of Master Registry Parent ID's to build map of Accounts that match the Master Registery Parent IDs 
        Map<ID, Account> mapAccountParent = new Map<ID, Account>([ Select Org_ID2__c from Account where Org_ID2__c in :setMasterRegistryParent_ID]); // all Parent Accounts related to idMasterRegistryParent

    // transopose the mapAccountParent to build map<ID, ID> as <Master Registry Parent ID, Account ID> - building this avoids "per record soql" in the next step
        Map<ID, ID> mapMasterRegistryAccountParent = new Map<ID, ID>();
        for ( ID idAccount : mapAccountParent.keyset()) 
        {
            mapMasterRegistryAccountParent.put(mapAccountParent.get(idAccount).Org_ID2__c, idAccount);
        }

    // for each triggered account, update the parent ID
        for (Account accountTriggerNew : Trigger.new) //now cycle through the records in the trigger and populate the Account.ParentID
        {
            
        // if Org ID is null or the Org ID has no parent, then set parent ID to Null
            if
            ( 
                accountTriggerNew.Org_ID2__c == NULL

            )
            {
                 accountTriggerNew.ParentID = null;
                    if (accountTriggerNew.ID == strTestChildAccountID)
                    {
                        accountTriggerNew.Phone = 'section 3 Org_ID2__c == null fired!';
                    }
                    
        //only update ParentID if we received a result (if no result, exists then do nothing e.g. if the org id didn't change)
            } else if (mapMasterRegistryCurrentParent.get(accountTriggerNew.Org_ID2__c) != null && mapMasterRegistryAccountParent.get(mapMasterRegistryCurrentParent.get(accountTriggerNew.Org_ID2__c).Parent_Global_ID__c) != null)
            {
              accountTriggerNew.ParentID = mapMasterRegistryAccountParent.get(mapMasterRegistryCurrentParent.get(accountTriggerNew.Org_ID2__c).Parent_Global_ID__c);
                    if (accountTriggerNew.ID == strTestChildAccountID)
                    {
                        accountTriggerNew.Phone = 'section 3 should have worked fired!';
                    }

            } 


            // testing only
            if (mapMasterRegistryCurrentParent.get(accountTriggerNew.Org_ID2__c) != null )
            {
              accountTriggerNew.ParentID = mapMasterRegistryAccountParent.get(mapMasterRegistryCurrentParent.get(accountTriggerNew.Org_ID2__c).Parent_Global_ID__c);
                    if (accountTriggerNew.ID == strTestChildAccountID)
                    {
                        accountTriggerNew.Phone = 'section 3 first get worked ';
                    }

            } 

        }    
    // retrieve all master registry records that are children of the current master registry records
        Map<ID, Master_Registry__c> mapMasterRegistryCurrentChild = new Map<ID, Master_Registry__c> ([SELECT ID FROM Master_Registry__c WHERE Parent_Global_ID__c != null AND Parent_Global_ID__c in :setMasterRegistryCurrent_ID]);

    // convert mapMasterRegistryCurrentChild to a set of IDs
        Set<ID> setMasterRegistryChild_ID  = new Set<ID>();
        for ( ID classID : mapMasterRegistryCurrentChild.keyset()) 
        {
            setMasterRegistryChild_ID.Add( classID);
        }
    // set of accounts that need this trigger fired        
        Set<Account> setAccountchild = new Set<Account>([ Select ParentID, Org_ID2__c From Account where Org_ID2__c in :setMasterRegistryChild_ID]);

    // for each setAccountChild - update the ParentID to null to invoke this trigger on the child accounts
        for ( Account objAccount : setAccountchild)
        {
            objAccount.ParentID = null;
            Update objAccount;
        }

}