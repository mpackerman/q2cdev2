trigger new_fulfillment_task on Lead ( after insert) {

System.Debug('ENTERED INTO TRIGGER');

//Map<String, Lead> leadMap = new Map<String, Lead>();

// loop through trigger records
for (integer i=0; i<Trigger.new.size(); i++)
{


if (trigger.isInsert) 
{

System.Debug('INSIDE INSERT');

    if ((Trigger.new[i].items_to_send__c != null) && (Trigger.new[i].Quantity_Sent__c != null))
    {
        //Map<String, Lead> leadMap = new Map<String, Lead>();
        
        
        Task act = new Task();
        act.Subject = 'Fulfillment Request';
        act.Description = 'The following packets were requested : ' + Trigger.new[i].items_to_send__c;
        
        act.items_to_send__c = Trigger.new[i].items_to_send__c;
        
        
        act.Quantity_to_Send__c = Double.valueOf(Trigger.new[i].Quantity_Sent__c);
        
        act.whoId = Trigger.new[i].Id;
        
        // act.ownerId = user_id;
        act.Status = 'Completed';
        
        
        act.ActivityDate = System.today();

        
        act.RecordTypeId = '012700000001TwxAAE'; // Fulfillment Request Record Type
        //act.RecordTypeId = [SELECT RecordType.Id FROM RecordType WHERE RecordType.Name = 'Fulfillment Request'];
        
        // act.whatId = what_id;
        //act.IsClosed = true; //this line doesn't compile.
        Database.SaveResult sr = Database.insert (act);
    }
}
    }

}