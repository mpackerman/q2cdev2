trigger setGlobalID on Master_Registry__c (before insert) {

    for (Master_Registry__c mr : Trigger.new)
    {
        String highestGlobalId = [Select id, name from Master_Registry__c where Global_ID__c < 6000000 order by name desc limit 1].name;
        Integer highestGlobalIdNum = integer.valueof(highestGlobalId);
        highestGlobalIdNum++;
        String newGlobalId = string.valueof(highestGlobalIdNum);
        mr.name = newGlobalId;
    }

}