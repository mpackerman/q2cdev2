trigger LeadTrigger on Lead (after update, after undelete, before delete) {

  set<ID>     cIds  = new set<ID>();
  List<String>   apIds  = new List<String>();
  List<ActionPlan__c>  deletePermantently_apIds= new List<ActionPlan__c>();  
    
  
  //check if assignment rules need to be run again
  
  if(trigger.isUpdate && trigger.isAfter){
    Database.DMLOptions dmo = new Database.DMLOptions();     //initialize dmlOptions for potential use in re-assigning the lead
        dmo.assignmentRuleHeader.useDefaultRule= true;      //this is the option in the dmlOptions to re-assign the leads
      
    List<ID> ownerIds = new List<ID>();
    for(Lead l : trigger.new){
      ownerIds.add(l.ownerId);
    }

    Map<Id, User> ownersToExclude = new Map<Id, User>([
      SELECT Id
        , UserRoleId
      FROM User
      WHERE Id IN: ownerIds
      AND UserRoleId = '00E70000000tcgK'
    ]);

    List<ID> assignmentRulesToTriggerFuture = new List<ID>();
    //if dmlOptions is used again in this trigger, you should move this to inside the lead for loop
    //Map<ID, List<ID>> ownerToLeadMap = new Map<ID, List<ID>>();//for later use if the owner is inactive - we want to reassign the lead
    for(Lead l : trigger.newMap.values()){//using the map so we update the same list if the owner is inactive
      if(l.isConverted == false){  //don't want this to happen for converted leads
        if(l.mkto2__Lead_Score__c!=trigger.oldMap.get(l.id).mkto2__Lead_Score__c && l.RecordTypeId == '012700000001NDJ' && !ownersToExclude.containsKey(l.ownerId) &&
        (userInfo.getUserId() == '00570000001va9DAAQ' || test.isRunningTest())){//if the score has passed the threshold, re-run assignment rules
          assignmentRulesToTriggerFuture.add(l.id);
        }
        /*
        if(ownerToLeadMap.containsKey(l.ownerId)){//if the owner key already exists
          ownerToLeadMap.get(l.ownerId).add(l.id);//add the lead to the list of those to be checked for an inactive owner
        }
        else{
          ownerToLeadMap.put(l.ownerId, new List<ID>{l.id});//no owner key yet - create it with the initial lead id
        }
        */
      }
    }
    if(assignmentRulesToTriggerFuture.size()>0){
      system.debug('Firing assignment rules for leads: ' + assignmentRulesToTriggerFuture);
      LeadTriggerUtils.runAssignemntRules(assignmentRulesToTriggerFuture);
    }
    /*
    if(ownerToLeadMap.size()>0){
      for(User u : [Select Id from User where isActive = false and ID IN : ownerToLeadMap.keySet()]){//if the owner is inactive we want to re-assign the lead
        for(ID leadId : ownerToLeadMap.get(u.id)){//iterate over leads owned by an inactive user
          trigger.newMap.get(leadId).setOptions(dmo);
        }
      }
    }
    */
  }
  //Delete related action plans
  if ( trigger.isdelete ){
    for( Lead l : trigger.old ){
          cIds.add( l.Id );
       }

    /* GET Action Plans to delete from recycle bin */
       deletePermantently_apIds = [ select Id, Name , LastModifiedDate from ActionPlan__c where Lead__c in : cIds and isDeleted = true ALL ROWS ];
       
       if ( deletePermantently_apIds.size() >0 ){       
      Database.emptyRecycleBin(deletePermantently_apIds);
       }

    List<ActionPlan__c> apList =  [ select Id from ActionPlan__c where Lead__c in : cIds ];
    for( ActionPlan__c ap : [ select Id from ActionPlan__c where Lead__c in : cIds ] ){
          apIds.add( ap.Id );
       }
       
       if ( apIds.size() >0 ){
      ActionPlansBatchDelete aPBatch = new ActionPlansBatchDelete(apIds, Userinfo.getUserId());
      Database.ExecuteBatch( aPBatch );
    }    
  }
  
  //Undelete related action plans
  if ( trigger.isUnDelete ){
    for( Lead l : trigger.new ){
          cIds.add( l.Id );
       }
       list <ActionPlan__c> aPs = [ select Id from ActionPlan__c where Lead__c in : cIds ALL ROWS ];
       
       try{
         if(ActionPlanObjectTriggerTest.isTest){
           //throw dmlException
           insert new Contact();  
         }
         //undelete aPs;
         Database.undelete( aPs,false);
       } catch ( Dmlexception e ){
         for (Lead l: trigger.new){
           l.addError('You can not undelete an action plan whose related object is deleted.');
      }
       }
  }

}