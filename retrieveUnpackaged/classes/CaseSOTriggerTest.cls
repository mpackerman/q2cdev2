@isTest 
private class CaseSOTriggerTest {

    static testMethod void validateSalesorderUpdate() {
       Case c = new Case(Sales_Order_Name__c='a097000000ROQQX');
       // Insert case  
    
       insert c;
    
       // Retrieve case  
    
       c = [SELECT Sales_Order__c FROM case WHERE Id =:c.id];

       // Test that program correctly added the Sales Order value  
    
       // to the Sales Order field  
    
       System.assertEquals('a097000000ROQQX', c.Sales_Order__c);
    }
}