public with sharing class LeadTriggerUtils {

	@future
	public static void runAssignemntRules(List<ID> leads){
		Database.DMLOptions dmo = new Database.DMLOptions();     //initialize dmlOptions for potential use in re-assigning the lead
        dmo.assignmentRuleHeader.useDefaultRule= true;			//this is the option in the dmlOptions to re-assign the leads
		List<Lead> toUpdate = new List<Lead>();
		system.debug('Running Assignment Rules ' + leads);
		for(ID l : leads){
			toUpdate.add(new Lead(id=l));
		}
		Database.update(toUpdate, dmo);
	}
}