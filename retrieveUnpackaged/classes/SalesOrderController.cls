public class SalesOrderController {

    public class MissingAccountException extends Exception {}
    public class NoLineItemsException extends Exception {}
    
    
    public Account account {get; private set;}
    public Opportunity opportunity {get; private set;}
    public SCRB_SalesOrder__c order {get; private set;}
    public OpportunityLineItem oppLineItem {get; private set;}
    public SCRB_SalesOrderLineItem__c[] salesOrderLineItems {get; private set;}
    public SCRB_SalesOrderLineItem__c salesOrderLineItem {get; private set;}
    public List<SCRB_SalesOrderLineItem__c> lineItems= new List<SCRB_SalesOrderLineItem__c>();
    private integer itemCount=0;
    
    private PriceBookEntry pbEntry;

    public SalesOrderController() {
               
        Boolean ShowPartner=false;  
        Id id = ApexPages.currentPage().getParameters().get('id');
        //Id accountId=ApexPages.currentPage().getParameters().get('aId');
        Id opportunityId = ApexPages.currentPage().getParameters().get('oId');
        
        
                  
        opportunity = (opportunityId == null) ? new Opportunity() : 
        [SELECT name, ownerId, accountID, CloseDate, Description, StageName, Pricebook2Id FROM Opportunity WHERE id = :opportunityId];
              
        if  (opportunity.AccountId != null){ 
             account =new Account();   
             account= [SELECT name, Org_ID3__c,accountnumber, billingstreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,ShippingStreet,  ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, phone, fax FROM account WHERE id = :opportunity.AccountId] ;
        }else {
             throw new MissingAccountException('Cannot create an order from this opportunity: Please assign an account to the opportunity.');        
        }
        
        order = (id == null) ? new SCRB_SalesOrder__c() : 
        [SELECT SCRB_SalesOrder__c.Name FROM SCRB_SalesOrder__c WHERE id = :id];
                      

        //Pre-populate Opportunity and account information
             order.Name=Opportunity.Name;
             order.AccountId__c=opportunity.AccountId;
             order.OpportunityId__c = opportunity.id;
             order.Description__c=opportunity.description;
             order.discountAmount__c=0;
             order.freightAmount__c=0; 
//             order.Type__c='Internal Sales Order';
             order.StatusCode__c='New';
             order.RequestedShipmentOn__c=Date.Today();
             order.DocumentDate__c=Date.Today();            
             order.BillingStreet__c=account.BillingStreet;
             order.BillingCity__c=account.BillingCity;
             order.BillingState__c=account.BillingState;
             order.BillingPostalCode__c=account.BillingPostalCode;
             order.BillingPhone__c=account.Phone;
             order.BillingFax__c=account.Fax;
             order.BillingCountry__c=account.BillingCountry;                                
             order.Org_ID__c=account.Org_ID3__c; 
             order.ShippingStreet__c= account.ShippingStreet;
             order.ShippingCity__c=account.ShippingCity;
             order.ShippingState__c=account.ShippingState;
             order.ShippingPostalCode__c=account.ShippingPostalCode;
             order.ShippingPhone__c=account.Phone;
             order.ShippingFax__c=account.Fax;
             Order.ShippingCountry__c=account.ShippingCountry;
             Order.Owner__c=opportunity.ownerId;
             
             //Retrieve the PriceBook name for Prepopulation in order 
             string pbID = opportunity.Pricebook2Id;
             PriceBook2 pb = (opportunity.Pricebook2Id == null) ? new PriceBook2() : 
            [Select Id, Name from Pricebook2 where Id=:pbID];
              order.Pricebook__c=pb.Name;
             
        //Create a new SCRB_SalesOrderLineItem__c and populate a row for each oppLineItem that exists
        
         integer i = 0;
         salesOrderLineItems = new SCRB_SalesOrderLineItem__c[0];
            for(OpportunityLineItem opp: [SELECT Id, Discount, Opportunity_Product_ID__c, SortOrder, PricebookEntryId, Description, ProductCode__c, OptionId__c, LineType__c, ProductIDForSalesOrder__c, Quantity, ListPrice, UnitPrice, TotalPrice, ServiceDate FROM OpportunityLineItem WHERE Opportunityid = :opportunityId])
            {
                
                string pbeID = opp.PricebookEntryId;
                pbEntry = (pbeId == null) ? new PriceBookEntry() : 
                [Select Id, Name,Pricebook2Id, Product2Id, ProductCode from PricebookEntry where Id=:pbeID];
                 
                           
                salesOrderLineItem = new SCRB_SalesOrderLineItem__c();
                salesOrderLineItem.SalesOrderId__c=order.Id;
                salesOrderLineItem.SortOrder__c = i+1;
                If (opp.LineType__c == 'Product/Service'){
                salesOrderlineItem.ProductId__c = opp.ProductIDForSalesOrder__c; }
                Else
                {salesOrderlineItem.ProductId__c = opp.OptionId__c; } 
                salesOrderlineItem.Product_Code__c = opp.ProductCode__c; 
                salesOrderLineItem.Name = opp.ProductCode__c;
                salesOrderLineItem.Quantity__c = opp.Quantity;
                salesOrderLineItem.ListPrice__c= opp.ListPrice;
                salesOrderLineItem.SalesPrice__c=opp.UnitPrice;
                salesOrderLineItem.TotalPrice__c=opp.TotalPrice;
                salesOrderlineItem.Discount__c = opp.Discount;
                salesOrderlineItem.Opportunity_Product_ID__c = opp.Opportunity_Product_ID__c;
                salesOrderLineItem.RequestedShipmentOn__c=Date.Today();
                SalesOrderLineItems.add(salesOrderLineItem);
                i++;
            }
            itemCount=i;
            if (itemCount == 0) {
                throw new NoLineItemsException('Unable to create Sales Order: Products must be added to the Opportunity before creating a Sales Order.');
            }
            System.Debug('Finished Page Load');
                         
              }
             
             //Returns the user back to the opportunities page this was launched from
    public PageReference cancel(){
         return (new ApexPages.StandardController(opportunity)).view();
    }
     //saves order and child orders
    public PageReference save() {
                     System.Debug('Running Save Method');
                     
                   try 
                   {
                     insert order;
                     System.Debug(order.id);
                     for(SCRB_SalesOrderLineItem__c li: salesOrderLineItems)
                     {
                         li.SalesOrderId__c=order.id;                   
                     }
                     insert salesOrderLineItems;
                     //If the Opportunity Stage is not 'Closed Won' then update it
                     if (Opportunity.StageName != 'Closed Won'){
                         Opportunity.StageName='Closed Won';
                         update Opportunity;
                                              }                    
                   }catch(System.DMLException e) 
                   {
                       ApexPages.addMessages(e);
                       return null;
                   }         
                             
                   //  After Save, navigate to the default view page:  
                    return (new ApexPages.StandardController(order)).view();
               }
 
       
  

     public PageReference loadHelpPage(){
        PageReference pg = new PageReference('/apex/CreateSalesOrderHelp');
        return pg;
    }
   


  }