@isTest 
private class CaseTriggerTest {

    static testMethod void validateOpportunityUpdate() {
       Case c = new Case(Opportunity__c='0067000000LmyMi');
       // Insert case  
    
       insert c;
    
       // Retrieve case  
    
       c = [SELECT Opportunity__c FROM case WHERE Id =:c.id];

       // Test that program correctly added the Opportunity value  
    
       // to the Opportunity field  
    
       System.assertEquals('0067000000LmyMi', c.Opportunity__c);
    }
}