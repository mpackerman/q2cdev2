@isTest
private class SalesOrderTestController {

        
     
      
      private static Opportunity CreateTestOpp(){
        //Populate the Opportunity ID query parameter
       Account acct = new Account();
       acct.name='TestAcct';
       insert acct;
       System.Debug('Account:' + acct.id);
       
       Opportunity op = new Opportunity(); 
       OpportunityLineItem li= new OpportunityLineItem();
       List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
       Product2 product = new Product2();
       product.Name='testProduct223';
       insert product;
       PriceBook2 pbook2=[select id, Name from PriceBook2 where Name='Standard Price Book'];
              
       PriceBookEntry prod = new PricebookEntry();
       prod.UnitPrice = 2.32;
       prod.PriceBook2Id = pbook2.id;
       prod.UseStandardPrice=FALSE;
       prod.Product2ID=product.id;
       prod.IsActive     = true;
       insert prod;
       
       op.AccountId = acct.id;
       op.Name='Test Opportunity223';
       op.StageName = 'Submit Pending';
       op.CloseDate= Date.Today();
       op.Brand__c = 'Aventa';
        op.Preliminary_Interest__c = 'Course Sales – K12/Aventa';
        op.Launch_Date__c  = 'Summer 13';
        op.Semester_Course_Enrollments__c= 5;
        op.Service_Delivery_State__c='FL';
       insert op;



       li.PriceBookEntryID = prod.id;
       li.OpportunityId = op.Id;
       li.UnitPrice=2.32;
       li.Quantity= 1;
       lineItems.add(li);
       insert lineItems[0];
       return op;

              } 
              
       private static SCRB_SalesOrder__c CreateTestOrder(){
        Opportunity op= CreateTestOpp();
        SCRB_SalesOrder__c order = new SCRB_SalesOrder__c();
        SCRB_SalesOrderLineItem__c[] salesOrderLineItems = new SCRB_SalesOrderLineItem__c[1];
        SCRB_SalesOrderLineItem__c salesOrderLineItem = new SCRB_SalesOrderLineItem__c();
        
        order.name = op.name;
        order.AccountId__c = op.AccountId;
        order.OpportunityId__c = op.Id;
        order.StatusCode__c = 'Submitted';
        order.BillingState__c='FL';
        order.ShippingState__c='FL';
        insert order;
        
         Product2 product = new Product2();
         product.Name='testProduct223';
         insert product;
         PriceBook2 pbook2=[select id, Name from PriceBook2 where Name='Standard Price Book'];
              
       PriceBookEntry prod = new PricebookEntry();
       prod.UnitPrice = 2.32;
       prod.PriceBook2Id = pbook2.id;
       prod.UseStandardPrice=FALSE;
       prod.Product2ID=product.id;
       prod.IsActive     = true;
       insert prod;
       
                  salesOrderLineItem.SalesOrderId__c=order.Id;
         salesOrderLineItem.SortOrder__c = 1;
         salesOrderlineItem.ProductId__c = product.id;
         salesOrderLineItem.Quantity__c = 1;
         salesOrderLineItem.ListPrice__c= 2.32;
         salesOrderLineItem.SalesPrice__c=2.32;
         salesOrderLineItem.TotalPrice__c=2.32;
         salesOrderLineItem.Product_Code__c = 'test code';
         salesOrderLineItem.RequestedShipmentOn__c=Date.Today();
         //SalesOrderLineItems.add(salesOrderLineItem); 
         
         insert SalesOrderLineItem;     
         
        return order;               
       }

      //*******************************************************************  
      //*Tests that the Page Loads with after being passed an opportunity id.  
      //**********************************************************************
      public static testMethod void TestLoadPage() {
        
        PageReference pageRef = Page.TestSalesOrder;
        Test.setCurrentPage(pageRef);
        Opportunity op= CreateTestOpp();
                          
       ApexPages.currentPage().getParameters().put('oId', op.Id);
  
      //Load the Page
        SalesOrderController controller = new SalesOrderController();
            
           
             }
             
              //****************************************************************************
        //*  Tests that the Page saves with defaults  
        //**************************************************************************** 
          public static testMethod void TestCreateOrderSubmit() {
      
            PageReference pageRef = Page.TestSalesOrder;
            Test.setCurrentPage(pageRef);
      
            Opportunity op = CreateTestOpp();  
            SCRB_SalesOrder__c ord = new SCRB_SalesOrder__c();
            //Populate the Opportunity ID query parameter
            ApexPages.currentPage().getParameters().put('oId', op.Id);
  
            //Load the Page
            SalesOrderController controller = new SalesOrderController();
            PageReference nextPage = controller.save();//.getUrl();
            System.Debug('Next Page:' + nextpage);
      
          }
          
          //Tests that the Cancel Method returns to the appropriate source opportunity        
         public static testMethod void TestCreateSalesOrderCancel(){
     
            PageReference pageRef = Page.TestSalesOrder;
            Test.setCurrentPage(pageRef);
      
            Opportunity op = CreateTestOpp();  
            ApexPages.currentPage().getParameters().put('oId', op.Id);
  
            SalesOrderController controller = new SalesOrderController();
            string cancelPage = controller.cancel().getUrl();
        
            System.Debug('Cancel Page:'+ cancelPage);
         
            //System.assertEquals(cancelPage,'/'+op.Id);
        
     }

           //Tests that an existing sales order can be cancelled.  
      public static testMethod void TestCancelSalesOrderOK() {
      
        PageReference pageRef = Page.TestSalesOrder;
        Test.setCurrentPage(pageRef);
      
         SCRB_SalesOrder__c ord = CreateTestOrder();

        ApexPages.currentPage().getParameters().put('id', ord.Id);
  
      //Load the Page
        CancelSalesOrderController controller = new CancelSalesOrderController();
            
        String nextPage = controller.OK().getUrl();      
       //User u1 = [select id from User where alias='auser'];

             }
             
              //Tests that the Cancel Method returns to the appropriate source opportunity        
     public static testMethod void TestCancelSalesOrderCancel(){
     
        PageReference pageRef = Page.TestSalesOrder;
        Test.setCurrentPage(pageRef);
       SCRB_SalesOrder__c ord = CreateTestOrder();
        
                
        ApexPages.currentPage().getParameters().put('id', ord.id);
  
        CancelSalesOrderController controller = new CancelSalesOrderController();
        string cancelPage = controller.cancel().getUrl();
        
        System.Debug('Cancel Page:'+ cancelPage);
         
        // System.assertEquals(cancelPage,'/006A0000003CFdD');
        
     }

          
        }