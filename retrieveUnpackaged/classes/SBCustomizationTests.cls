@isTest(SeeAllData=true)
public class SBCustomizationTests {
    
    static testMethod void testQuotedAccount() {
    
        RecordType art = [select id,Name from RecordType where SobjectType='Account' and Name='FuelEd' Limit 1];
        RecordType ort = [select id,Name from RecordType where SobjectType='Opportunity' and Name='FuelEd' Limit 1];
        RecordType qrt = [select id,Name from RecordType where SobjectType='SBQQ__Quote__c' and Name='Quote - Draft' Limit 1];
        
        // Create parent MR
        Master_Registry__c testMR1 = new Master_Registry__c (
                Institution_Name__c='Parent Test MR',
                Name='4129450',
                No_Parent__c=true
        );
        insert testMR1;
        
        // Create child MR
        Master_Registry__c testMR2 = new Master_Registry__c (
                Institution_Name__c='Child Test MR',
                Name='4129451',
                No_Parent__c=false,
                Parent_Global_ID__c=testMR1.id
        );
        insert testMR2;
        
        // Create the Parent/Alliance account
        Account testAcct1 = new Account(
                Name='Parent/Alliance Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                Org_ID2__c=testMR1.id
        
        );
        insert testAcct1;
        
        // Create the Child account
        Account testAcct2 = new Account(
                Name='Child Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                ParentId=testAcct1.id,
                Alliance_Parent_Account__c=testAcct1.id,
                Org_ID2__c=testMR2.id
        );
        insert testAcct2;
        
        System.debug('Parent Account; ' + testAcct2.parentid);
        System.debug('Alliance Account; ' + testAcct2.Alliance_Parent_Account__c);
        
        // Create the Pricing Opportunity
        Opportunity testOpp = new Opportunity (
                Name='Opportunity for testing',
                AccountId=testAcct2.id,
                Type='Current Customer: New Program',
                CloseDate=date.today(),
                StageName='Prospect',
                Probability=13,
                ForecastCategoryName='Pipeline',
                Service_Delivery_State__c='FL',
                Preliminary_Interest__c='Online Courses',
                Estimated_Service_Start_Date__c=(date.today()+1),
                Estimated_Service_End_Date__c=(date.today()+365),
                Refund_Drop_Policy__c='Custom 14-day: 100%',
                RecordType=ort
        );
        insert testOpp;
        
        // Create the Pricing Quote
        SBQQ__Quote__c testQuote = new SBQQ__Quote__c (
                SBQQ__Account__c=testAcct2.id,
                SBQQ__Opportunity2__c=testOpp.id,
                SBQQ__Status__c='Draft',
                SBQQ__Type__c='Quote',
                SBQQ__StartDate__c=(date.today()+1),
                SBQQ__EndDate__c=(date.today()+365),
                SBQQ__ExpirationDate__c=(date.today()+30),
                SBQQ__PaymentTerms__c='Net 30',
                Drop_Policy__c='50% refund for days 0-14',
                Auto_Renew__c='NO',
                SBQQ__BillingName__c='child account',
                SBQQ__BillingStreet__c='123 Main St',
                SBQQ__BillingCity__c='Flagler',
                SBQQ__BillingState__c='FL',
                SBQQ__BillingPostalCode__c='10101',
                SBQQ__BillingCountry__c='USA',
                Contracted_Pricing_Account__c='Quoted Account',
                RecordType=qrt
        );
        insert testQuote;
        
        testQuote.Contracted_Pricing_Account__c='Quoted Account Parent';
        update testQuote;
        
        testQuote.Contracted_Pricing_Account__c='Alliance Account';
        update testQuote;
        
    }
    
    
    static testMethod void testGetOppId() {
    
        RecordType art = [select id,Name from RecordType where SobjectType='Account' and Name='FuelEd' Limit 1];
        RecordType ort = [select id,Name from RecordType where SobjectType='Opportunity' and Name='FuelEd' Limit 1];
        RecordType qrt = [select id,Name from RecordType where SobjectType='SBQQ__Quote__c' and Name='Quote - Draft' Limit 1];
        RecordType crt = [select id,Name from RecordType where SobjectType='Contract' and Name='Subscription Parent' Limit 1];
        
        // Create parent MR
        Master_Registry__c testMR1 = new Master_Registry__c (
                Institution_Name__c='Parent Test MR',
                Name='4129450',
                No_Parent__c=true
        );
        insert testMR1;
        
        // Create child MR
        Master_Registry__c testMR2 = new Master_Registry__c (
                Institution_Name__c='Child Test MR',
                Name='4129451',
                No_Parent__c=false,
                Parent_Global_ID__c=testMR1.id
        );
        insert testMR2;
        
        // Create the Parent/Alliance account
        Account testAcct1 = new Account(
                Name='Parent/Alliance Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                Org_ID2__c=testMR1.id
        
        );
        insert testAcct1;
        
        // Create the Child account
        Account testAcct2 = new Account(
                Name='Child Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                ParentId=testAcct1.id,
                Alliance_Parent_Account__c=testAcct1.id,
                Org_ID2__c=testMR2.id
        );
        insert testAcct2;
        
        System.debug('Parent Account; ' + testAcct2.parentid);
        System.debug('Alliance Account; ' + testAcct2.Alliance_Parent_Account__c);
        
        // Create the Pricing Opportunity
        Opportunity testOpp = new Opportunity (
                Name='Opportunity for testing',
                AccountId=testAcct2.id,
                Type='Current Customer: New Program',
                CloseDate=date.today(),
                StageName='Prospect',
                Probability=13,
                ForecastCategoryName='Pipeline',
                Service_Delivery_State__c='FL',
                Preliminary_Interest__c='Online Courses',
                Estimated_Service_Start_Date__c=(date.today()+1),
                Estimated_Service_End_Date__c=(date.today()+365),
                Refund_Drop_Policy__c='Custom 14-day: 100%',
                pricebook2id='01s700000002Q9F',
                RecordType=ort
        );
        insert testOpp;
        
        // Create the Pricing Quote
        SBQQ__Quote__c testQuote = new SBQQ__Quote__c (
                SBQQ__Account__c=testAcct2.id,
                SBQQ__Opportunity2__c=testOpp.id,
                SBQQ__Status__c='Draft',
                SBQQ__Type__c='Quote',
                SBQQ__StartDate__c=(date.today()+1),
                SBQQ__EndDate__c=(date.today()+365),
                SBQQ__ExpirationDate__c=(date.today()+30),
                SBQQ__PaymentTerms__c='Net 30',
                Drop_Policy__c='50% refund for days 0-14',
                Auto_Renew__c='NO',
                SBQQ__BillingName__c='child account',
                SBQQ__BillingStreet__c='123 Main St',
                SBQQ__BillingCity__c='Flagler',
                SBQQ__BillingState__c='FL',
                SBQQ__BillingPostalCode__c='10101',
                SBQQ__BillingCountry__c='USA',
                Contracted_Pricing_Account__c='Quoted Account',
                RecordType=qrt
        );
        insert testQuote;
        
        // Create the Subscription Parent Contract
        Contract testContract = new Contract (
                Accountid=testAcct2.id,
                Service_Delivery_State__c='FL',
                Contract_Request_Summary__c='Test class text to prevent new renewal opportunity',
                Status='Created',
                StartDate=(date.today()+30),
                EndDate=(date.today()+365),
                SBQQ__Opportunity__c=testOpp.id,
                ContractTerm=12,
                SBQQ__RenewalForecast__c=false,
                SBQQ__RenewalQuoted__c=false,
                RecordType=crt
        );
        insert testContract;
        delete testContract;
        undelete testContract;
        delete testQuote;
        delete testOpp;
        undelete testOpp;
        
    }
    
    static testMethod void testGetContractId() {
    
        RecordType art = [select id,Name from RecordType where SobjectType='Account' and Name='FuelEd' Limit 1];
        RecordType ort = [select id,Name from RecordType where SobjectType='Opportunity' and Name='FuelEd' Limit 1];
        RecordType qrt = [select id,Name from RecordType where SobjectType='SBQQ__Quote__c' and Name='Quote - Draft' Limit 1];
        RecordType crt = [select id,Name from RecordType where SobjectType='Contract' and Name='Subscription Parent' Limit 1];
        
        // Create parent MR
        Master_Registry__c testMR1 = new Master_Registry__c (
                Institution_Name__c='Parent Test MR',
                Name='4129450',
                No_Parent__c=true
        );
        insert testMR1;
        
        // Create child MR
        Master_Registry__c testMR2 = new Master_Registry__c (
                Institution_Name__c='Child Test MR',
                Name='4129451',
                No_Parent__c=false,
                Parent_Global_ID__c=testMR1.id
        );
        insert testMR2;
        
        // Create the Parent/Alliance account
        Account testAcct1 = new Account(
                Name='Parent/Alliance Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                Org_ID2__c=testMR1.id
        
        );
        insert testAcct1;
        
        // Create the Child account
        Account testAcct2 = new Account(
                Name='Child Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                ParentId=testAcct1.id,
                Alliance_Parent_Account__c=testAcct1.id,
                Org_ID2__c=testMR2.id
        );
        insert testAcct2;
        
        System.debug('Parent Account; ' + testAcct2.parentid);
        System.debug('Alliance Account; ' + testAcct2.Alliance_Parent_Account__c);
        
        // Create the Pricing Opportunity
        Opportunity testOpp = new Opportunity (
                Name='Opportunity for testing',
                AccountId=testAcct2.id,
                Type='Current Customer: New Program',
                CloseDate=date.today(),
                StageName='Prospect',
                Probability=13,
                ForecastCategoryName='Pipeline',
                Service_Delivery_State__c='FL',
                Preliminary_Interest__c='Online Courses',
                Estimated_Service_Start_Date__c=(date.today()+1),
                Estimated_Service_End_Date__c=(date.today()+365),
                Refund_Drop_Policy__c='Custom 14-day: 100%',
                pricebook2id='01s700000002Q9F',
                RecordType=ort
        );
        insert testOpp;
        
        // Create the Pricing Quote
        SBQQ__Quote__c testQuote = new SBQQ__Quote__c (
                SBQQ__Account__c=testAcct2.id,
                SBQQ__Opportunity2__c=testOpp.id,
                SBQQ__Status__c='Draft',
                SBQQ__Type__c='Quote',
                SBQQ__StartDate__c=(date.today()+1),
                SBQQ__EndDate__c=(date.today()+365),
                SBQQ__ExpirationDate__c=(date.today()+30),
                SBQQ__PaymentTerms__c='Net 30',
                Drop_Policy__c='50% refund for days 0-14',
                Auto_Renew__c='NO',
                SBQQ__BillingName__c='child account',
                SBQQ__BillingStreet__c='123 Main St',
                SBQQ__BillingCity__c='Flagler',
                SBQQ__BillingState__c='FL',
                SBQQ__BillingPostalCode__c='10101',
                SBQQ__BillingCountry__c='USA',
                Contracted_Pricing_Account__c='Quoted Account',
                RecordType=qrt
        );
        insert testQuote;
        
        // Create the Subscription Parent Contract
        Contract testContract = new Contract (
                Accountid=testAcct2.id,
                Service_Delivery_State__c='FL',
                Contract_Request_Summary__c='Test class text to prevent new renewal opportunity',
                Status='Created',
                StartDate=(date.today()+30),
                EndDate=(date.today()+365),
                SBQQ__Opportunity__c=testOpp.id,
                ContractTerm=12,
                SBQQ__RenewalForecast__c=false,
                SBQQ__RenewalQuoted__c=false,
                RecordType=crt
        );
        insert testContract;
        
        // Create the Sales Order
        SCRB_SalesOrder__c testSalesOrder = new SCRB_SalesOrder__c (
                AccountId__c=testAcct2.id,
                Bill_To_Name__c='child account',
                Bill_to_Account__c=testAcct2.id,
                BillingCity__c='Flagler',
                BillingCountry__c='USA',
                BillingPostalCode__c='10101',
                BillingState__c='FL',
                BillingStreet__c='123 Main St',
                ShippingCity__c='Flagler',
                ShippingCountry__c='USA',
                ShippingPostalCode__c='10101',
                ShippingState__c='FL',
                ShippingStreet__c='123 Main St',
                Contract__c=testContract.id,
                OpportunityId__c=testOpp.id,
                StatusCode__c='Approved - In Fullfillment'
        );
        insert testSalesOrder;
        testSalesOrder.StatusCode__c='Fulfilled';
        update testSalesOrder;      
            
        
    }
    
     
    static testMethod void testDeleteContact() {
    
        RecordType art = [select id,Name from RecordType where SobjectType='Account' and Name='FuelEd' Limit 1];
        RecordType ort = [select id,Name from RecordType where SobjectType='Opportunity' and Name='FuelEd' Limit 1];
        RecordType qrt = [select id,Name from RecordType where SobjectType='SBQQ__Quote__c' and Name='Quote - Draft' Limit 1];
        RecordType crt = [select id,Name from RecordType where SobjectType='Contract' and Name='Subscription Parent' Limit 1];
        
        // Create parent MR
        Master_Registry__c testMR1 = new Master_Registry__c (
                Institution_Name__c='Parent Test MR',
                Name='4129450',
                No_Parent__c=true
        );
        insert testMR1;
        
        // Create child MR
        Master_Registry__c testMR2 = new Master_Registry__c (
                Institution_Name__c='Child Test MR',
                Name='4129451',
                No_Parent__c=false,
                Parent_Global_ID__c=testMR1.id
        );
        insert testMR2;
        
        // Create the Parent/Alliance account
        Account testAcct1 = new Account(
                Name='Parent/Alliance Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                Org_ID2__c=testMR1.id
        
        );
        insert testAcct1;
        
        // Create the Child account
        Account testAcct2 = new Account(
                Name='Child Account',
                Client_Service_Territory__c='Fuhrmeister',
                Audit_Review_Status__c='Verified',
                Phone='404-555-4994',
                BillingStreet='123 Main St',
                BillingCity='Flagler',
                BillingState='FL',
                BillingPostalCode='10101',
                BillingCountry='USA',
                RecordType=art,
                ParentId=testAcct1.id,
                Alliance_Parent_Account__c=testAcct1.id,
                Org_ID2__c=testMR2.id
        );
        insert testAcct2;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid= testAcct2.id;
        cont.MailingState='FL';
        cont.Phone='5555555555';
        cont.Email='testguy@testingtest.com';
        insert cont;
        
        delete cont;
        undelete cont;
            
            
        
    }
    
}