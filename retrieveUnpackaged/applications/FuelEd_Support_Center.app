<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>FuelEd Support Center</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>MPM4_BASE__Milestone1_Project__c</tab>
    <tab>standard-Idea</tab>
    <tab>Quota_Summary__c</tab>
    <tab>Master_Registry__c</tab>
</CustomApplication>
