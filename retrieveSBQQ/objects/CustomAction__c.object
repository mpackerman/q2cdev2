<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Custom action definition.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Flag that determines whether this action is used.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the checkbox to make this action available for use.</inlineHelpText>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ConditionsMet__c</fullName>
        <deprecated>false</deprecated>
        <description>Choose the behavior for your conditions: All is AND; Any is OR.</description>
        <externalId>false</externalId>
        <inlineHelpText>Choose the behavior for your conditions: All is AND; Any is OR.</inlineHelpText>
        <label>Conditions Met</label>
        <picklist>
            <picklistValues>
                <fullName>All</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Any</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Default__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Sets action to be the default overriding action.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select checkbox to set action as the default overriding action.</inlineHelpText>
        <label>Default</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <description>Description of the Custom Action which will appear when users hover over the button.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the value for what you want to appear when a user hovers over the Custom Action button.</inlineHelpText>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>DisplayOrder__c</fullName>
        <deprecated>false</deprecated>
        <description>The order in which this action display in the UI.</description>
        <externalId>false</externalId>
        <inlineHelpText>The order in which this action display in the UI.</inlineHelpText>
        <label>Display Order</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Label__c</fullName>
        <deprecated>false</deprecated>
        <description>Label displayed for this action in the UI.</description>
        <externalId>false</externalId>
        <inlineHelpText>Label displayed for this action in the UI.</inlineHelpText>
        <label>Label</label>
        <picklist>
            <picklistValues>
                <fullName>Add Products</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Add Favorites</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Renew Subscriptions</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Upgrade Assets</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Page__c</fullName>
        <deprecated>false</deprecated>
        <description>The page of which to add the custom action.</description>
        <externalId>false</externalId>
        <inlineHelpText>The page of which to add the custom action.</inlineHelpText>
        <label>Page</label>
        <picklist>
            <picklistValues>
                <fullName>Product Configurator</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quote Line Editor</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>TargetObject__c</fullName>
        <deprecated>false</deprecated>
        <description>Object targeted by this action.</description>
        <externalId>false</externalId>
        <inlineHelpText>Object targeted by this action.</inlineHelpText>
        <label>Target Object</label>
        <picklist>
            <picklistValues>
                <fullName>Product</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Asset</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Subscription</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Favorite</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>URLTarget__c</fullName>
        <deprecated>false</deprecated>
        <description>Method of navigating to the URL.</description>
        <externalId>false</externalId>
        <inlineHelpText>Method of navigating to the URL.</inlineHelpText>
        <label>URL Target</label>
        <picklist>
            <picklistValues>
                <fullName>Popup</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Replace Page</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>URL__c</fullName>
        <deprecated>false</deprecated>
        <description>URL to visit &apos;URL&apos; is selected as the Target Object.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify URL to navigate to. You are able to add Quote, Account, and Opportunity fields available in Quote Line Editor (API Names). Examples: http://www.steelbrick.com, http://www.yoursite.com/{!Id}, or /{!Account.Name}/ {!SBQQ__YourCustomVar__c}</inlineHelpText>
        <label>URL</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <label>Custom Action</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Active__c</columns>
        <columns>NAME</columns>
        <columns>DisplayOrder__c</columns>
        <columns>Label__c</columns>
        <columns>TargetObject__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Action Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Custom Actions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
